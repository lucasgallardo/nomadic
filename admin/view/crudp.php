<?php 
include 'header.php'; 
include '../control/functions.php';
?>

<body>

    <div id="wrapper">
        <?php include('navigation.php'); ?>          

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12"><h1 class="page-header">Personas
                    <form class="navbar-form navbar-right" action="" method="POST">
                          <input type="search" class="form-control" name="buscado" placeholder="Buscar nómade...">
                    </form></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-2">
                    <!-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Agregar</button> -->
                </div>
                <div class="col-lg-10">
                    <?php message(); ?>
                </div>                
                <br>
                <hr>
                <div class="col-lg-12">
                    <?php include '../control/crudp_list_admin.php';  ?>                            
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->

        <script>
          function eliminar(id){            
            var mensaje = "¿QUIÉRE BORRAR ESTE USUARIO?";
            if(confirm(mensaje)){
              window.location="../control/person_admin.php?IdBorrar="+id;
              }else{

              }
            }
        </script>

  <!-- Modal -->
  <div id="myModal" class="modal fade in" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Nuevo Nómade</h4>
        </div>
        <div class="modal-body">
            <?php include('advertising_loader.php'); ?>        
        </div> <!-- modal body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>

<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<script src="../dist/js/functions.js"></script>

</body>

</html>

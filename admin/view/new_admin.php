<div class="panel">
    <div class="row">
        <form method="post">
            <div class="col-lg-10">
              <div class="form-group">
              <label for="email">Email</label>
              <input type="text" name="email" id="email" class="form-control">
            </div>

            <div class="form-group">
              <label for="user">Usuario</label>
              <input type="text" name="user" id="user" class="form-control">
            </div>

            <div class="form-group">
              <label for="pass">Contraseña</label>
              <input type="password" name="pass" id="pass" class="form-control">
            </div>

            <div class="form-group">
              <label for="rpass">Repetir contraseña</label>
              <input type="password" name="rpass" id="rpass" class="form-control">
            </div>

            <br><br>

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="button" name="registrar" id="registrar" class="btn btn-primary" value="Registrar">                    
                </div>
                <div class="col-lg-10">
                    <span id="result"></span>                    
                </div>
            </div>
            <br><br>
            </div>            
        </form>
    </div> <!-- row -->
</div> <!-- panel -->
<div class="panel">
    <div class="row">
        <form class="" action="../control/advertising_admin.php" method="POST" enctype="multipart/form-data">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Descripción</label>
                    <input class="form-control" type="text" name="descripcion">
                </div>
                <div class="form-group">
                    <label>Tipo</label>
                    <select class="form-control" name="tipo">
                        <option>Grande</option>
                        <option>Mediana</option>
                        <option>Pequeña</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Fecha de Inicio</label>
                    <input class="form-control" type="date" name="fecha_inicio" value="<?php echo date('Y-m-d'); ?>">
                </div>
                <div class="form-group">
                    <label>Fecha de Fin</label>
                    <input class="form-control" type="date" name="fecha_finalizacion" value="<?php echo date('Y-m-d'); ?>">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Link de la publicidad</label>
                    <input class="form-control" type="text" name="link">
                </div>
                <div class="form-group">
                    <label>Target</label>
                    <select class="form-control" name="tarjet">
                        <option value="1">Persona</option>
                        <option value="2">Empresa</option>
                        <option value="3">Indistinto</option>                                                
                    </select>
                </div>
                <div class="form-group">
                    <label>Imagen</label>
                    <input type="file" name="imagen" required="">
                </div>
                <button type="submit" class="btn btn-primary" name="save">Guardar</button>
            </div>
        </form>
    </div> <!-- row -->
</div> <!-- panel -->

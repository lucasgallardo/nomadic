<div class="panel">
    <div class="row">
        <form class="" action="../control/social_admin.php" method="POST" enctype="multipart/form-data">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name">
                </div>
                <div class="form-group">
                    <label>Link de la Red Social</label>
                    <input class="form-control" type="text" name="link">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Imagen</label>
                    <input type="file" name="imagen" required="">
                </div>
                <button type="submit" class="btn btn-primary" name="save">Guardar</button>
            </div>
        </form>
    </div> <!-- row -->
</div> <!-- panel -->

<?php 
session_start();
if (!isset($_SESSION["user_id"])) {
	header('Location:../index.php');
}
include '../control/conexion.php';
include 'header.php'; 
include '../control/functions.php';
$sql=mysqli_query($conexion,"SELECT * FROM admin_user WHERE id_admin_user='$_SESSION[user_id]' ") or die(mysqli_error($conexion));
$row=mysqli_fetch_array($sql);
?>

<body>
	<div id="wrapper">
		<?php include('navigation.php'); ?>          

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12"><h1 class="page-header">Perfil de <?php echo $row['name_admin_user']; ?></h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-2">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Agregar usuario</button>
				</div>
				<div class="col-lg-10">
					<?php message(); ?>
				</div>                
				<br>
				<hr>
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Datos de Administrador</h3>
						</div>
						<div class="panel-body">
							<?php 
							?>
							<form action="../control/admin_back.php" method="POST" class="form-horizontal" role="form">
								<div class="col-md-6">
									<div class="form-group">
										<label for="nombre" class="col-md-3 control-label">Nombre:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre y Apellido" value="<?php echo $row['name_admin_user']; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label for="nombre" class="col-md-3 control-label">E-mail:</label>
										<div class="col-md-8">
											<input type="text" class="form-control" id="correo" name="correo" placeholder="Correo electrónico" value="<?php echo $row['email_admin_user']; ?>">
										</div>
									</div>                          
									<div class="text-right">
										<div class="form-group">
											<div class="col-md-10">
												<button type="submit" name="datos" class="btn btn-primary">Guardar cambios</button>
											</div>
										</div>
									</div>
								</div>
							</form>							

							<div class="col-md-6">
								<form action="../control/admin_back.php" method="POST" class="form-horizontal" role="form" onsubmit="return validarPasswd()">
									<div class="form-group">
										<label for="nombre" class="col-md-4 control-label">Contraseña Nueva:</label>
										<div class="col-md-8">
											<input type="password" class="form-control" id="usuario_clave" name="passNueva" placeholder="Contraseña nueva" required="">
										</div>
									</div>
									<div class="form-group">
										<label for="nombre" class="col-md-4 control-label">Confirmar Contraseña:</label>
										<div class="col-md-8">
											<input type="password" class="form-control" id="usuario_clave2" name="passConfirmar" placeholder="Reingrese contraseña" required="">
										</div>
									</div>
									<div class="text-right">
										<div class="form-group">
											<div class="col-md-10">
												<button type="submit" name="submit" class="btn btn-primary">Cambiar Contraseña</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>                    	
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->
		<script type="text/javascript">
			function validarPasswd(){
				var p1 = document.getElementById("usuario_clave").value;
				var p2 = document.getElementById("usuario_clave2").value;
				if (p1 != p2) {
					alert("Las contraseñas deben de coincidir");
					return false;
				} else {
					return true; 
				}
			}
		</script>

		<!-- Modal -->
		<div id="myModal" class="modal fade in" role="dialog">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Nuevo Usuario Administrador</h4>
					</div>
					<div class="modal-body">
						<?php include('new_admin.php'); ?>
					</div> <!-- modal body -->
					<div class="modal-footer">
		            	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>						
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="../vendor/jquery/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="../vendor/metisMenu/metisMenu.min.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="../dist/js/sb-admin-2.js"></script>

		<script src="../dist/js/functions.js"></script>

	</body>

	</html>

<?php 
include 'header.php'; 
include '../control/functions.php';
?>

<body>

    <div id="wrapper">
        <?php include('navigation.php'); ?>          

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">                    
                    <h1 class="page-header">Tarifas</h1>                        
                </div>                
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Administración de Tarifas</div>
                        <div class="panel-body">
                            <div class="row">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th width="10">Nombre</th>
                                            <th>Descripción</th>
                                            <th>Monto</th>
                                            <th>Link MercadoPago</th>
                                            <th>Ir</th>
                                            <th>Link PayPal</th>
                                            <th>Ir</th>
                                            <th>Guardar</th>
                                        </th>
                                    </thead>
                                    <tbody>
                                        <?php include '../control/rates_list_admin.php'; ?>
                                    </tbody>
                                </table>                                
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->                        
                    </div>
                    <!-- /.panel-body -->
                    <?php message(); ?>              
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<script src="../dist/js/functions.js"></script>

</body>

</html>

$(document).ready(function() {
  $('#login').click(function(){
    var user = $('#user').val();
    var pass = $('#pass').val();
    if($.trim(user).length > 0 && $.trim(pass).length > 0){
      $.ajax({
        url:"control/authenticator.php",
        method:"POST",
        data:{user:user, pass:pass},
        cache:"false",
        beforeSend:function() {
          $('#login').val("Conectando...");
        },
        success:function(data) {
          $('#login').val("Login");
          if (data=="1") {
            $(location).attr('href','main.php');
          } else {
            $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> las credenciales son incorrectas.</div>");
          }
        }
      });
    }else{
      $("#result").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>Debes completar todos los campos primero.</div>");
    };
  });
});

$(document).ready(function(){
  $('#registrar').click(function(){
    var email = $('#email').val();
    var user = $('#user').val();
    var pass = $('#pass').val();
    var rpass = $('#rpass').val();
    if ($.trim(user).length > 0 && $.trim(pass).length > 0 && $.trim(email).length > 0 && $.trim(rpass).length > 0){
      $.ajax({
        url:'control/register_back.php',
        method:"POST",
        data:{email:email, user:user, pass:pass, rpass:rpass},
        cache:false,
        beforeSend:function(){
          $('#registrar').val("Comprobando información...");
        },
        success:function(data){
          $('#registrar').val("Registrar");
          if(data){
            $("#result").html(data);
          };
        }
      });
    }else{
      $("#result").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>Debes completar todos los campos primero.</div>");
    };
  });
});

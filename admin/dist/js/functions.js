$(document).ready(function() {
  $('#login').click(function(){
    var user = $('#user').val();
    var pass = $('#pass').val();
    if($.trim(user).length > 0 && $.trim(pass).length > 0){
      $.ajax({
        url:"control/authenticator.php",
        method:"POST",
        data:{user:user, pass:pass},
        cache:"false",
        beforeSend:function() {
          $('#login').val("Conectando...");
        },
        success:function(data) {
          $('#login').val("Login");
          console.log(data);
          if (data=="1") {
            $(location).attr('href','view/main.php');
          } else {
            $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> las credenciales son incorrectas.</div>");
          }
        }
      });
    }else{
      $("#result").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>Debes completar todos los campos primero.</div>");
    };
  });
});

$(document).ready(function(){
  $('#registrar').click(function(){
    var email = $('#email').val();
    var user = $('#user').val();
    var pass = $('#pass').val();
    var rpass = $('#rpass').val();
    if ($.trim(user).length > 0 && $.trim(pass).length > 0 && $.trim(email).length > 0 && $.trim(rpass).length > 0){
      $.ajax({
        url:'../control/admin_back.php',
        method:"POST",
        data:{email:email, user:user, pass:pass, rpass:rpass},
        cache:false,
        beforeSend:function(){
          $('#registrar').val("Comprobando información...");
        },
        success:function(data){
          $('#registrar').val("Registrar");
          if(data){
            $("#result").html(data);
          };
        }
      });
    }else{
      $("#result").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>Debes completar todos los campos primero.</div>");
    };
  });
});

function showEdit(editableObj) {
            $(editableObj).css("background","rgb(215, 195, 247)");
        }
function state_update(editableObj,column,idUpdate) {
    $(editableObj).css("background","#FFF url(loaderIcon.gif) no-repeat right");
    $.ajax({
        url: "../control/advertising_admin.php",
        type: "POST",
        data:'column='+column+'&editval='+editableObj.innerHTML+'&idUpdate='+idUpdate,
        success: function(data){
            $(editableObj).css("background","rgb(217, 237, 247)");
        }
   });
}

function update_social(editableObj,column,idUpdate) {
    $(editableObj).css("background","#FFF url(loaderIcon.gif) no-repeat right");
    $.ajax({
        url: "../control/social_admin.php",
        type: "POST",
        data:'column='+column+'&editval='+editableObj.innerHTML+'&idUpdate='+idUpdate,
        success: function(data){
            $(editableObj).css("background","rgb(217, 237, 247)");
        }
   });
}

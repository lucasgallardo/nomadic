<div class="panel panel-default">
    <div class="panel-heading">Listado de publicidades</div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Descripción</th>
                    <th>Imagen</th>
                    <th>Tamaño</th>
                    <th>Link</th>
                    <th>Tarjet</th>
                    <th>Habilitada</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                <?php
               	include_once 'conexion.php';
                include_once 'functions.php';
                $query=mysqli_query($conexion,"SELECT * FROM advertising") or die(mysqli_error($conexion));
                while ($row=mysqli_fetch_array($query)) { ?>
                	<tr>
                		<td><?php echo $row['advertising_description']; ?></td>
                		<td><img src="../../img/advertising/<?php echo $row['advertising_img']; ?>" alt="sin imagen" width='50px'></td>
                		<td><?php echo $row['advertising_type']; ?></td>
                		<td><?php echo $row['advertising_link']; ?></td>
                    <td><?php echo tarjet_name($row['advertising_tarjet']); ?></td>
                		<td><div class="checkbox">
                            <label>
                                <?php if($row['advertising_available']==1){ ?>
                                <input type="checkbox" value="" checked onClick="state_update(this,'0','<?php echo $row['advertising_id'] ?>')">
                                <?php }else{ ?>
                                <input type="checkbox" value="" onClick="state_update(this,'1','<?php echo $row['advertising_id'] ?>')">
                                <?php } ?>
                            </label>
                        </div></td>
                        <td><?php echo '<button class="btn btn-danger" onclick="eliminar('.$row['advertising_id'].')"><span class="glyphicon glyphicon-trash"></span></button>'; ?></td>
                	</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

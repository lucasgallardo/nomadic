<div class="panel panel-default">
    <div class="panel-heading">Listado de Empresas</div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Postulante</th>
                    <th>Email</th>
                    <th>Postulado en</th>
                    <th>Empresa</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                <?php
               	include_once 'conexion.php';
                if(isset($_POST['buscado'])) {
                    $query=mysqli_query($conexion,"SELECT * FROM user_empresa
                        WHERE (user_empresa_title like '%$_POST[buscado]%'
                           OR user_empresa_email like '%$_POST[buscado]%'
                           OR user_empresa_name like '%$_POST[buscado]%'
                           OR user_empresa_cuit like '%$_POST[buscado]%' ) ") or die(mysqli_error($conexion));
                }else{
                    $query=mysqli_query($conexion,"SELECT * FROM
                                                  oferta_postulante
                                                      INNER JOIN
                                                  user ON oferta_postulante.id_postulante = user.user_id
                                                      INNER JOIN
                                                  oferta_laboral ON oferta_postulante.id_oferta = oferta_laboral.id_empresa
                                                      INNER JOIN
                                                  user_empresa ON oferta_laboral.id_empresa = user_empresa.user_empresa_id")
                                                  or die(mysqli_error($conexion));
                }


                while ($row=mysqli_fetch_array($query)) { ?>
                	<tr>
                		<td><?php echo $row['user_name']; ?></td>
                		<td><?php echo $row['user_email']; ?></td>
                    <td><?php echo $row['tipo_de_puesto']; ?></td>
                    <td><?php echo $row['user_empresa_title']; ?></td>
                		<td><a href=""><span class="badge"><?php echo complaint($conexion, $row['user_empresa_id'], 1) ?></span></a></td>
                    <td><?php echo '<button class="btn btn-danger" onclick="eliminar('.$row['user_empresa_id'].')"><span class="glyphicon glyphicon-trash"></span></button>'; ?></td>
                	</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

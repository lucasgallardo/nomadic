<?php
session_start();

function message(){
    if (isset($_SESSION['message'])) {
        switch ($_SESSION['message']) {
            case "error_login":
                echo "<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Usuario o Contraseña incorrectos</div>";
                break;
            case "ad_saved":
                echo "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Publicidad guardada correctamente</div>";
                break;
            case "ad_error":
                echo "<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error al guardar</div>";
                break;
            case "update":
                echo "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Actualizado correctamente</div>";
                break;
            case "update_error":
                echo "<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error al actualizar</div>";
                break;
            case "ad_deleted":
                echo "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Borrado correctamente</div>";
                break;
            case "ad_error_delete":
                echo "<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error al borrar</div>";
                break;
            case "social_saved":
                echo "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Red social guardada correctamente</div>";
                break;
            case "social_error":
                echo "<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Error al guardar</div>";
                break;


        }
        $_SESSION['message'] = "";
    }
}

function complaint($conexion,$id,$type){
    //1=empresa, 2=nomade
        $sql=mysqli_query($conexion,"SELECT COUNT(*) as total FROM complaint
                                     WHERE complaint_foreign_id='$id'
                                     AND complaint_type='$type'");
        $row=mysqli_fetch_array($sql);
        return $row['total'];
}

function tarjet_name($tarjet){
	switch ($tarjet) {
		case '1':
			return "Personas";
			break;
		case '2':
			return "Empresas";
			break;
		default:
			return "Indistinto";
			break;
	}
}
?>

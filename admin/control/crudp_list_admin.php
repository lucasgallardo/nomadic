<div class="panel panel-default">
    <div class="panel-heading">Listado de Nómades</div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>                    
                    <th>Verificado</th>
                    <th>Denuncias</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                <?php
               	include_once 'conexion.php';
                if(isset($_POST['buscado'])) {
                    $query=mysqli_query($conexion,"SELECT * FROM user 
                        WHERE (user_name like '%$_POST[buscado]%'
                           OR user_email like '%$_POST[buscado]%') ") or die(mysqli_error($conexion));
                }else{
                    $query=mysqli_query($conexion,"SELECT * FROM user") or die(mysqli_error($conexion));
                }
                
                
                while ($row=mysqli_fetch_array($query)) { ?>
                	<tr>
                		<td><?php echo $row['user_name']; ?></td>
                		<td><?php echo $row['user_email']; ?></td>
                		<?php if ($row['user_verified']==1) {
                			echo '<td><span class="glyphicon glyphicon-ok-sign text-success fa-2x"></span></td>';
                		}else{
                			echo '<td><span class="glyphicon glyphicon-remove-sign text-danger fa-2x"></span></td>';                			
                		}
                		?>                		
                		<td><a href=""><span class="badge"><?php echo complaint($conexion, $row['user_id'], 2) ?></span></a></td>
                        <td><?php echo '<button class="btn btn-danger" onclick="eliminar('.$row['user_id'].')"><span class="glyphicon glyphicon-trash"></span></button>'; ?></td>
                	</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
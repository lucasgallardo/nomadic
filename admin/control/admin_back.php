<?php 
session_start();
include 'conexion.php';

if (isset($_POST['datos'])) {
	$usuario_correo=mysqli_real_escape_string($conexion,$_POST['correo']);
	$usuario_nombre=mysqli_real_escape_string($conexion,$_POST['nombre']);

	$actualizo=mysqli_query($conexion,"UPDATE admin_user SET name_admin_user='$usuario_nombre', email_admin_user='$usuario_correo' WHERE id_admin_user='$_SESSION[user_id]'") OR die(mysqli_error($conexion));
	if ($actualizo) {
		$_SESSION['message']="update";
	}else{
      	$_SESSION['message']="update_error";
    }
    header('Location:../view/profile.php');		
}

if (isset($_POST['submit'])) {  
	$usuario_clave_nueva = mysqli_real_escape_string($conexion,$_POST['passNueva']);	

 	$semilla = "administrador"; //uso como semilla para encriptar la clave
  	$admin_pass = crypt($usuario_clave_nueva,$semilla);
	
	$sql=mysqli_query($conexion,"UPDATE admin_user SET pass_admin_user='$admin_pass' WHERE id_admin_user='$_SESSION[user_id]'") or die(mysqli_error($conexion));
	if ($sql) {
		$_SESSION['message']="update";
	}else{
      	$_SESSION['message']="update_error";
    }
    header('Location:../view/profile.php');	

}

if(isset($_POST["user"]) && isset($_POST["pass"]) && isset($_POST["rpass"]) && isset($_POST["email"])){
  $email = mysqli_real_escape_string($conexion, $_POST["email"]);
  $user = mysqli_real_escape_string($conexion, $_POST["user"]);
  $pass = mysqli_real_escape_string($conexion, $_POST["pass"]);
  $rpass = mysqli_real_escape_string($conexion, $_POST["rpass"]);
  $result = "";

  if(strlen($pass) > 30) {
    $result .= "<br>-La contraseña supera los 30 caracteres.";
  }
  if ($pass != $rpass) {
    $result .= "<br>-Las contraseñas no coinciden.";
  }

  $semilla = "administrador"; //uso como semilla para encriptar la clave
  $admin_pass = crypt($pass,$semilla);

  if(strlen($user) > 30) {
    $result .= "<br>-El usuario supera los 30 caracteres.";
  } else {
    $sql = "SELECT COUNT(*) as cantidad FROM admin_user WHERE name_admin_user='$user'";
    $res = mysqli_query($conexion, $sql);
    $data = mysqli_fetch_array($res);
    if ($data["cantidad"] > 0) {
      $result .= "<br>-El usuario ya existe.";
    }
  }

  if(strlen($email) > 100) {
    $result .= "<br>-El email supera los 100 caracteres.";
  } else {
    if (preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $email ) ) {
        $sql = "SELECT COUNT(*) as cantidad FROM admin_user WHERE email_admin_user='$email'";
      $res = mysqli_query($conexion, $sql);
      $data = mysqli_fetch_array($res);
      if ($data["cantidad"] > 0) {
        $result .= "<br>-El email ya está registrado.";
      }
    } else {
      $result .= "<br>-Está intentando ingresar un email inválido.";
    }
  }

  if ($result != "") {
    echo "<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error</strong><br>$result</div>";
  } else {
    $sql = "INSERT INTO admin_user VALUES(NULL, '$user', '$admin_pass', '$email')";
    mysqli_query($conexion, $sql);
    echo "<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Correcto!</strong><br>Se ha registrado correctamente.</div>";
  }
} else {
  echo "Error";
}
?>
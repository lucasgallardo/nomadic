<div class="panel panel-default">
    <div class="panel-heading">Listado de publicidades</div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Link</th>
                    <th>Ir</th>
                    <th>Habilitada</th>
                    <th>Borrar</th>
                </tr>
            </thead>
            <tbody>
                <?php
               	include_once 'conexion.php';
                include_once 'functions.php';
                $query=mysqli_query($conexion,"SELECT * FROM social") or die(mysqli_error($conexion));
                while ($row=mysqli_fetch_array($query)) { ?>
                	<tr>
                		<td><?php echo $row['social_name']; ?></td>
                		<td><img src="../../img/<?php echo $row['social_img']; ?>" alt="sin imagen" width='50px'></td>
                		<td><?php echo $row['social_link']; ?></td>
                    <td><a href="<?php echo $row['link1_rate']; ?>" target="_blank"><span class="glyphicon glyphicon-link"></span></a></td>
                    <td><div class="checkbox">
                            <label>
                                <?php if($row['social_enable']==1){ ?>
                                <input type="checkbox" value="" checked onClick="update_social(this,'0','<?php echo $row['social_id'] ?>')">
                                <?php }else{ ?>
                                <input type="checkbox" value="" onClick="update_social(this,'1','<?php echo $row['social_id'] ?>')">
                                <?php } ?>
                            </label>
                        </div></td>
                        <td><?php echo '<button class="btn btn-danger" onclick="eliminar('.$row['social_id'].')"><span class="glyphicon glyphicon-trash"></span></button>'; ?></td>
                	</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

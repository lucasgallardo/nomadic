<?php
function rates_loader($id,$conexion){

	$sql=mysqli_query($conexion,"SELECT * FROM rate WHERE id_rate='$id' ") or die(mysqli_error($conexion));
	$row=mysqli_fetch_array($sql);
	return array('id' => $row['id_rate'],
				 'number' => $row['name_rate'],
				 'description' => $row['description_rate'],
				 'price' => $row['price_rate'],
				 'linkMP' => $row['link1_rate'],
				 'linkPP' => $row['link2_rate']);
}

function load_person($id,$conexion){
	$query=mysqli_query($conexion,"SELECT * FROM user,personas WHERE user_id='$id' AND id_persona='$id' ") or die(mysqli_error($conexion));
	$row=mysqli_fetch_array($query);
	if ($row['srcAvatar']=="") { //si no tiene imagen de perfil coloca una genérica
		$img="../img/generic_profile.png";
	}else{
		$img="../img/".$row['srcAvatar'];
	}
	return array('name' => $row['user_name'],
 							 'edad' => CalculaEdad($row['fecNac']),
						 	 'cuil' => $row['cuil'],
						   'genero' => $row['genero'],
						 	 'pais' => $row['nacionalidad'],
						   'image' => $img,
						 	 'email' => $row['user_email']);
}

function load_company($id,$conexion){
	//$query=mysqli_query($conexion,"SELECT * FROM user_empresa,empresa WHERE  user_empresa_id='$id' AND id_empresa=user_empresa_id ") or die(mysqli_error($conexion));
	$query=mysqli_query($conexion,"SELECT * FROM user_empresa
																 inner join empresas on user_empresa.user_empresa_id = empresas.id_empresa
		                             WHERE  user_empresa_id='$id' ")
																 or die(mysqli_error($conexion));
	$row=mysqli_fetch_array($query);
	if ($row['img']=="") { //si no tiene imagen de perfil coloca una genérica
		$img="../img/generic_profile.png";
	}else{
		//$img="../img/".$row['srcAvatar'];
		$img=$row['img'];
	}
	return array('name' => $row['user_empresa_title'],
							 'razon_social' => $row['user_empresa_name'],
						 	 'cuil' => $row['user_empresa_cuit'],//$row['user_empresa_cuit'],
						   'pais' => $row['pais'],//$row['user_empresa_cuit'],
						 	 'provincia' => $row['provEst'],//$row['user_empresa_cuit']
							 'image' => $img,
							 'id' => $row['user_empresa_id']
						 );
}

function datosCV($id,$conexion){
	$query=mysqli_query($conexion,"SELECT
																	    *
																	FROM
																	    user
																	    inner join personas on user.user_id = personas.id_persona
																	    inner join carnets on user.user_id = carnets.id_persona
																	    inner join datos_contacto on user.user_id = datos_contacto.id_persona
																	    inner join disponibilidades on user.user_id = disponibilidades.id_persona
																	    inner join rubros on user.user_id = rubros.id_persona
																			inner join direcciones on user.user_id = direcciones.idPersona
																	WHERE
																	    user.user_id = '$id'")
																 or die(mysqli_error($conexion));
	$row=mysqli_fetch_array($query);
	if ($row['srcAvatar']=="") { //si no tiene imagen de perfil coloca una genérica
		$img="../img/generic_profile.png";
	}else{
		$img="../img/".$row['srcAvatar'];
	}
	return array('name' => $row['user_name'],
							 'edad' => CalculaEdad($row['fecNac']),
							 'cuil' => $row['cuil'],
							 'genero' => $row['genero'],
							 'pais' => $row['nacionalidad'],
							 'image' => $img,
						   'email' => $row['user_email'],
						   'telefono' => $row['telefono'],
						   'direccion' => $row['calle']." ".$row['altura']." - ".$row['piso']." - ".$row['dpt'],
							 'cp' => $row['codPostal'],
							 'departamento' => $row['departamento'],
						   'provincia' => $row['provEst'],
						   'pais' => $row['pais'],
							 'idioma' => $row['idioma']
						 );
}

function CalculaEdad( $fecha ) {
    list($Y,$m,$d) = explode("-",$fecha);
    return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
}

function load_oferta_completa($id,$conexion){
	$query=mysqli_query($conexion,"SELECT * FROM oferta_laboral
																 INNER JOIN user_empresa
		                             WHERE id_oferta_laboral='$id' ")
																 or die(mysqli_error($conexion));
	$row=mysqli_fetch_array($query);
	$company=load_company($row['id_empresa'],$conexion);

	return array('id_empresa' => $row['id_empresa'],
							 'codigo_identificacion' => $row['codigo_identificacion'],
						   'tipo_de_puesto' => $row['tipo_de_puesto'],
							 'cantidad' => $row['cantidad'],
							 'pasante' => $row['pasante'],
							 'descripcion' => $row['descripcion'],
							 'experiencia' => $row['experiencia'],
							 'nivel_educativo' => $row['nivel_educativo'],
							 'residencia_pais' => $row['residencia_pais'],
							 'residencia_provincia' => $row['residencia_provincia'],
							 'nomade' => $row['nomade'],
							 'edad' => $row['edad'],
							 'genero' => $row['genero'],
							 'idioma' => $row['idioma'],
							 'carnet_conducir' => $row['carnet_conducir'],
							 'carnet_categoria' => $row['carnet_categoria'],
							 'remuneracion' => $row['remuneracion'],
							 'name' => $company['name'],
 							 'razon_social' => $company['razon_social'],
 						 	 'cuil' => "23-234234234-2",//$company['cuil'],
 						   'pais' => "Argentina",//$company['pais'],
 						 	 'provincia' => "Mendoza",//$company['provincia']
 							 'image' => $company['image']
						   );
}

function rubros_seleccionados($id_oferta,$conexion){
	$rubros=mysqli_query($conexion,"SELECT * FROM rubros_oferta_laboral
		                              WHERE id_oferta='$id_oferta'")
	                                or die(mysqli_error($conexion));
	$resultado=mysqli_fetch_array($rubros);
	if ($resultado['bodega']=='true') {
		$seleccionados.='<input type="checkbox" name="bodegas" id="bodegas">
		<label for="bodegas"><span><img src="../img/bodega-ico.png" alt=""><p>bodegas</p></span></label>';
	}
	if ($resultado['hoteles']=='true') {
		$seleccionados.='<input type="checkbox" name="hoteles" id="hoteles">
		<label for="hoteles"><span><img src="../img/hoteleria-ico.png" alt=""><p>hoteles</p></span></label>';
	}
	if ($resultado['gastronomia']=='true') {
		$seleccionados.='<input type="checkbox" name="gastronomia" id="gastronomia">
		<label for="gastronomia"><span><img src="../img/resto-ico.png" alt=""><p>gastronomia</p></span></label>';
	}
	if ($resultado['turismo']=='true') {
		$seleccionados.='<input type="checkbox" name="turismo" id="turismo">
		<label for="turismo"><span><img src="../img/turismo-ico.png" alt=""><p>turismo</p></span></label>';
	}
	if ($resultado['spa']=='true') {
		$seleccionados.='<input type="checkbox" name="spa" id="spa">
		<label for="spa"><span><img src="../img/spa-ico.png" alt=""><p>spa</p></span></label>';
	}
	if ($resultado['lineas_aereas']=='true') {
		$seleccionados.='<input type="checkbox" name="lineas-aereas" id="lineas-aereas">
		<label for="lineas-aereas"><span><img src="../img/avion-ico.png" alt=""><p>líneas aéreas</p></span></label>';
	}
	if ($resultado['lineas_terrestres']=='true') {
		$seleccionados.='<input type="checkbox" name="lineas-terrestres" id="lineas-terrestres">
		<label for="lineas-terrestres"><span><src="../img/transporte-ico.png" alt=""><p>líneas terrestres</p></span></label>';
	}
	if ($resultado['alquiler_autos']=='true') {
		$seleccionados.='<input type="checkbox" name="alquiler-de-autos" id="alquiler de autos">
		<label for="alquiler de autos"><span><src="../img/auto-ico.png" alt=""><p>alquiler de autos</p></span></label>';
	}
	if ($resultado['wine_shop']=='true') {
		$seleccionados.='<input type="checkbox" name="wine-shop" id="wine-shop">
		<label for="wine-shop"><span><img src="../img/wineshop-ico.png" alt=""><p>wine shop</p></span></label>';
	}
	if ($resultado['logistica']=='true') {
		$seleccionados.='<input type="checkbox" name="logistica" id="logistica">
		<label for="logistica"><span><img src="../img/logistica-ico.png" alt=""><p>logística</p></span></label>';
	}

	return $seleccionados;
}

function postulantesTotales($id,$conexion){
	$sql = mysqli_query($conexion,"SELECT id_oferta_laboral FROM oferta_laboral
	                                 WHERE id_empresa = '$id'");
  $total=mysqli_num_rows($sql);
	return $total;
}

function postulantesPorOferta($id_oferta,$id,$conexion){
	$sql=mysqli_query($conexion,"SELECT id_oferta_postulante FROM oferta_postulante
																WHERE id_oferta='$id_oferta'");
	$total=mysqli_num_rows($sql);
	return $total;
}

function ofertas_totales($conexion){
	$sql=mysqli_query($conexion,"SELECT id_oferta_laboral FROM oferta_laboral")
	                             or die(mysqli_error($conexion));
	return mysqli_num_rows($sql);
}

function postulantes_totales($conexion){
	$sql=mysqli_query($conexion,"SELECT user_id FROM user
	                             WHERE user_verified=1")
	                             or die(mysqli_error($conexion));
	return mysqli_num_rows($sql);
}

function tipo_de_usuario($conexion,$user){
	$sql=mysqli_query($conexion,"SELECT user_id FROM user WHERE user_id='$user'")
															 or die(mysqli_error($conexion));
	if(mysqli_num_rows($sql)==1){
		return 1; //nomade
	}else{
		$sql=mysqli_query($conexion,"SELECT user_empresa_id FROM user_empresa
			                           WHERE user_empresa_id='$user'")
																 or die(mysqli_error($conexion));
		if (mysqli_num_rows($sql)==1) {
			return 2; //empresa
		}
	}
}
?>

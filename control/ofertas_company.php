<?php
session_start();
include_once('conexion.php');
$ofertas=mysqli_query($conexion,"SELECT id_oferta_laboral,
                                        id_empresa,
                                        tipo_de_puesto,
                                        descripcion
                                  FROM oferta_laboral
                                  WHERE id_empresa = '$_SESSION[user]' ") or die(mysqli_error($conexion));
if (mysqli_num_rows($ofertas)<1) {
  echo "<h3>Todavía no ha creado ofertas</h3><br><h4>Puede hacerlo desde el botón
  <strong>Generar nueva oferta laboral</strong>";
}
while ($row=mysqli_fetch_array($ofertas)) {  ?>
  <article class="postulantes-a-oferta">
    <!--ID o nombre o código de la oferta (más que nada para control)-->
    <p class="id">OM-000584D</p>

    <!--el numero de gente que se postuló a mi oferta laboral-->
    <div class="cantidad-postulantes">
      <img src="img/ico-nomades-cv.png" alt="">
      <span>
        <h4><?php echo postulantesPorOferta($row['id_oferta_laboral'],$row['id_empresa'],$conexion); ?></h4>
        <p>Postulaciones</p>
      </span>
    </div>


  <!--muestra reducida de la oferta creada por la empresa-->
  <div id="resultado-ofertas">

    <div class="resultado-grupo">
      <div class="rubro"><img src="img/bodega-ico.png" alt="Bodega"></div>
      <div class="unir">
        <p class="puesto"><?php echo $row['tipo_de_puesto']; ?></p>

        <p class="descripcion"><?php echo $row['descripcion']; ?></p>
      </div>
    </div>
    <div class="modificar-oferta">
      <div class="vertical"></div>
      <div class="inputs">
        <a href="oferta-completa.php?io=<?php echo $row['id_oferta_laboral']; ?>"><input type="submit" name="" id="" class="submit-otro" value="ver oferta"></a>
        <input type="submit" name="" id="" class="submit-otro" value="eliminar oferta">
      </div>
    </div>
  </div> <!--termina oferta-->
    <div class="barra-acciones">
      <a href="p_o.php?ol=<?php echo $row['id_oferta_laboral']; ?>"><p id="cambia">Ver postulantes</p></a>
      <a href=""><p class="descarga">Descargar todos los CV</p></a>
    </div>
  </article>
  <br>
  <hr>
<?php } ?>

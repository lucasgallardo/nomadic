  <?php
  if($existe=="persona"){
    $sql="SELECT user_id ui, user_name un, user_email ue FROM user WHERE user_email='$email'";
    $type=10;
  }elseif ($existe=="empresa") {
    $sql="SELECT user_empresa_id ui, user_empresa_name un, user_empresa_email ue FROM user_empresa WHERE user_empresa_email='$email'";
    $type=20;
  }

  $sql = mysqli_query($conexion,$sql) or die(mysqli_error($conexion));
  if($row = mysqli_fetch_array($sql)) {
    $date=date('m-Y');
    $token=crypt($email,$date);
    $user_id=$row["ui"];
    $destinatario = $row["ue"];
    $asunto = "Recuperar clave Nomadic";

$cuerpo='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Nomadic Resources</title>
        <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        .content {width: 100%; max-width: 600px;}
        h3{
            font-family:"Montserrat";
            font-size: 18px;
            text-align: center;
            font-weight: 400;
            color: #841F30;
            padding: 0;
            margin:0;
            margin-bottom: 20px;
        }

        h2{
            font-family: "Montserrat";
            font-size: 11px;
            text-align: center;
            font-weight: 300;
            text-transform: uppercase;
            padding: 0;
            margin:0;
            margin-bottom: 0;
        }

        p{
            font-family: "Montserrat";
            font-size: 14px;
            text-align: center;
            font-weight: 300;
            text-align: center;
            padding: 0;
            margin:0;
            margin-bottom: 20px;
        }

        p span{
            font-family: "Montserrat";
            font-weight: 400;
        }

        img{
            position: relative;
            display:block;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 20px;
            margin-top: 20px;
            padding-right: 5px;
        }

        img.redes{
            width: 50px;
            height: auto;
            display: block;
            margin-right: auto;
            margin-left: auto;
            padding-right: 0px;
        }

        a{
            text-decoration: none;
            color: black;
        }

        a:hover{
            color: #841F30;
        }

        table#fondo{
            background-color: lightgray;
            width: 100%;
            height: auto;
            border-radius: 10px 10px;
            margin-bottom: 20px;
            margin-top: 20px;
            padding-top: 20px;
        }

         table#fondoredes{
            background-color: #DADDCB;
            width: 100%;
            height: auto;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        table#fondoredes h2{
            padding-top: 20px;
        }

       button {
            background-color: #841F30; /* Bordó */
            color: white;
            padding: 10px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            font-family: "Montserrat"
            border-radius: 8px 8px 8px 8px;
            -moz-border-radius: 8px 8px 8px 8px;
            -webkit-border-radius: 8px 8px 8px 8px;
            border: 0px solid #000000;
            display:block;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 20px;
            -webkit-transition: .5s;
            -o-transition: .5s;
            transition: .5s;
            cursor: pointer;
        }

        button:hover{
            background-color: gray;
            color:white;
            -webkit-transition: .5s;
            -o-transition: .5s;
            transition: .5s;
        }

        </style>
    </head>

    <body yahoo bgcolor="#f6f8f1">
        <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="350px" height="130px">

                            </td>
                        </tr>
                        <tr>
                            <td width="160px" height="96px">
                                <a href="http://www.nomadicresources.com" target="_blanck"><img src="'.$_SERVER["HTTP_HOST"].'/img/nomadiclogo.png" alt="Nomadic Resources" /></a>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <a href="'.$_SERVER["HTTP_HOST"].'/view/clave.php?tk='.$token.'&ui='.$user_id.'&ue='.$email.'&t='.$type.'"><h3>Sigue el siguente enlace para recuperar tu contraseña</h3></a>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <p>

                               <span>O copia y pega el siguiente enlace en tu navegador</b>. '.$_SERVER["HTTP_HOST"].'/view/clave.php?tk='.$token.'&ui='.$user_id.'&ue='.$email.'&t='.$type.'</span> </p>
                            </td>
                        </tr>
                    </table>
                    <table id="fondo" class="content" align="center" cellpadding="0" cellspacing="0" border="0" background="#E6E7E8">
                        <tr>
                            <td>
                                <h2>PRIMEROS PASOS</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3>Consejos para hacer un Curriculum de calidad</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <spam>Curriculum Vitae,</spam> la herramienta indispensable para encontrar un buen trabajo. <br />Descubre cómo hacer un buen CV gracias a nuestras recomendaciones, <br />de la mano de especialistas.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="'.$_SERVER["HTTP_HOST"].'/img/iconocurriculum.png" alt="curriculum vitae" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button> EMPEZÁ EL TUYO
                                </button>
                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <hr>
                            </td>
                        </tr>
                    </table>
                    <table id="fondoredes" class="content" align="center" cellpadding="0" cellspacing="0" border="0" background="#DADDCB">
                             <tr><td><h2 width="100%" background="#DADDCB">Seguinos en las redes</h2></td></tr>

                            </table>
                    <table id="fondoredes" class="content" align="center" cellpadding="0" cellspacing="0" border="0" background="#DADDCB">
                        <tr>
                        <td></td>
                            <td width="70px" style="text-align:center"><a href=""><img src="'.$_SERVER["HTTP_HOST"].'/img/youtube.png" alt="Youtube" class="redes"/></a></td>
                            <td width="70px" style="text-align:center"><a href=""><img src="'.$_SERVER["HTTP_HOST"].'/img/facebook-rojo.png" alt="Facebook" class="redes" /></a></td>
                            <td width="70px" style="text-align:center"><a href=""><img src="'.$_SERVER["HTTP_HOST"].'/img/twitter-rojo.png" alt="Twitter" class="redes" /></a></td>
                            <td width="70px" style="text-align:center"><a href=""><img src="'.$_SERVER["HTTP_HOST"].'/img/instagram.png" alt="Instagram" class="redes" /></a></td>
                            <td width="70px" style="text-align:center"><a href=""><img src="'.$_SERVER["HTTP_HOST"].'/img/blogspot.png" alt="Blogspot" class="redes" /></a></td>
                            <td width="70px" style="text-align:center"><a href=""><img src="'.$_SERVER["HTTP_HOST"].'/img/google-mas.png" alt="Google+" class="redes"/></a></td>
                             <td></td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <hr>
                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <a href=""> <h3>www.nomadicresources.com.ar</h3></a>
                            </td>
                        </tr>
                    </table>
                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <p>Copyright © 2017 nomadicresources <br>
                                    Estás recibiendo este mail porque te registraste en<br> nomadicresources.com.ar</p>
                                    <br>
                                    <p>¿Querés cambiar cómo recibís estos mails?<br>
                                    Podés ajustar tus preferencias o <br>
                                    <a href="">desuscribirte de esta lista.</a></p>
                                    <br>
                                    <a href=""><p>Ver Condiciones y Política de Privacidad.</p></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>

';


//para el envío en formato HTML
      $headers = "MIME-Version: 1.0\r\n";
      $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
      $headers .= "X-Priority: 3\n";
      // $headers .= "X-MSMail-Priority: Normal\n";
      // $headers .= "X-Mailer: php\n";

//dirección del remitente
      //$headers .= "From: Registro sitio Nomadic <noreply@nomadic.com>\r\n";
      $headers .= "From: Recupera clave Nomadic <no-reply@nomadicresources.com>\r\n";

      mail($destinatario,$asunto,$cuerpo,$headers);
    }
?>

<?php

include('conexion.php');

if(isset($_POST["name"]) && isset($_POST["lastname"]) && isset($_POST["email"]) && isset($_POST["pass"]) && isset($_POST["rpass"])){
  $name = mysqli_real_escape_string($conexion, $_POST["name"]);
  $lastname = mysqli_real_escape_string($conexion, $_POST["lastname"]);
  $user_name = $name." ".$lastname;
  $email = mysqli_real_escape_string($conexion, $_POST["email"]);
  $pass = mysqli_real_escape_string($conexion, $_POST["pass"]);
  $rpass = mysqli_real_escape_string($conexion, $_POST["rpass"]);
  $empresa = mysqli_real_escape_string($conexion, $_POST["empresa"]);
  $cuit = mysqli_real_escape_string($conexion, $_POST["cuit"]);
  $result = "";
  $captcha=$_POST['g-recaptcha-response'];

/* Validamos con Google */
$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcT-DQUAAAAAB7_mhhba9iFT3PLQb_FtRAhPmCW&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

/* Enviamos de vuelta las respuestas, si no es humano, envía 0 y si lo es, envía 1.*/

if($response.success==false){
  $result .= "<br>-Error en el captcha";
}
/*
  if(strlen($pass) > 30) {
    $result .= "<br>-La contraseña supera los 30 caracteres.";
  }
  */
  if ($pass != $rpass) {
    $result .= "<br>-Las contraseñas no coinciden.";
  }

  $semilla = "!#$%&/()"; //uso como semilla para encriptar la clave
  $user_pass = crypt($pass,$semilla);


/*
  if(strlen($user) > 30) {
    $result .= "<br>-El usuario supera los 30 caracteres.";
  } else {
    $sql = "SELECT COUNT(*) as cantidad FROM user WHERE name='$name'";
    $res = mysqli_query($conexion, $sql);
    $data = mysqli_fetch_array($res);
    if ($data["cantidad"] > 0) {
      $result .= "<br>-El usuario ya existe.";
    }
  }
*/
  if(strlen($email) > 100) {
    $result .= "<br>-El email supera los 100 caracteres.";
  } else {
    if (preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $email ) ) {
      if(!empty($empresa) && !empty($cuit)){
        $sql = "SELECT COUNT(*) as cantidad FROM user_empresa WHERE user_empresa_email='$email'";
      }else {
        $sql = "SELECT COUNT(*) as cantidad FROM user WHERE user_email='$email'";
      }
      $res = mysqli_query($conexion, $sql);
      $data = mysqli_fetch_array($res);
      if ($data["cantidad"] > 0) {
        $result .= "<br>-El email ya está registrado.";
      }
    } else {
      $result .= "<br>-Está intentando ingresar un email inválido.";
    }
  }

  if ($result != "") {
    echo "<div class='error'>$result</div>";
  } else {
    if (!empty($empresa) && !empty($cuit)) { //si cargo datos de empresa ingresa en el if
      $sql = "INSERT INTO user_empresa VALUES(NULL, '$empresa', '$cuit', '$user_name', '$user_pass', '$email', 0)";
    } else {
      $sql = "INSERT INTO user VALUES(NULL, '$user_name', '$user_pass', '$email', 0)";
    }
    mysqli_query($conexion, $sql) or die(mysqli_error($conexion));
    include('mail_delivery.php');
    echo "<div class=''> ¡Se ha registrado correctamente!<br>
    Un mail con información fue enviando a la casilla de correo que usó para registrarse<br></div>";
  }
} else {
  echo "Error";
}

?>

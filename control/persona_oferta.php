<?php
session_start();
include_once('conexion.php');
$id=$_REQUEST['ol'];
$ofertas=mysqli_query($conexion,"SELECT id_oferta_laboral,
  id_empresa,
  tipo_de_puesto,
  descripcion
  FROM oferta_laboral
  WHERE id_empresa = '$_SESSION[user]'
  AND id_oferta_laboral = '$id' ") or die(mysqli_error($conexion));
  $row=mysqli_fetch_array($ofertas) ?>
  <article class="postulantes-a-oferta">
    <!--ID o nombre o código de la oferta (más que nada para control)-->
    <p class="id">OM-000584D</p>

    <!--el numero de gente que se postuló a mi oferta laboral-->
    <div class="cantidad-postulantes">
      <img src="img/ico-nomades-cv.png" alt="">
      <span>
        <h4>32</h4>
        <p>Postulaciones</p>
      </span>
    </div>


    <!--muestra reducida de la oferta creada por la empresa-->
    <div id="resultado-ofertas">

      <div class="resultado-grupo">
        <div class="rubro"><img src="img/bodega-ico.png" alt="Bodega"></div>
        <div class="unir">
          <p class="puesto"><?php echo $row['tipo_de_puesto']; ?></p>

          <p class="descripcion"><?php echo $row['descripcion']; ?></p>
        </div>
      </div>
      <div class="modificar-oferta">
        <div class="vertical"></div>
        <div class="inputs">
          <input type="submit" name="" id="" class="submit-otro" value="ver oferta">
          <input type="submit" name="" id="" class="submit-otro" value="eliminar oferta">
        </div>
      </div>
    </div> <!--termina oferta-->
    <div class="barra-acciones">
      <a href=""><p class="descarga">Descargar todos los CV</p></a>
    </div>
<!-- Filtro de búsqueda postulante -->
<?php
//query build
if (isset($_POST['filtrar'])) {
  $id=$_POST['ol'];
  $sql="SELECT * FROM oferta_postulante
            INNER JOIN user ON user.user_id = oferta_postulante.id_postulante ";

  if (isset($_POST['disponibilidad'])) {
    //$sql.="AND nomade=true";
  }
  if (isset($_POST['nomade'])) {
    $sql.="INNER JOIN	datos_contacto
            ON oferta_postulante.id_postulante = datos_contacto.id_persona
            AND datos_contacto.nomade='true'";
  }
  if(isset($_POST['idiomas'])){
    $sql.="INNER JOIN personas
            ON oferta_postulante.id_postulante = personas.id_persona
            AND personas.idioma != ''";
  }

  $sql.=" WHERE oferta_postulante.id_oferta = '$id' ";

} else {
  $sql="SELECT * FROM user, oferta_postulante
                      WHERE id_oferta='$id'
                      AND user_id=id_postulante";
}

 ?>
    <div id="filtros" class="filtros-postulantes">
    	<div class="grupo-filtros">
        <form class="" action="" method="POST">
    		<select name="disponibilidad" id="disponibilidad">
    			<option value="" disabled selected hidden>Disponibilidad</option>
    			<option value="fulltime">Full-time</option>
    			<option value="parttime">Part-time</option>
    			<option value="eventual">Eventual</option>
          <option value="temporal">Temporal</option>
          <option value="finde">Fines de Semana</option>
    			<option value="">Me es indiferente</option>
    		</select>

        <?php if(isset($_POST['nomade'])){ ?>
            <p class="soy-nomade textos-rosa"><input type="checkbox" name="nomade" id="checkeador" checked>Es Nómade</p>
        <?php } else{ ?>
    		    <p class="soy-nomade textos-rosa"><input type="checkbox" name="nomade" id="checkeador">Es Nómade</p>
        <?php }
        if (isset($_POST['idiomas'])) { ?>
            <p class="soy-nomade textos-rosa"><input type="checkbox" name="idiomas" id="checkeador" checked>Maneja varios idiomas</p>
        <?php } else {?>
    		    <p class="soy-nomade textos-rosa"><input type="checkbox" name="idiomas" id="checkeador">Maneja varios idiomas</p>
        <?php } ?>

    		<p class="soy-nomade experiencia textos-rosa"><input type="checkbox" name="experiencia" id="checkeador">Con experiencia</p>
        <input type="hidden" name="ol" value="<?php echo $id; ?>">

    		<input class="submit-rosa" type="submit" value="FILTRAR" name="filtrar" id="filtrar">
        </form>
    	</div>
    </div>
    <!--resultado persona-->
    <?php

    $postulante=mysqli_query($conexion,$sql) or die(mysqli_error($conexion));
    while ($result=mysqli_fetch_array($postulante)) { ?>
      <div id="resultado-persona">
        <div class="unir">
          <div class="avatar">
            <img src="../img/avatar.jpg" alt="">
          </div>

          <div class="resultado-grupo">
            <p class="nombre"><?php echo $result['user_name']; ?></p>
            <p class="puesto">Ingeniero en biodiversidad y plataforma sdfsdfgsdg</p>
            <p class="idioma">Español, francés, inglés, catalan, italiano</p>
          </div>

          <div class="resultado-grupo">
            <p class="edad">35 años</p>
            <p class="experiencia">5 años</p>
            <p class="disponibilidad">Part-time, full-time, eventual</p>
          </div>

          <div class="resultado-grupo">
            <div class="nacionalidad">
              <img src="../img/argentina.jpg" alt="">
              <p>Argentina,</p>
              <p class="ciudad">Mendoza</p>
            </div>
            <p class="nomade">Soy nómade</p>
          </div>
        </div>

        <div class="submit">
          <div class="vertical"></div>
          <div class="input">
            <input type="submit" name="" id="ver-perfil" value="ver perfil" class="submit-rosa">
            <a href="../control/pdf.php?id=<?php echo $result['user_id']; ?>"><p class="descarga">Descargar</p></a>
            <a href=""><p class="denunciar">Denunciar</p></a>
          </div>
        </div>
      </div> <!--termina resultado persona-->

    <?php } ?>


  </article>

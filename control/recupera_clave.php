<?php

include('conexion.php');

if(isset($_POST["mail"])){
  $email = mysqli_real_escape_string($conexion, $_POST["mail"]);

/*
  if(strlen($user) > 30) {
    $result .= "<br>-El usuario supera los 30 caracteres.";
  } else {
    $sql = "SELECT COUNT(*) as cantidad FROM user WHERE name='$name'";
    $res = mysqli_query($conexion, $sql);
    $data = mysqli_fetch_array($res);
    if ($data["cantidad"] > 0) {
      $result .= "<br>-El usuario ya existe.";
    }
  }
*/
  if(strlen($email) > 100) {
    $result .= "<br>-El email supera los 100 caracteres.";
  } else {
    if (preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $email ) ) {

      $query=mysqli_query($conexion,"SELECT user_id FROM user WHERE user_email='$email'") or die(mysqli_error($conexion));
      if(mysqli_num_rows($query)==1){
        $existe="persona";
      }else {
        $query=mysqli_query($conexion,"SELECT user_empresa_id FROM user_empresa WHERE user_empresa_email='$email'") or die(mysqli_error($conexion));
        if(mysqli_num_rows($query)==1){
          $existe="empresa";
        }else{
          $result .= "<br>-El mail ingresado no existe en nuestra base de datos.";
        }
      }
    } else {
      $result .= "<br>-Está intentando ingresar un email inválido.";
    }
  }

  if ($result != "") {
    echo "<div class='error'>$result</div>";
  } else{
    include('mail_delivery_recupera.php');
    echo "<br>Te enviamos un mail para recuperar clave. <br>Por favor, revisa tu casilla de correo.";
  }

} else {
  echo "Error";
}

?>

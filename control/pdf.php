<?php
include('conexion.php');
include('functions.php');
$buscado=$_REQUEST[id];
$datos=datosCV($buscado,$conexion);

$html = '
<div class="container">
    <div class="row">
        <div class="col-xs-3">
            <img src="'.$datos[image].'" alt="" width="200px">
        </div>
        <div class="col-xs-6">
            <h1>'.$datos[name].'</h1>
            <h4>'.$datos[email].'</h4>
            <h4>'.$datos[telefono].'</h4>
            <h4>'.$datos[direccion].'</h4>
            <h4>'.$datos[cp].' - '.$datos[departamento].'</h4>
            <h4>'.$datos[provincia].' - '.$datos[pais].'</h4>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-9 col-xs-offset-1">
            <h3 font style="text-transform: uppercase;">Antecedentes laborales</h3>
        </div>
    </div>';

    $sql=mysqli_query($conexion,"SELECT * FROM datos_laborales
                                 WHERE id_persona='$buscado'
                                 ORDER BY inicio DESC");
    if (mysqli_num_rows($sql)==0) {
      $html.='<div class="row">
                  <div class="col-xs-9 col-xs-offset-1">
                    <p>Sin antecedentes laborales</p>
                  </div>
              </div>';
    }else{
      while ($resultado=mysqli_fetch_array($sql)) {
        $html.='<div class="row">
                  <div class="col-xs-3">
                    '.$resultado[inicio].' - '.$resultado[fin].'
                  </div>
                  <div class="col-xs-7">
                    <strong>
                      '.$resultado[empresa].':
                    </strong>
                      '.$resultado[descripcion].'
                  </div>
                </div>';
      }
      $html.='<hr>';
    }

    $html.='<div class="row">
                <div class="col-xs-9 col-xs-offset-1">
                    <h3 font style="text-transform: uppercase;">Formación Académica</h3>
                </div>
            </div>';

    $sql=mysqli_query($conexion,"SELECT * FROM academico_personas
                                 WHERE id_persona='$buscado'
                                 ORDER BY a_inicio DESC");
    if (mysqli_num_rows($sql)==0) {
      $html.='<div class="row">
                  <div class="col-xs-9 col-xs-offset-1">
                    <p>Sin antecedentes laborales</p>
                  </div>
              </div>';
    }else{
      while ($resultado=mysqli_fetch_array($sql)) {
        $html.='<div class="row">
                  <div class="col-xs-3">
                    '.$resultado[a_inicio].' - '.$resultado[a_fin].'
                  </div>
                  <div class="col-xs-7">
                    <strong>
                      '.$resultado[titulo].' -
                    </strong>
                      '.$resultado[nivel].'
                  </div>
                </div>';
      }
      $html.='<hr>';

      $html.='<div class="row">
                  <div class="col-xs-9 col-xs-offset-1">
                      <h3 font style="text-transform: uppercase;">Idiomas</h3>
                  </div>
              </div>';
      if ($datos[name]=="") {
        $html.='<div class="row">
                    <div class="col-xs-9 col-xs-offset-1">
                      <p>Sin idiomas cargados</p>
                    </div>
                </div>';
      }else{
          $html.='<div class="row">
                    <div class="col-xs-3">
                      '.$datos[idioma].'
                    </div>
                  </div>';
        }
        $html.='<hr>';
    }


//==============================================================
//==============================================================
//==============================================================
        $nombreDocumento="CV prueba";

        include("pdf/mpdf.php");
        //$mpdf=new mPDF('c','A3');
        $mpdf=new mPDF();
        $css= file_get_contents('../css/pdf.css');
        $mpdf->SetFont('Arial','B',11);
        $mpdf->WriteHTML($css,1);
        $mpdf->WriteHTML($html);
        //$mpdf->Output($nombreDocumento,'I');
        $mpdf->Output($nombreDocumento,'D');
        exit;

//==============================================================
//==============================================================
//==============================================================

        ?>

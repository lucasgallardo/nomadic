<?php
session_start();
include('./conexion.php');

$id = $_SESSION["user"];
//$id= 9;
$datos=json_decode(file_get_contents("php://input"));
if(isset($datos)){
	$accion = $datos-> accion;
	if($accion == 'guardarDPP'){
			$nombre=$datos->nombre;
			$nacimiento=$datos->nacimiento;
			$dni= $datos-> dni;
			$edad = $datos-> edad;
			$genero = $datos-> genero;
			$nacionalidad = $datos-> nacionalidad;
			$cuil = $datos-> cuil;
			$verif = "SELECT id_persona FROM personas
					  WHERE id_persona = '$id'";
			$cons = mysqli_query($conexion, $verif) or die(mysqli_error($conexion));
			$datosObt= mysqli_fetch_array($cons);
			$datosObt['id_persona'];
			if (!isset($datosObt['id_persona'])) {
				$guardar = "INSERT INTO personas(id_persona, fecNac, dni, edad, genero, nacionalidad, cuil) 
							VALUES('$id', '$nacimiento', '$dni', '$edad', '$genero', '$nacionalidad', '$cuil')";
				$ingresarD=mysqli_query($conexion, $guardar) or die(mysqli_error($conexion));
			}else {
				$insertDP = "UPDATE personas SET fecNac = '$nacimiento', dni = '$dni', edad = '$edad', 
							 genero= '$genero', nacionalidad = '$nacionalidad', cuil = '$cuil'
							WHERE id_persona = '$id'";
				$actualizarD = mysqli_query($conexion, $insertDP) or die(mysqli_error($conexion));
			}
			
		}else if ($accion =='guardarDDP') {
				$id_personas = $datos -> id_person;
				$calle = $datos-> calle;
				$altura = $datos-> altura;
				$piso = $datos-> piso;
				$dpt = $datos-> apartamento;
				$dept = $datos-> loc;
				$provincia = $datos-> prov;
				$pais = $datos->pais;
				$codPos=$datos->codPos;
				$latitud = $datos->lat;
				$longitud = $datos->long;
				$verifDom = "SELECT idPersona FROM direcciones
							 WHERE idPersona = '$id_personas'";
				$consDom = mysqli_query($conexion, $verifDom) or die (mysqli_error($conexion));
				$datosObtDom = mysqli_fetch_array($consDom);
				if(!isset($datosObtDom['idPersona'])){
					$guardarDom = "INSERT INTO direcciones(idPersona, calle, altura, piso, dpt, pais, provEst, departamento,
														   codPostal, latitud, longitud) VALUES('$id_personas', '$calle', '$altura',
														   '$piso', '$dpt', '$pais', '$provincia', '$dept', '$codPos', '$latitud', '$longitud')";
					$ingresarDDom= mysqli_query($conexion, $guardarDom) or die (mysqli_error($conexion));
				}else{
					$updateDom = "UPDATE direcciones SET calle='$calle', altura='$altura', piso = '$piso', dpt='$dpt', pais='$pais', 
								  provEst = '$provincia', departamento='$dept', codPostal = '$codPos', latitud = '$latitud', 
								  longitud = '$longitud'
								  WHERE idPersona = '$id_personas'";
					$insertUPD = mysqli_query($conexion, $updateDom);
				}

		}else if($accion == 'guardarDCP'){
				$usuario = $datos-> user_id;
				$mail = $datos-> mail;
				$mailAlt = $datos-> mailAlt;
				$tel = $datos-> tel;
				$telAlt = $datos-> telAlt;
				$nomade = $datos-> nomade;
				$verifContacto = "SELECT id_persona FROM datos_contacto
								  WHERE id_persona = '$usuario'";
				$obtVerifC = mysqli_query($conexion, $verifContacto) or die (mysqli_error($conexion));
				$ArrayobtVerifC = mysqli_fetch_array($obtVerifC);
				if (!isset($ArrayobtVerifC['id_persona'])) {
					$DContacto = "INSERT INTO datos_contacto(id_persona, mail, mailAlternat, telefono, 
										  telAlternat, nomade)VALUES('$usuario', '$mail', '$mailAlt', '$tel', 
										  '$telAlt', '$nomade')";
					$insertarDContacto = mysqli_query($conexion, $DContacto);
				}else{
					$updateContacto = "UPDATE datos_contacto SET mailAlternat = '$mailAlt', telefono = '$tel',
									   telAlternat='$telAlt', nomade='$nomade'
									   WHERE id_persona = '$usuario'";
					$insertUpdateContacto = mysqli_query($conexion, $updateContacto) or die (mysqli_error($conexion));
				}
		}else if ($accion == 'guardarRubros') {
				$rubro = $datos->rubro;
				$usuario = $datos-> user_id;
				$verificarRubro = "SELECT id_persona, $rubro FROM rubros
								   WHERE id_persona = '$usuario'";
				$obtVerifR = mysqli_query($conexion, $verificarRubro) or die (mysqli_error($conexion));
				$ArrayobtVerifR = mysqli_fetch_array($obtVerifR);
				if (!isset($ArrayobtVerifR['id_persona'])) {
					$Drubros = "INSERT INTO rubros(id_persona, $rubro) VALUES('$usuario','true')";
					$insertarDrubros= mysqli_query($conexion, $Drubros) or die(mysqli_error($conexion));
				}else if (isset($ArrayobtVerifR['id_persona']) && $ArrayobtVerifR['id_persona'] == $usuario && ($ArrayobtVerifR[$rubro] == '' || $ArrayobtVerifR[$rubro] =='false')) {
					$updateRubro="UPDATE rubros SET $rubro = 'true'
								  WHERE id_persona = '$usuario'";
					$insertarUpdateRubro= mysqli_query($conexion, $updateRubro) or die($conexion);
				}
				else{
					$updateRubroD="UPDATE rubros SET $rubro = 'false'
								   WHERE id_persona = '$usuario'";
					$insertarUpdateRubroD = mysqli_query($conexion, $updateRubroD) or die(mysqli_error($conexion));
				}
		}else if ($accion == "guardarCargos") {
			$usuarioCar = $datos->user_id;
			$cargo = $datos->cargo;
			$verificarCargo = "SELECT id_persona FROM puestos_personas
							   WHERE id_persona = '$usuarioCar'";
			$obtVerifiCa = mysqli_query($conexion, $verificarCargo) or die(mysqli_error($conexion));
			$ArrayobtVeriCa = mysqli_fetch_array($obtVerifiCa);
			if (!isset($ArrayobtVeriCa['id_persona'])) {
				$Dcargos = "INSERT INTO puestos_personas(id_persona, descripcion_puesto) VALUES('$usuarioCar', '$cargo')";
				$insertarDcagos = mysqli_query($conexion, $Dcargos) or die(mysqli_error($conexion));
			}else{
				$updateCargo = "UPDATE puestos_personas SET descripcion_puesto= '$cargo'
								WHERE id_persona = '$usuarioCar'";
				$insertarUpdateCargo = mysqli_query($conexion, $updateCargo) or die(mysqli_error($conexion));
			}
		}else if ($accion == "guardarAntLab") {
			$empresa = $datos->empresa;
			$sector = $datos->sector;
			$puestoAnte = $datos->puestoAnte;
			$finicio = $datos->finicio;
			$ffin = $datos->ffin;
			$descripcionP = $datos->descripcion;
			$userAL = $datos->user_id;
			if(isset($datos->id_laboral)){$id_laboral = $datos->id_laboral;} else {$id_laboral= 0;};
			$verificarDlaboral = "SELECT id_laboral, id_persona FROM datos_laborales
								  WHERE id_persona = '$userAL'
								  AND id_laboral = '$id_laboral'";
			$obtVerificarDlaboral = mysqli_query($conexion, $verificarDlaboral) or die (mysqli_error($conexion));
			$ArrayobtVerificarDlaboral = mysqli_fetch_array($obtVerificarDlaboral);
			if (!isset($ArrayobtVerificarDlaboral)) {
				$Ddlaboral = "INSERT INTO datos_laborales(id_persona, empresa, sector, puestoAntec, inicio, fin, 
														  descripcion) VALUES('$userAL', '$empresa', '$sector',
														   '$puestoAnte', '$finicio', '$ffin', '$descripcionP')";
				$insertarDblaboral=mysqli_query($conexion, $Ddlaboral) or die(mysqli_error($conexion));
			}else{
					if( $ArrayobtVerificarDlaboral['id_laboral'] == $id_laboral){

					$updateAL = "UPDATE datos_laborales SET empresa= '$empresa', sector = '$sector', puestoAntec ='$puestoAnte',
							 	inicio = '$finicio', fin='$ffin', descripcion = '$descripcionP'
							 	WHERE id_laboral = '$id_laboral'
							 	AND id_persona = '$userAL'";
					$insertarUpdateAL = mysqli_query($conexion, $updateAL) or die(mysqli_error($conexion));
					}else{
						$DdlaboralD = "INSERT INTO datos_laborales(id_persona, empresa, sector, puestoAntec, inicio, fin, 
														  descripcion) VALUES('$userAL', '$empresa', '$sector',
														   '$puestoAnte', '$finicio', '$ffin', '$descripcionP')";
						$insertarDblaboral=mysqli_query($conexion, $DdlaboralD) or die(mysqli_error($conexion));
					}
			}
		}else if ($accion == 'eliminarAntLab') {
			if(isset($datos->id_laboral)){$id_laboral = $datos->id_laboral;} else {$id_laboral= 0;};
			$eliminarDLboral = "DELETE FROM datos_laborales
								WHERE id_laboral = '$id_laboral'";
			$accionEliminarDlaboral = mysqli_query($conexion, $eliminarDLboral) or die (mysqli_error());
		}else if($accion == 'GuardAcademico'){
				$id_personaA = $datos->id_personaA;
				$nivelAcademico = $datos->nivelAcademico;
				$titulo = $datos->titulo;
				$aInicio = $datos->aInicio;
				$aFin = $datos->aFin;
				if(isset($datos->id_academico)){$id_academico = $datos->id_academico;} else {$id_academico= 0;};
				$verificarDAcademicos = "SELECT id_academico, id_persona
										 FROM academico_personas
										 WHERE id_persona = '$id_personaA'
										 AND id_academico = '$id_academico'";
				$obtenerVerifDAcadem= mysqli_query($conexion, $verificarDAcademicos) or die(mysqli_error($conexion));
				$ArrayObtVerifDAcadem = mysqli_fetch_array($obtenerVerifDAcadem);
				if(!isset($ArrayObtVerifDAcadem)){
					$insertarDA = "INSERT INTO academico_personas(id_persona, nivel, titulo, a_inicio, a_fin)
								   VALUES('$id_personaA', '$nivelAcademico', '$titulo', '$aInicio', '$aFin')";
					$insertIDA = mysqli_query($conexion, $insertarDA) or die(mysqli_error($conexion));
				}else{
					if ($ArrayObtVerifDAcadem['id_academico'] == $id_academico) {
						$updateDA = "UPDATE academico_personas SET nivel = '$nivelAcademico', titulo = '$titulo', 
									 a_inicio = '$aInicio', a_fin = '$aFin'
									 WHERE id_academico = '$id_academico'
									 AND id_persona = '$id_personaA'";
						$insertarUpdateDA = mysqli_query($conexion, $updateDA) or die(mysqli_error($conexion));
					}else{
						/*$insertarDAD = "INSERT INTO academico_personas(id_persona, nivel, titulo, a_inicio, a_fin)
								   VALUES('$id_personaA', '$nivelAcademico', '$titulo', '$aInicio', '$aFin')";
						$insertIDAD = mysqli_query($conexion, $insertarDAD) or die(mysqli_error($conexion));*/
					}
				}
		}else if($accion == 'eliminarAcademico'){
			$id_personaA = $datos->user_id;
			$id_academico = $datos-> id_aca;
			$eliminarAcademico = "DELETE FROM academico_personas 
							      WHERE id_academico = '$id_academico'
							      AND id_persona = '$id_personaA'";
			$deleteAca=mysqli_query($conexion, $eliminarAcademico) or die(mysqli_error($conexion));
		}else if($accion == 'GuardarCarnet'){
				if(isset($datos->idCarnets)){$id_carnets = $datos->idCarnets;}else{$id_carnets = 0;}
				$id_personaC = $datos->id_personaC;
				$categoria = $datos->categoria;
				$vtoCarnet = $datos->vtoCarnet;
				$verificarCarnets = "SELECT idcarnets, id_persona
									 FROM carnets
									 WHERE id_persona = '$id_personaC'
									 AND idcarnets = '$id_carnets'";
				$obtenerVerifCarn = mysqli_query($conexion, $verificarCarnets) or die (mysqli_error($conexion));
				$ArrayovtVC = mysqli_fetch_array($obtenerVerifCarn);
				if(!isset($ArrayovtVC)){
						$insertCarnets = "INSERT INTO carnets(id_persona, tipo, vencimiento)
										  VALUES('$id_personaC', '$categoria', '$vtoCarnet')";
						$insertIDC = mysqli_query($conexion, $insertCarnets) or die(mysqli_error($conexion));
				}else if($ArrayovtVC['idcarnets']==$id_carnets){
						$updateDC = "UPDATE carnets SET tipo = '$categoria', vencimiento = '$vtoCarnet'
									 WHERE id_persona = '$id_personaC'
									 AND idcarnets = '$id_carnets'";
						$insertarUpdateDC = mysqli_query($conexion, $updateDC) or die(mysqli_error($conexion));
				}
		}else if($accion == 'EliminarCarnet'){
				$id_carnetE = $datos->id_carnets;
				$id_personaEC = $datos->id_personaE;
				$eliminarCarnets = "DELETE FROM carnets
									WHERE idcarnets = '$id_carnetE'
									AND id_persona = '$id_personaEC'";
				$deleteCarnets = mysqli_query($conexion, $eliminarCarnets) or die(mysqli_error($conexion));
		}else if($accion == 'GuardarDiponible'){
				$id_personaDisp = $datos->user_dis;
				$fullTime = $datos->fullTime;
				$parTime = $datos->parTime;
				$eventual= $datos->eventual;
				$temporal= $datos->temporal;
				$finSemana= $datos->finSemana;
				$verifDispo = "SELECT id_persona, fulltime, partime, eventual, temporal, finSemana
							   FROM disponibilidades
							   WHERE id_persona = '$id_personaDisp'";
				$obtnerVerifDispo = mysqli_query($conexion, $verifDispo) or die(mysqli_error($conexion));
				$ArrayOBTVD = mysqli_fetch_array($obtnerVerifDispo);
				if(!isset($ArrayOBTVD)){
					$insertDispo = "INSERT INTO disponibilidades(id_persona, fulltime, partime, eventual, temporal, finSemana)
									VALUES($id_personaDisp, $fullTime, $parTime, $eventual, $temporal, $finSemana)";
					$insertDD= mysqli_query($conexion, $insertDispo);
				}else{
					$updateDispo = "UPDATE disponibilidades SET fulltime = '$fullTime', parTime = '$parTime', eventual = '$eventual',
									temporal = '$temporal', finSemana = '$finSemana'
									WHERE id_persona = '$id_personaDisp'";
					$insertUpdateDispo= mysqli_query($conexion, $updateDispo)or die(mysqli_error($conexion));
				}
		}else if($accion == 'GuardarIdioma'){
				$user_idioma = $datos ->user_idioma;
				$idiomas = $datos->idiomas;
				$verifIdiomas = "SELECT id_persona FROM personas
								 WHERE id_persona = '$user_idioma'";
				$ObtenerCIdioma= mysqli_query($conexion, $verifIdiomas) or die(mysqli_error($conexion));
				$ArrayObtIdioma = mysqli_fetch_array($ObtenerCIdioma);
				if(isset($ArrayObtIdioma)){
					$updateIdioma = "UPDATE personas SET idioma = '$idiomas'
							  	 	 WHERE id_persona= '$user_idioma'";
					$insertUPID = mysqli_query($conexion, $updateIdioma) or die (mysqli_error($conexion));
				}else{
					$GuardarIdioma = "INSERT INTO personas(id_persona, idioma) values('$user_idioma', '$idiomas')";
					$InsertarIdioma = mysqli_query($conexion, $GuardarIdioma) or die(mysqli_error($conexion));
				}

				
		}
}else{

$consulta= "SELECT user_id, user_name, user_email, edad, dni, nacionalidad, cuil, idioma, fecNac, genero, calle, altura, piso, dpt, pais,
				   provEst, departamento, codPostal, mailAlternat, telefono, telAlternat,nomade, bodega, hoteles,
				   gastronomia, turismo, spa, lineas_aereas, lineas_terrestres, alquiler_autos, wine_shop, logistica, descripcion_puesto,
				   fulltime, partime, eventual, temporal, finSemana,
				   CASE 
						WHEN personas.srcAvatar = '' THEN '../img/default.png'
						WHEN personas.srcAvatar IS NULL THEN '../img/default.png'
						else personas.srcAvatar
					END img
			FROM user LEFT JOIN personas
			ON user.user_id = personas.id_persona
			LEFT JOIN direcciones
			on(user.user_id = direcciones.idPersona)
			LEFT JOIN datos_contacto
			on(user.user_id = datos_contacto.id_persona)
			LEFT JOIN rubros 
			on(user.user_id = rubros.id_persona)
			LEFT JOIN puestos_personas
			on(user.user_id = puestos_personas.id_persona)
			LEFT JOIN disponibilidades
			on(user.user_id = disponibilidades.id_persona)
			WHERE user.user_id ='$id'";
$resultado = mysqli_query($conexion, $consulta) or die(mysqli_error($conexion));
while($row=$resultado->fetch_assoc()){
	$data[]=$row;
};
if(isset($data)){
		print json_encode($data);
}};

/*GUARDAR DATOS PERSONALES*/

/*FIN GUARDAR DATOS PERSONALES*/
?>
<?php
  include('conexion.php');
  include('functions.php');
  $buscado=11;//$_REQUEST[id];
  $datos=datosCV($buscado,$conexion);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="../css/pdf.css">
  </head>
  <body>
    <?php
    $html='
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <img src="'.$datos[image].'" alt="" width="200px">
            </div>
            <div class="col-xs-6">
                <h1>'.$datos[name].'</h1>
                <h4>'.$datos[email].'</h4>
                <h4>'.$datos[telefono].'</h4>
                <h4>'.$datos[direccion].'</h4>
                <h4>'.$datos[cp].' - '.$datos[departamento].'</h4>
                <h4>'.$datos[provincia].' - '.$datos[pais].'</h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-9 col-xs-offset-1">
                <h3 font style="text-transform: uppercase;">Antecedentes laborales</h3>
            </div>
        </div>';

        $sql=mysqli_query($conexion,"SELECT * FROM datos_laborales
                                     WHERE id_persona='$buscado'
                                     ORDER BY inicio DESC");
        if (mysqli_num_rows($sql)==0) {
          $html.='<div class="row">
                      <div class="col-xs-9 col-xs-offset-1">
                        <p>Sin antecedentes laborales</p>
                      </div>
                  </div>';
        }else{
          while ($resultado=mysqli_fetch_array($sql)) {
            $html.='<div class="row">
                      <div class="col-xs-2">
                        '.$resultado[inicio].' - '.$resultado[fin].'
                      </div>
                      <div class="col-xs-10">
                        <strong>
                          '.$resultado[empresa].':
                        </strong>
                          '.$resultado[descripcion].'
                      </div>
                    </div>';
          }
          $html.='<hr>';
        }

        $html.='<div class="row">
                    <div class="col-xs-9 col-xs-offset-1">
                        <h3 font style="text-transform: uppercase;">Formación Académica</h3>
                    </div>
                </div>';

        $sql=mysqli_query($conexion,"SELECT * FROM academico_personas
                                     WHERE id_persona='$buscado'
                                     ORDER BY a_inicio DESC");
        if (mysqli_num_rows($sql)==0) {
          $html.='<div class="row">
                      <div class="col-xs-9 col-xs-offset-1">
                        <p>Sin antecedentes laborales</p>
                      </div>
                  </div>';
        }else{
          while ($resultado=mysqli_fetch_array($sql)) {
            $html.='<div class="row">
                      <div class="col-xs-2">
                        '.$resultado[a_inicio].' - '.$resultado[a_fin].'
                      </div>
                      <div class="col-xs-10">
                        <strong>
                          '.$resultado[titulo].' -
                        </strong>
                          '.$resultado[nivel].'
                      </div>
                    </div>';
          }
          $html.='<hr>';
        }



        //
        //
        // <div class="row">
        // trabajo 2
        // </div>
        // <div class="row">
        // trabajo 3
        // </div>
        // <hr>
        //
        //
        // <div class="row">
        //     <div class="col-xs-5" style="width:350px">
        //           <div class="row">
        //               <div class="col-xs-4">
        //                   <ul class="list-group">
        //                       <li class="list-unstyled">Poliza</li>
        //                       <li class="list-unstyled">Certificado</li>
        //                       <li class="list-unstyled">Emisión</li>
        //                       <li class="list-unstyled">Intermediario</li>
        //                       <li class="list-unstyled">Zona-Subzona</li>
        //                       <li class="list-unstyled">N° Factura</li>
        //                       <li class="list-unstyled">Fecha Factura</li>
        //                   </ul>
        //               </div>
        //               <div class="col-xs-4" style="width:170px">
        //                   <ul class="list-group">
        //                       <li class="list-unstyled">'.$datos['numero_de_poliza'].'</li>
        //                       <li class="list-unstyled">'.$datos['numero_de_certificado'].'</li>
        //                       <li class="list-unstyled">Sunchales, '.$datos['fecha_factura_articulo'].'</li>
        //                       <li class="list-unstyled">101139-224740</li>
        //                       <li class="list-unstyled">4-0</li>
        //                       <li class="list-unstyled">'.$datos['numero_factura_articulo'].'</li>
        //                       <li class="list-unstyled">'.$datos['fecha_factura_articulo'].'</li>
        //                   </ul>
        //               </div>
        //         </div>
        //     </div>
        //     <div class="col-xs-6" style="width:544px">
        //         <div class="panel panel-default panelDerecho">
        //             <div class="panel-heading" style="background-color: #bbbbbb">
        //                 <h3 class="text-center">Datos del Asegurado</h3>
        //             </div>
        //             <div class="panel-body">
        //                 <p>'.$datos['nombre_cliente'].'</p>
        //                 <p>'.$datos['direccion_cliente'].'</p>
        //                 <p>('.$datos['codigo_postal_cliente'].') '.$datos['localidad_cliente'].' - '.$datos['provincia_cliente'].'</p>
        //                 <div style="border-top: 2px solid #000000">
        //                     <strong>Tomador</strong> Telenik
        //                 </div>
        //             </div>
        //         </div>
        //     </div>
        // </div>
        // <div class="row">
        //     <div class="col-xs-11">
        //         <div class="panel panel-default">
        //             <div class="panel-heading" style="background-color: #bbbbbb">
        //                 <h3 class="text-center">Datos de la Cobertura</h3>
        //             </div>
        //             <div class="panel-body" style="font-size:15px">
        //                 <p><strong>Bien objeto del seguro: </strong>'.$datos['descripcion_articulo'].'</p>
        //                 <div class="col-xs-offset-3">
        //                 <p>'.$datos['numero_serie_articulo'].': según consta en el nro. de remito indicado en la factura '.$datos['numero_factura_articulo'].'</p>
        //                 </div>
        //                 <p><strong>Capital asegurado: </strong>$'.$datos['precio_articulo'].'</p>
        //                 <p><strong>Meses de garantía del fabricante: </strong>'.$datos['garantia_fabricante_articulo'].'</p>
        //                 <p><strong>Ubicación del riesgo: </strong>'.$datos['direccion_cliente'].' - ('.$datos['codigo_postal_cliente'].') '.$datos['localidad_cliente'].' - '.$datos['provincia_cliente'].'</p>
        //                 <table class="table">
        //                     <thead>
        //                         <tr>
        //                             <th>Cobertura</th>
        //                             <th class="text-center">Vigencia de la cobertura</th>
        //                             <th></th>
        //                         </tr>
        //                     </thead>
        //                     <tbody>
        //                         <tr>
        //                             <td></td>
        //                             <td>Desde las 12 hs. del</td>
        //                             <td>Hasta las 12 hs. del</td>
        //                         </tr>
        //                         <tr>
        //                             <td><strong>Extensión de Garantías - Reparación</strong></td>
        //                             <td><strong>'.$datos['desde_extension_garantia_reparacion'].'</strong></td>
        //                             <td><strong>'.$datos['hasta_extension_garantia_reparacion'].'</strong></td>
        //                         </tr>
        //                         <tr>
        //                             <td><strong>Incendio, rayo y/o explosión</strong></td>
        //                             <td><strong>'.$datos['fecha_factura_articulo'].'</strong></td>
        //                             <td><strong>'.$datos['hasta_incendio'].'</strong></td>
        //                         </tr>
        //                         <tr>
        //                             <td><strong>Robo</strong></td>
        //                             <td><strong>'.$datos['fecha_factura_articulo'].'</strong></td>
        //                             <td><strong>'.$datos['hasta_robo'].'</strong></td>
        //                         </tr>
        //                         <tr>
        //                             <td><strong>Daño accidental</strong></td>
        //                             <td><strong>'.$datos['fecha_factura_articulo'].'</strong></td>
        //                             <td><strong>'.$datos['hasta_danio'].'</strong></td>
        //                         </tr>
        //                     </tbody>
        //                 </table>
        //                 <p>Esta póliza se ha contratado dentro de los límites y condiciones estipuladas en aclaraciones y exclusiones
        //                     generales en poder del tomador de la presente: Telenik</p>
        //                     <p>Se deja expresa constancia que la cobertura descrita en el presente certificado es a primer riesgo</p>
        //                 </div>
        //             </div>
        //         </div>
        //     </div>
        //     <div class="row">
        //         <p class="col-xs-11">Este certificado tendrá validez siempre que se cumpla con el plan de pago pactado oportunamente.</p>
        //         <p class="col-xs-11">Se deja expresa constancia que la cobertura de Garantía Extendida se mantendrá suspendida mientras dure el período
        //             correspondiente a la garantía del fabricante del bien citado precedentemente.</p>
        //             <p class="col-xs-11"><small>En caso de siniestro comuníquese de inmediato al teléfono 0800-444-2850 para efectuar la denuncia correspondiente y ser debidamente asesorado por nuestro personal técnico</small></p>
        //         </div>
        //
        //         <div class="row">
        //             <div class="col-xs-3">
        //                 UNIDAD DE NEGOCIOS CASA CENTRAL
        //                 Ruta 34 km. 257 - (2322) SUNCHALES
        //                 casacentral@sancorseguros.com
        //                 Tel. (03493) 428500
        //             </div>
        //
        //             <div class="col-xs-3 col-xs-offset-4">
        //                 <img src="img/firma.jpg">
        //                 <p>Alejandro Simón</p>
        //                 <p>Gerente General</p>
        //             </div>
        //         </div>
        //     </div>
        //     ';

            echo $html;
          ?>
  </body>
</html>

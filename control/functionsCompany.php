<?php
session_start();
include('./conexion.php');

$id = $_SESSION["user"];
//$id= 1;
$datosC = json_decode(file_get_contents("php://input"));
if (isset($datosC)){
	$accion = $datosC->accion;
	if ($accion == 'GuardarDContactos') {
		$id_empresa = $datosC->id_empresa;
		$nombreC = $datosC->nombreC;
		$documentoC = $datosC->documentoC;
		$puestoC = $datosC->puestoC;
		$telC = $datosC->telC;
		$telAltC = $datosC->telAltC;
		$mailC = $datosC->mailC;
		$mailAltC = $datosC->mailAltC;
		$buscarContactoC = "SELECT id_empresa FROM datos_contacto_empresas
							WHERE id_empresa = '$id_empresa'";
		$selectBuscarC = mysqli_query($conexion, $buscarContactoC) or die(mysqli_error($conexion));
		$ArraySelectBuscar = mysqli_fetch_array($selectBuscarC);
		if(isset($ArraySelectBuscar)){
			$updateContactoC = "UPDATE datos_contacto_empresas SET nombre= '$nombreC', dni_contacto = '$documentoC',
								puesto = '$puestoC', telefono = '$telC', tel_alternativo = '$telAltC',
								mail = '$mailC', mail_alternativo = '$mailAltC'
								WHERE id_empresa = '$id_empresa'";
			$insertarUpdateCont= mysqli_query($conexion, $updateContactoC) or die(mysqli_error($conexion));

		}else{
			$GuardarDContactos = "INSERT INTO datos_contacto_empresas(id_empresa, nombre, dni_contacto, puesto,
								  telefono, tel_alternativo, mail, mail_alternativo) VALUES('$id_empresa', 
								  '$nombreC', '$documentoC', '$puestoC', '$telC', '$telAltC', '$mailC', '$mailAltC')";
			$insertGuardarDC = mysqli_query($conexion, $GuardarDContactos) or die(mysqli_error($conexion));
		}
	}else if ($accion == 'guardarRubros') {
				$rubro = $datosC->rubro;
				$usuario = $datosC->user_id;
				$verificarRubro = "SELECT id_empresa, $rubro FROM rubros_empresas
								   WHERE id_empresa = '$usuario'";
				$obtVerifR = mysqli_query($conexion, $verificarRubro) or die (mysqli_error($conexion));
				$ArrayobtVerifR = mysqli_fetch_array($obtVerifR);
				if (!isset($ArrayobtVerifR['id_empresa'])) {
					$Drubros = "INSERT INTO rubros_empresas(id_empresa, $rubro) VALUES('$usuario','true')";
					$insertarDrubros= mysqli_query($conexion, $Drubros) or die(mysqli_error($conexion));
				}else if (isset($ArrayobtVerifR['id_empresa']) && $ArrayobtVerifR['id_empresa'] == $usuario && ($ArrayobtVerifR[$rubro] == '' || $ArrayobtVerifR[$rubro] =='false')) {
					$updateRubro="UPDATE rubros_empresas SET $rubro = 'true'
								  WHERE id_empresa = '$usuario'";
					$insertarUpdateRubro= mysqli_query($conexion, $updateRubro) or die($conexion);
				}
				else{
					$updateRubroD="UPDATE rubros_empresas SET $rubro = 'false'
								   WHERE id_empresa = '$usuario'";
					$insertarUpdateRubroD = mysqli_query($conexion, $updateRubroD) or die(mysqli_error($conexion));
				}
		}else if ($accion =='guardarSitio') {
				$sitioName = $datosC->sitioName;
				$userSC = $datosC->userSC;				
				$buscarContactoC = "SELECT id_empresa FROM datos_contacto_empresas
							WHERE id_empresa = '$userSC'";
				$selectBuscarC = mysqli_query($conexion, $buscarContactoC) or die(mysqli_error($conexion));
				$ArraySelectBuscar = mysqli_fetch_array($selectBuscarC);
				if(isset($ArraySelectBuscar)){
					$updateContactoC = "UPDATE datos_contacto_empresas SET sitio_web= '$sitioName'
										WHERE id_empresa = '$userSC'";
					$insertarUpdateCont= mysqli_query($conexion, $updateContactoC) or die(mysqli_error($conexion));

				}else{
					$GuardarDContactos = "INSERT INTO datos_contacto_empresas(id_empresa, sitio_web) VALUES('$userSC','$sitioName')";
					$insertGuardarDC = mysqli_query($conexion, $GuardarDContactos) or die(mysqli_error($conexion));
				}
		}else if($accion == 'guardarEmpresas'){
			$userEmp = $datosC->id_empresa;
			$razonSoc = $datosC->razonSoc;
			//$cuit = $datosC->cuit;
			$calleEmp = $datosC->calleEmp;
			$alturaEmp = $datosC->alturaEmp;
			$pisoEmp = $datosC->pisoEmp;
			$apartamento=$datosC->apartamento;
			$departamento = $datosC->loc;
			$prov = $datosC->prov;
			$pais = $datosC->pais;
			$codPos = $datosC->codPos;
			$lat = $datosC->lat;
			$long = $datosC->long;
			$telEmp=$datosC->telEmp;
			$mailEmp = $datosC->mailEmp;

			$buscarEmpresas = "SELECT id_empresa from empresas
							   WHERE id_empresa = '$userEmp'";
			$selectBucarEmp = mysqli_query($conexion, $buscarEmpresas) or die (mysqli_error($conexion));
			$ArrayEmpresas = mysqli_fetch_array($selectBucarEmp);
			if(!isset($ArrayEmpresas)){
				$guardarEmpresas = "INSERT INTO empresas(id_empresa, razonSocial, calle, altura, piso, dpt, pais, provEst, 
														 departamento, codPostal, latitud, longitud, telefonoEmp, mailEmp)
														VALUES('$userEmp', '$razonSoc', '$calleEmp', '$alturaEmp', '$pisoEmp',
															   '$apartamento', '$pais', '$prov', '$departamento', '$codPos', 
															   '$lat', '$long', '$telEmp', '$mailEmp')";
				$insertarEmpresa = mysqli_query($conexion, $guardarEmpresas) or die (mysqli_error($conexion));
			}else{
				$ActualizarEmpresas="UPDATE empresas SET razonSocial= '$razonSoc', calle = '$calleEmp', altura= '$alturaEmp', 
														 piso='$pisoEmp', dpt = '$apartamento', pais = '$pais', 
														 provEst= '$prov', departamento = '$departamento', codPostal = '$codPos',
														 latitud = '$lat', longitud = '$long', telefonoEmp= '$telEmp', 
														  mailEmp = '$mailEmp'
									WHERE id_empresa = '$userEmp'";
				$updateEmpresa = mysqli_query($conexion, $ActualizarEmpresas) or die(mysqli_error($conexion));
			}
			/*echo $userEmp."</br>";
			echo $razonSoc."</br>";
			echo $cuit."</br>";
			echo $calleEmp."</br>";
			echo $alturaEmp."</br>";
			echo $pisoEmp."</br>";
			echo $apartamento."</br>";
			echo $departamento."</br>";
			echo $prov."</br>";
			echo $pais."</br>";
			echo $codPos."</br>";
			echo $lat."</br>";
			echo $long."</br>";
			echo $telEmp."</br>";
			echo $mailEmp."</br>";*/

		}
}else{
	$consulta_company = "SELECT user_empresa_id, user_empresa_title, user_empresa_cuit, user_empresa_name, user_empresa_email, nombre, dni_contacto,
								puesto, telefono, tel_alternativo, mail, mail_alternativo, sitio_web, bodega, hoteles, gastronomia, turismo, spa, lineas_aereas, 
								lineas_terrestres, alquiler_autos, wine_shop, logistica, razonSocial, calle, altura, piso, dpt, pais,
								provEst, departamento, codPostal, telefonoEmp, mailEmp
						 FROM user_empresa LEFT JOIN datos_contacto_empresas
						 ON user_empresa.user_empresa_id = datos_contacto_empresas.id_empresa
						 LEFT JOIN rubros_empresas
						 ON user_empresa.user_empresa_id = rubros_empresas.id_empresa
						 LEFT JOIN empresas
						 ON user_empresa.user_empresa_id = empresas.id_empresa
						 WHERE user_empresa.user_empresa_id = '$id'";
	$resultado_company = mysqli_query($conexion, $consulta_company) or die(mysqli_error($conexion));

while ($row=$resultado_company->fetch_assoc()) {
		$dataC[]=$row;
};
if (isset($dataC)) {
	print json_encode($dataC);
}
};
?>
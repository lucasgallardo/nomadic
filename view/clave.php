<?php
session_start();
include_once('../control/conexion.php');
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Nomadic Resources</title>
	<!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->

	<link href="../img/icono.png" rel="shortcut icon" type="image/png" width="32px">

	<meta name="expires" content="text">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<!--para buscadores-->
	<meta name="author" content="Nomadic Resources" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="index, follow" />

	<!--si se ve bien em productos apple pongo lo siguiente-->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="expires" content="Thu Oct 14 16:06:35 CEST 2027">


	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Siempre se coloca primero normalize.css -->
	<link rel="stylesheet" href="../css/normalize.css" type="text/css">
	<!--estos dos estilos siguientes son solo para el carrusel-->
	<link rel="stylesheet" type="text/css" href="../css/slick.css">
	<link rel="stylesheet" type="text/css" href="../css/slick-theme.css">
	<link rel="stylesheet" href="../css/estilos.css" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

</head>



<body>
	<div id="pantalla">
		<header>
			<h1><img src="../img/nomadic-resources.png" alt="Nomadic Resources"></h1>
		</header>

	</div>
	<?php include("nav.php");?>
	<div id="pantalla">
		<style>
		nav.sticky div a.login-nav{
			display: none;
		}
	</style>

	<section id="formulario-contacto">
		<div id="contenedor">
			<article>
				<div class="formulario-log">
					<?php
					$usuario_email=$_REQUEST['ue'];
					$user_id=$_REQUEST['ui'];
					$type=$_REQUEST['t'];
					$date=date('m-Y');
					$tk=$_REQUEST['tk'];
					$token=crypt($usuario_email,$date);

					if ($tk==$token) { ?>
						<h2 class="form-signin-heading">Ingresar Nueva Clave</h2>
						<label for="inputEmail" class="sr-only">Clave</label>
						<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Clave" required autofocus>
						<label for="inputPassword" class="sr-only">Reingresa Clave</label>
						<input type="password" id="inputPassword2" name="repassword" class="form-control" placeholder="Reingresa Clave" required>
						<input type="hidden" name="idCliente" id="idCliente" value="<?php echo $user_id; ?>">
						<input type="hidden" name="usuario_tipo" id="usuario_tipo" value="<?php echo $type; ?>">
						<input type="hidden" name="usuario_email" id="usuario_email" value="<?php echo $usuario_email; ?>">
						<input type="submit" name="guarda_clave" id="guarda_clave" class="submit" value="Guardar">
						<span id="resultado"></span>

					<?php }else{
						header('Location:index.php');
					} ?>
				</div>
			</article>
		</div>
	</section>

	<?php include("seguinos-redes.php");?>

	<?php include("publicidades-ancho.php");?>

	<?php include("footer.php");?>

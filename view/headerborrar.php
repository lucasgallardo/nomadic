<?php

/**
 * Head + Header *
**/

?>



<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Nomadic Resources</title>
	<!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->

	<link href="img/icono.png" rel="shortcut icon" type="image/png" width="32px">

	<meta name="expires" content="text">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<!--para buscadores-->
	<meta name="author" content="Brick Soluciones Gráficas, Lucas Gallardo" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="index, follow" />

	<!--si se ve bien em productos apple pongo lo siguiente-->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="expires" content="Thu Oct 14 16:06:35 CEST 2027">


	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->

	<!-- Siempre se coloca primero normalize.css -->
	<link rel="stylesheet" href="view/css/normalize.css" type="text/css">
	<link rel="stylesheet" href="css/estilos.css" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

</head>



<body>
<div id="pantalla">
	<header>
		<h1><img src="view/img/nomadic-resources.png" alt="Nomadic Resources"></h1>
	</header>

</div>
<?php include("view/nav.php");?>
<div id="pantalla">

<?php include("header.php");?>

<div ng-app="myApp" ng-controller = "contNomadicPers">
<section class="perfil-completo-persona">
	<div id="contenedor">
		<article>
			<h2>QUIERO SER PARTE DE NOMADIC</h2>
			<div class="perfil-foto">
				<div ng-repeat="foto in data">
					<img ng-src="{{foto.img}}" alt="Foto Perfil" id="fotosSP">
				</div>
				<a href="">
					<button id="boton" ng-click="mostrarSFotoP()">CAMBIAR FOTO</button>
				</a></p>
				<div style="position: absolute; background: grey; z-index: 100; display:none; padding: 8px; border-radius:5px;" id="contFP">
					<form id="subidaP">
				        <!--<label>Subir un archivo</label><br />-->
				        <input name="fotoP" type="file" id="fotoP" accept="image/png, .jpeg, .jpg, image/gif"/><br /><br />
				        <input type="hidden" id="accion" name="accion" value="fPersonas"/></br>
				        <span ng-repeat="userP in data">
				        	<input type = "hidden" name="usuario" value="{{userP.user_id}}">
				    	</span>
				        <button id="boton" ng-click="subirArchivoP()">Subir Imagen</button><br />
				        <!--<button id="boton" >Subir Imagen</button><br />-->
				    </form>
				</div>
				&nbsp;
					<div style="background:#CEF6CE; max-width: 400px; text-align: center; display: none; position: relative; margin: auto; padding: 5px;" id="msgGEmpFP">
						¡Datos guardados correctamente!
					</div>
			</div>
			<div class="perfil">
				<!--Datos personales-->
				<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: absolute; margin: auto;" id="msgG">
					¡Datos guardados correctamente!
				</div></br>
				<div ng-repeat="datosP in data" ng-cloak  class="datos-personales" id="perfil">
					</br>
					<input value="{{datosP.user_id}}" id="user_id" type="hidden">
          			<input value="{{datosP.user_name}}" type="text" id="nombre" placeholder="Nombre y Apellido" required readonly>
          			<span style="color: red;">{{valNombre}}</span>
					<input value = "{{datosP.fecNac}}" type="date" name="" id="nacimiento"  required>
					<span style="color: red;">{{valNaci}}</span>
					<input value ="{{datosP.edad}}" type="text" name="" id="edad" placeholder="Edad" required>
					<input value ="{{datosP.dni}}" type="text" name="" id="dni" placeholder="Número de documento">
					<select name = "genero" id="generosa">
						<option value ="{{selectU}}" selected hidden >{{selectU}}</option>
						<option value = "{{selectD}}" >{{selectD}}</option>
						<option value = "{{selctT}}" >{{selctT}}</option>
						<option ng-show="{{selectU == 'Género' }}" value = "{{selctC}}" >{{selctC}}</option>
					</select>
					<input value ="{{datosP.nacionalidad}}" type="text" name="" id="nacionalidad" placeholder="Nacionalidad">
					<input value = "{{datosP.cuil}}" type="text" name="" id="cuil-persona" placeholder="CUIL">
					<input type="submit" name="" id="Modifdpp" class="submit" ng-click="modifDPP()" value="Modificar"></br>
				</div>
				<!--FIN Datos personales-->

				<!--Residencia-->
				<div ng-repeat="residencia in data">
					<div class="residencia">
						<h3>Residencia</h3>
						<span style="color: red;">{{valCalle}}</span><input type="text" name="" id="calle" placeholder="Calle" value="{{residencia.calle}}">
						<span style="color: red;">{{valAltura}}</span></br>
						<input type="text" name="" id="altura" placeholder="Altura" value="{{residencia.altura}}">
						<input type="text" name="" id="piso" placeholder="Piso" value="{{residencia.piso}}">
						<input type="text" name="" id="apartamento" placeholder="Dpto" value="{{residencia.dpt}}">
						<span style="color: red;">{{valPais}}</span><input type="text" name="" id="pais" placeholder="País" value="{{residencia.pais}}">
						<span style="color: red;">{{valProvincia}}</span><input type="text" name="" id="provincia" placeholder="Provincia" value="{{residencia.provEst}}">
						<span style="color: red;">{{valDepartamento}}</span><input type="text" name="" id="departamento" placeholder="Departamento" value="{{residencia.departamento}}">
						<span style="color: red;">{{valCodPos}}</span><input type="text" name="" id="cp" placeholder="Código Postal" value="{{residencia.codPostal}}">
						<br>
						<!--<a href="" class="agregar">Agregar ubicación en el mapa</a>-->
						<br>
						<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: relative; margin: auto;" id="msgGDom">
							¡Datos guardados correctamente!
						</div>
						<input type="submit" name="" id="" class="submit" ng-click="guardarDDP(residencia.user_id)" value="Modificar">
						<input type="hidden" id="latitudes" ng-model="latitudes">
						<input type="hidden" id="longitudes" ng-model="longitudes"></p>
					</div>
				</div>
				<!--FIN Residencia-->

				<!--Datos de contacto-->
				<div ng-repeat="dcontactos in data">
					<div class="datos-contacto">
						<h3>Datos de contacto</h3>
						<span style="color: red; font-size: 14px;">{{valMail}}</span>
						<input type="email" name="" id="mail" placeholder="@email" value="{{dcontactos.user_email}}" readonly>
						<span style="color: red; font-size: 14px;">{{valMailAlt}}</span>
						<input type="email" name="" id="mailAlt" placeholder="Agregar mail alternativo" value="{{dcontactos.mailAlternat}}">
						<span style="color: red; font-size: 14px;">{{valTel}}</span>
						<input type="tel" name="" id="telefono" placeholder="Número de teléfono" value="{{dcontactos.telefono}}">
						<span style="color: red; font-size: 14px;">{{valTelAlt}}</span>
						<input type="tel" name="" id="telefonoAlt" placeholder="Agregar  teléfono alternativo" value="{{dcontactos.telAlternat}}">
						<span><input type="checkbox" name="checkeador" id="checkeador" ng-checked="{{dcontactos.nomade}}" value="true"> Soy nómade</span>
						<p class="aclaracion">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat a ut deserunt harum officiis mollitia expedita animi, ipsa minus. Libero, voluptatibus. Omnis dolorem voluptatum quo similique modi sunt vero ut!</p>
						<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: relative; margin: auto;" id="msgGCon">
							¡Datos guardados correctamente!
						</div>
						<input type="submit" name="" id="" class="submit" value="Modificar" ng-click="InsertDCP(dcontactos.user_id)">
					</div>
				<div>
				<!--FIN Datos de contacto-->


				<!--Eleccion rubros-->
				<div class="rubros">
					<h3>Quiero postularme en:</h3>
					<h5>Podés elegir más de uno</h5>
					<?php include("rubros.php");?>
					<br>
					<h3>Bajo el cargo/puesto de:</h3>
					<div ng-repeat="puesto in data">
						<input type="text" name="" id="puestos" placeholder="Agregar puesto (si es más de uno, dividi con ',')" value="{{puesto.descripcion_puesto}}">
						<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: relative; margin: auto;" id="msgGCar">
							¡Datos guardados correctamente!
						</div>
					<input type="submit" name="" id="" class="submit" value="Modificar" ng-click="cargos(puesto.user_id)">
					</div>
				</div> <!--FIN Eleccion rubros-->

				<!--Antecedentes laborales-->
				<div>
					<div class="antecedentes">
						<h3>Antecedentes laborales</h3>
						<input type="hidden" name="" id="id_laboral" ng-model = "id_laboral">
						<span style="color: red; font-size: 14px;">{{valEmpresal}}</span>
						<input type="text" name="" id="empresa" placeholder="Empresa" ng-model="empresa">
						<span style="color: red; font-size: 14px;">{{valSector}}</span>
						<input type="text" name="" id="sector" placeholder="Sector" ng-model="sector">
						<span style="color: red; font-size: 14px;">{{valPuesto}}</span>
						<input type="text" name="" id="puesto-antecedente" placeholder="Puesto" ng-model="puesto">
						<span style="color: red; font-size: 14px;">{{valFinicio}}</span></br>
						<input type="date" name="" id="inicio" ng-model="inicio">
						<input type="date" name="" id="fin" ng-model="fin">
						<textarea name="" id="descripcion" ng-model="descripcion" placeholder="Descripción de tareas (230 caracteres)" maxlength="230"></textarea>
						<br><input type="hidden" ng-model = "usuario_global">
						<input type="submit" name="" id="" class="submit" value="Agregar" ng-click="antLab();">
						<!--<input type="submit" name="" id="" class="submit submit-otro" value="Agregar">-->
					</div>
				</div>

				<div style="font-size: 12px; border: 1px solid black; padding: 5px; display: none;" id="contLab">
					<table border="0" ng-repeat ="Dlaboral in dataD">
						<tr>
							<td style="width: 50%; padding: 5px; vertical-align: bottom;"><h4>{{Dlaboral.empresa}}</h4></td>
							<td style="width: 15%; padding-left: 5px;"><input type="submit" name="" id="" class="submit submit-otro" value="Modificar" ng-click="buscarDL($index)"></td>
							<td style="width: 15%; padding-left: 5px;"><input type="submit" name="" id="" class="submit submit-otro" value="Eliminar" ng-click="eliminarDL(Dlaboral.id_laboral)"></td>
						</tr>
					</table>
				</div></br>
				<!--FIN Antecedentes laborales-->


				<!--Formación académica-->
				<div class="formacion">
					<h3>Formación académica</h3>
					<input type="hidden" id="id_academico" />
					<span style="color: red; font-size: 14px;">{{valNivelE}}</span>
					<select name="" id="nivel-educativo">
						<option value="" disabled selected hidden>Nivel educativo</option>
						<option value="primario">Primario</option>
						<option value="secundario">Secundario</option>
						<option value="terciario">Terciario</option>
						<option value="universitario">Universitario</option>
					</select>
					<span style="color: red; font-size: 14px;">{{valTitulo}}</span>
					<input type="text" name="" id="titulo" placeholder="Título">
					<span style="color: red; font-size: 14px;">{{valFechaI}}</span></br>
					<input type="date" name="" id="Ainicio"><input type="date" name="" id="Afin">
					<input type="submit" name="" id="" class="submit" value="Agregar" ng-click="CarDatosAcad()">
					<!--<input type="submit" name="" id="" class="submit submit-otro" value="Agregar">-->
				</div>

				<div style="font-size: 12px; border: 1px solid black; padding: 5px; display: none;" id="contStudy">
					<table border="0" ng-repeat ="academico in dataT">
						<tr>
							<td style="width: 50%; padding: 5px; vertical-align: bottom;"><h4>{{academico.nivel}}</h4></td>
							<td style="width: 15%; padding-left: 5px;"><input type="submit" name="" id="" class="submit submit-otro" value="Modificar" ng-click="ModifAcad($index)"></td>
							<td style="width: 15%; padding-left: 5px;"><input type="submit" name="" id="" class="submit submit-otro" value="Eliminar" ng-click="eliminarDA(academico.id_academico)"></td>
						</tr>
					</table>
				</div></br>


				<!--FIN Formación académica-->

				<!--IDIOMAS-->
				<div class="idiomas" ng-repeat="idioma in data">
					<h3>Idiomas que domina</h3>
					<span style="color: red;">{{MsgIdm}}</span>
					<input type="text" name="idioma" id="puestos" value="{{idioma.idioma}}" placeholder="Agregar idiomas (si es más de uno, dividi con ',')">
					<div style="background:#CEF6CE; max-width: 400px; text-align: center; display: none; position: relative; margin: auto; padding: 5px;" id="msgIdioma">
						¡Datos guardados correctamente!
					</div>
					<input type="submit" name="" id="" class="submit" value="Modificar" ng-click="CargIdioma()">
				</div><!--FIN IDIOMAS-->

				<!--Carnet de conducir-->
				<div class="carnet">
					<h3>Carnet de conducir</h3>
					<input type="hidden" id="id_carnets"></br>
					<input type="checkbox" name="checCC" id="checkeador" class="tiene-carnet"> Sí <span style="color: red;">{{valCC}}</span><br>
					<span style="color: red;">{{valCCTt}}</span><br>
					<select name="" id="categoria-carnet">
						<option value="" disabled selected hidden>Categoría</option>
						<option value="profesional">Profesional</option>
						<option value="b1">B1</option>
						<option value="b2">B2</option>
					</select>
					<span style="color: red;">{{valCCV}}</span><br>
					<input type="date" name="" id="vencimiento">
					<input type="submit" name="" id="" class="submit" value="Agregar" ng-click="CargarCC()">
				</div>


				<div style="font-size: 12px; border: 1px solid black; padding: 5px; display: none;" id="contCarnets">
					<table border="0" ng-repeat="carnets in dataC">
						<tr>
							<td style="width: 50%; padding: 5px; vertical-align: bottom;"><h4>{{carnets.tipo}}</h4></td>
							<td style="width: 15%; padding-left: 5px;"><input type="submit" name="" id="" class="submit submit-otro" value="Modificar" ng-click="ModificarCarnets($index)"></td>
							<td style="width: 15%; padding-left: 5px;"><input type="submit" name="" id="" class="submit submit-otro" value="Eliminar" ng-click="EliminarCarnets(carnets.idcarnets)"></td>
						</tr>
					</table>
				</div></br>

				<!--FIN Carnet de conducir-->

				<!--Disponibilidad-->
				<div class="disponibilidad" ng-repeat="disponib in data">
					<h3>Disponibilidad</h3>
					<span style="color: red;">{{valDisp}}</span><p>
					<p><input type="checkbox" name="fullTime" id="checkeador" ng-checked="{{disponib.fulltime}}"> Full Time
					</p>

					<p><input type="checkbox" name="parTime" id="checkeador" ng-checked="{{disponib.partime}}"> Part Time
					</p>

					<p><input type="checkbox" name="event" id="checkeador" ng-checked="{{disponib.eventual}}"> Eventual
					</p>

					<p><input type="checkbox" name="temp" id="checkeador" ng-checked="{{disponib.temporal}}"> Temporal
					</p>

					<p><input type="checkbox" name="fds" id="checkeador" ng-checked="{{disponib.finSemana}}"> Fines de semana
					</p>
					<br>
					<div style="background:#CEF6CE; max-width: 400px; text-align: center; display: none; position: relative; margin: auto; padding: 5px;" id="msgDispo">
						¡Datos guardados correctamente!
					</div>
					<input type="submit" name="" id="" class="submit" value="Agregar" ng-click="ModificarDispo()">
				</div>










			</div>
		</article>
		<?php include("sidebar.php");?>
		</div>

</section>

</div>

<?php include("seguinos-redes.php");?>

<?php include("publicidades-ancho.php");?>

<?php include("footer.php");?>

<?php
?>
<?php include("header.php");?>


<?php
 	$id_oferta=$_REQUEST['id'];
	$oferta=load_oferta_completa($id_oferta,$conexion);
	//print_r($oferta);
?>

<section id="ver-oferta-completa">
<div id="contenedor">
	<article>
	  <h2>Oferta completa</h2>
	  <p class="id">OM-000584D</p>
		<div id="header-login" class="empresa-logeada">
				<!--foto de perfil-->
				<div class="perfil-foto">
					<img src="<?php echo $oferta['image']; ?>" width="200px" alt="Foto Perfil">
				</div>
				<!--FIN foto de perfil-->

				<!--Datos personales-->
				<div class="datos-personales-login">
					<p class="nombre"><?php echo $oferta['name']; ?></p>
					<p class="numero-razon-social"><?php echo $oferta['cuil']; ?></p>
					<p class="pais"><?php echo $oferta['pais']; ?></p>
					<p class="provincia"><?php echo $oferta['provincia']; ?></p>
					<input type="submit" name="" id="" class="submit-otro" value="ver perfil"><a href=""><p class="denunciar">DENUNCIAR EMPRESA</p></a>
				</div>
				<!--FIN Datos personales-->

		</div>


		<?php include("slide.php");?>

			<!--Rubro de la oferta-->
			<div class="rubros">
				<h3>Rubros de la oferta</h3>
				<?php echo rubros_seleccionados($id_oferta,$conexion); ?>
			</div>

			<!--Puesto y cantidad de la oferta-->
		<div class="detalles">
			<div class="puestos">
				<h3>Tipo de puesto</h3>
				<!--Selección de puestos-->
				<p><?php echo $oferta['tipo_de_puesto']; ?></p>
			</div>

				<!--Selección de puestos-->
				<div class="cantidad-puestos">
					<h3>Cantidad</h3>
					<p><?php echo $oferta['cantidad']; ?></p>
				</div>

				<!--descripcion del puesto-->
				<div>
					<h3>Descripción de las tareas a realizar</h3>
					<p><?php echo $oferta['descripcion']; ?></p>
				</div>

				<!--experiencia-->
				<?php if($oferta['experiencia']=='true'){ ?>
					<div class="experiencia">
						<h3>Experiencia</h3>
						<p>Con experiencia comprobable</p>
					</div>
				<?php } ?>

			<!--detalles de la persona-->
			<div class="formacion">
				<h3>Formación académica</h3>
				<p><?php echo $oferta['nivel_educativo']; ?></p>
			</div>

			<!--Lugar de residencia-->
			<div class="residencia">
				<h3>Lugar de residencia</h3>
				<span>
					<p><?php echo $oferta['residencia_pais']; ?></p>
					<p><?php echo $oferta['residencia_provincia']; ?></p>
				</span>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d107216.43757617737!2d-68.79701787782601!3d-32.88458453124367!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x967e0e18d060ab8b%3A0x9676b699b4d10b1b!2sGuaymall%C3%A9n%2C+Mendoza!5e0!3m2!1ses!2sar!4v1508768807625"  allowfullscreen></iframe>
			</div>

			<!--debe ser nomade?-->
			<?php if ($oferta['pais']=='true'){ ?>
				<div class="datos-contacto">
					<span>Debe ser nómade</span>
					<p class="aclaracion">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat a ut deserunt harum officiis mollitia expedita animi, ipsa minus. Libero, voluptatibus. Omnis dolorem voluptatum quo similique modi sunt vero ut!</p>
				</div>
		  <?php } ?>

			<!--otras opciones-->
			<div class="edad">
				<h3>Edad</h3>
				<p>Entre <?php echo $oferta['edad']; ?> años</p>
			</div>

			<div class="genero">
				<h3>Género</h3>
				<p><?php echo ucwords($oferta['genero']); ?></p>
			</div>

			<div class="idioma">
				<h3>Idioma</h3>
				<p><?php echo $oferta['idioma']; ?></p>
			</div>

				<!--Carnet de conducir-->
				<?php if ($oferta['carnet_conducir']=='true'){ ?>
					<div class="carnet">
						<h3>Carnet de conducir</h3>
						<p><?php echo $oferta['carnet_categoria']; ?></p>
					</div> <!--FIN Carnet de conducir-->
				<?php } ?>

				<!--Remuneración pretendida-->
				<div class="remuneracion">
					<h3>Remuneración mensual</h3>
					<div class="pesos">$ <span><?php echo $oferta['remuneracion']; ?></span></div>

				</div>
			<br><br><br>

			<input type="hidden" id="id_oferta" name="id_oferta" value="<?php echo $id_oferta; ?>">
			<input type="hidden" id="postulante" name="postulante" value="<?php echo $_SESSION['user']; ?>">
			<input type="submit" name="postular" id="postular" class="submit" value="Postularme">
			<span id="resultPost"></span>

			<div class="redes" style="margin-top:15px">
				<a href=""><img src="../img/facebook.png" alt=""></a>
				<a href=""><img src="../img/twitter.png" alt=""></a>
				<a href=""><img src="../img/whatsapp.png" alt=""></a>
			</div>


		</div>
		</article>
		<?php include("sidebar.php");?>
	</div>
</section>

<?php include("seguinos-redes.php");?>

<?php include("publicidades-ancho.php");?>

<?php include("footer.php");?>

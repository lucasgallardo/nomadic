<?php

/**
 * Resultados de ofertas *
**/

?>


<section class="mis-postulaciones">
	<div id="contenedor">
		<h2>Mis postulaciones</h2>
		<input type="checkbox" name="" id="postulacion-activa">
		<input type="checkbox" name="" id="postulacion-vencidas">

		<label for="postulacion-activa" class="label-activa">
			<h3>ACTIVAS</h3>
		</label>
		<label for="postulacion-vencidas" class="label-vencida">
			<h3>VENCIDAS</h3>
		</label>
		<hr>


		<article class="activas">
			<?php include("../control/resultado-ofertas-activas.php");?>
		</article>

		<article class="vencidas">
			<?php // include("resultados-ofertas-inactivas.php");?>
		</article>

	</div>
</section>


<!---script para deschekear-->
				<script>
					$("input#postulacion-activa").click(function() {
					  $("input#postulacion-vencidas").attr("checked", false); //deschekear
					  $(this).attr("checked", true);  //checkeada esta sola
					});

					$("input#postulacion-vencidas").click(function() {
					  $("input#postulacion-activa").attr("checked", false); //deschekear
					  $(this).attr("checked", true);  //checkeada esta sola
					});

					$("#ver-mis-postulaciones-boton").click(function() {
					  $("input#postulacion-activa").attr("checked", true);
					});


				</script>

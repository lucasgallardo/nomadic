<?php
if (isset($_SESSION['user'])) { //valido que el usuario esté logueado
?>
<style>
	nav.sticky div a.login-nav{
	display: none;
}
</style>

<div id="header-login">
	<div id="contenedor">
		<!--foto de perfil-->
		<div class="perfil-foto">
			<img src="<?php echo $data['image'] ?>" width="170px" alt="Foto Perfil">
		</div>
		<!--FIN foto de perfil-->

		<!--Datos personales-->
		<div class="datos-personales-login">
			<p class="nombre"><?php echo $data['name']; ?></p>
			<p class="edad"><?php echo $data['edad']; ?> años</p>
			<p class="cuil"><?php echo $data['cuil']; ?></p>
			<p class="genero"><?php echo $data['genero']; ?></p>
			<p class="nacionalidad"><?php echo $data['pais']; ?></p>
			<a href="page-registro-persona.php"><input type="submit" name="" id="" class="submit-otro" value="ver perfil"></a>
			<a href="../control/exit.php"><input type="submit" name="" id="cerrar-sesion" class="submit-otro" value="cerrar sesión"></a>
		</div>
		<!--FIN Datos personales-->

		<!--Mis Postulaciones-->
		<a href="person_profile_p.php" id="ver-mis-postulaciones-boton">
			<div class="mis-postulaciones">
				<h2>35</h2>
				<h4>postulaciones<br>activas</h4>
				<button id="boton">VER MIS POSTULACIONES</button>
			</div>
		</a>
		<!--FIN Mis Postulaciones-->

		<!--Mis Ofertas-->
		<div class="mis-ofertas">
			<?php include("cifra-ofertas.php");?>
		</div>
		<!--FIN Mis Ofertas-->
	</div>
</div>
<?php
}
?>

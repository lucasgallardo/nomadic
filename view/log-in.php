<?php

/**
 * login *
**/


?>
<section class="login" id="login-modal">
	<div id="contenedor">
		<div class="vertical"></div>
		<article>
			<input type="checkbox" id="registro" name="registro">

			<a href="#" class="cerrar">CERRAR</a>
			<p class="sumate"><img src="../img/nomadic-resources.png" alt="Nomadic Resources" ></p>

			<label for="registro" class="registrarse"><div id="boton"></div></label>

			<div class="formulario-registro">
				<input type="checkbox" name="" id="checkeador" class="postulante"> Soy postulante nomadic
				<br>
				<input type="checkbox" name="" id="checkeador" class="empresa"> Soy empresa
				<br>

				<!---script para deschekear-->
				<script>
					$("input.postulante").click(function() {
					  $("input.empresa").attr("checked", false); //deschekear
					  $(this).attr("checked", true);  //checkeada esta sola
					});

					$("input.empresa").click(function() {
					  $("input.postulante").attr("checked", false); //deschekear
					  $(this).attr("checked", true);  //checkeada esta sola
					});

					$("input#registro").click(function() {
					  $("input.postulante").attr("checked", true);  //checkeada esta sola
					});
				</script>
				<style>




					summary::-webkit-details-marker {
				    display: none;
				}
					summary:hover {
						cursor:pointer; cursor: hand
					}


				</style>

				<div class="inputs-empresa">
					<input type="text" name="" id="nombre-empresa" placeholder="Nombre de la empresa" required>
 					<input type="text" name="" id="cuit" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="CUIT o CUIL" required >
 				</div>
				<div class="inputs-postulantes">
					<input type="text" name="" id="nombre" placeholder="Nombre" required>
					<input type="text" name="" id="apellido" placeholder="Apellido" rquired>
					<input type="email" name="" id="email" placeholder="email" required>
					<input type="password" name="" id="password" placeholder="Contraseña" required>
					<input type="password" name="" id="confirmar-password" placeholder="Confirmar contraseña">
					<p class="notas">Hay que agregar aca algun tipo de captcha o "no soy robot"</p>
					<div class="g-recaptcha" data-sitekey="6LcT-DQUAAAAABs3dwak3OrfRDVVYnjX0B8_gBd8"></div>
					<input type="checkbox" name="" id="condicionesterminos" class="condiciones"> Acepto los términos y condiciones legales
					<input type="submit" name="" id="registrar" class="submit" value="CREAR USUARIO">
					<span id="resultRegistro"></span>
				</div>
			</div>



			<div class="formulario-log">
				<input type="text" name="usuario" id="usuario" placeholder="Usuario" required>
				<input type="password" name="pass" id="pass" placeholder="Contraseña" required>
				<input type="submit" name="" id="ingresar" class="submit" value="INGRESAR">
				<span id="result"></span>
			</div>
			<details>
				<summary><p><a>Olvidé mi contraseña</a></p></summary>
				<p>
					<hr>
						<label>Recuperar Clave</label><br>
						<sub>Ingrese su dirección de correo para reenviarle el link de recuperación de clave</sub>
						<input type="email" id="mailrecupera" name="mailrecupera" placeholder="Correo" required autofocus>
						<input type="submit" name="" id="correo_recupera" class="submit" value="Enviar mail">
						<span id="resultRecupera"></span>
				</p>
			</details>
		</article>
	</div>
</section>

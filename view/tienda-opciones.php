<?php

/**
 * opciones de la tienda
**/
include_once '../control/conexion.php';
include_once '../control/functions.php';
?>
<?php
/**
 * pagina de empresa logeado *
**/
// session_start();
// include_once('../control/conexion.php');
// include_once('../control/functions.php');
//
// $data=load_company($_SESSION["user"],$conexion);
?>
<?php include("header.php");?>
<!-- <style>
	nav.sticky div a.login-nav{
	display: none;
}
</style> -->
<br>
<br>

<!-- <p class="notas">Suponiendo que no tienen más descargas de cv acceseibles, al querer bajar alguno aparece lo siguiente:</p> -->

		<!--inicia opciones de vista o descarga de cv-->
		<section id="tienda-opciones">
			<div id="contenedor">
				<h2>Tienda de CVs</h2>
				<p>Gracias por usar <strong>Nomadic Resources</strong>. Seleccione la opción que más le convenga para poder visualizar o descargar los currículums del sitio</p>
				<article class="opcion-cv">
					<?php $date=rates_loader(1,$conexion); ?>
					<img src="../img/ico-persona.png" alt="">
					<h1><?php echo $date['number']; ?></h1>
					<h4>CURRÍCULUM<br>VITAE</h4>
					<p><?php echo $date['description'] ?></p>
					<h3>$ <?php echo $date['price'] ?></h3>
					<input type="submit" name="" id="pagar" value="PAGAR" class="submit">
					<!-- <a mp-mode="dftl" href="https://www.mercadopago.com/mla/checkout/start?pref_id=103502707-92e8c0f2-f1ca-49c0-b866-0695176f5c98" name="MP-payButton" class='blue-ar-l-rn-none'>Pagar</a> -->
					<!-- <script type="text/javascript">
						(function(){
							function $MPC_load(){
								window.$MPC_loaded !== true && (function(){
									var s = document.createElement("script");
									s.type = "text/javascript";
									s.async = true;
									s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";
									var x = document.getElementsByTagName('script')[0];
									x.parentNode.insertBefore(s, x);
									window.$MPC_loaded = true;})();
								}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
					</script> -->
				</article>

				<article class="opcion-cv">
					<?php $date=rates_loader(2,$conexion); ?>
					<img src="../img/ico-personas.png" alt="">
					<h1><?php echo $date['number']; ?></h1>
					<h4>CURRÍCULUM<br>VITAE</h4>
					<p><?php echo $date['description'] ?></p>
					<h3>$ <?php echo $date['price'] ?></h3>
					<input type="submit" name="" id="pagar" value="PAGAR" class="submit">
					<div id="paypal-button"></div>

		  <!-- <script>
		    paypal.Button.render({
		      env: 'sandbox', // Or 'production',

		      commit: true, // Show a 'Pay Now' button

		      style: {
						label: 'checkout',
		        size:  'small',    // small | medium | large | responsive
		        shape: 'pill',     // pill | rect
		        color: 'gold'      // gold | blue | silver | black
		      },



					client: {
							 sandbox:    'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
							 production: '<insert production client id>'
					 },

		      payment: function(data, actions) {
						return actions.payment.create({
									 payment: {
											 transactions: [
													 {
															 amount: { total: '0.01', currency: 'USD' }
													 }
											 ]
									 }
							 });
		      },

		      onAuthorize: function(data, actions) {
						return actions.payment.execute().then(function() {
			                window.alert('Payment Complete!');
			            });
		      },

		      onCancel: function(data, actions) {
		        /*
		         * Buyer cancelled the payment
		         */
		      },

		      onError: function(err) {
		        /*
		         * An error occurred during the transaction
		         */
		      }
		    }, '#paypal-button');
		  </script> -->


				</article>

				<article class="opcion-cv">
					<?php $date=rates_loader(3,$conexion); ?>
					<img src="../img/ico-personas.png" alt="">
					<h1><?php echo $date['number']; ?></h1>
					<h4>CURRÍCULUM<br>VITAE</h4>
					<p><?php echo $date['description']; ?></p>
					<h3>$ <?php echo $date['price']; ?></h3>
					<input type="submit" name="" id="pagar" value="PAGAR" class="submit">
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
		<input type="hidden" name="cmd" value="_s-xclick">
		<input type="hidden" name="hosted_button_id" value="BBFCK3BEK9K9A">
		<!-- <input type="image" src="" width="100px" border="0" name="submit" alt="Pagar"> -->
		<!-- <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1"> -->
		</form>

				</article>

				<article class="opcion-cv">
					<?php $date=rates_loader(4,$conexion); ?>
					<img src="../img/ico-personas.png" alt="">
					<h1><?php echo $date['number']; ?></h1>
					<h4>CURRÍCULUM<br>VITAE</h4>
					<p><?php echo $date['description']; ?></p>
					<h3>$ <?php echo $date['price']; ?></h3>
					<input type="submit" name="" id="pagar" value="PAGAR" class="submit">
				</article>

				<article class="opcion-cv opcion-cv-temporal">
					<?php $date=rates_loader(5,$conexion); ?>
					<img src="../img/ico-calendario.png" alt="">
					<h1><?php echo $date['number']; ?></h1>
					<h4>MESES<br><br></h4>
					<p><?php echo $date['description']; ?></p>
					<h3>$ <?php echo $date['price']; ?></h3>
					<input type="submit" name="" id="pagar" value="PAGAR" class="submit">
				</article>
			</div>
		</section>

<!-- <p class="notas">oferta completa</p>
<?php // include("oferta-completa.php");?> -->


<?php include("seguinos-redes.php");?>

<?php include("publicidades-ancho.php");?>

<?php include("footer.php");?>

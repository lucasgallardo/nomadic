<?php
/**
 * lo importante en este php es lo de GENERAR OFERTAS! pero lo armamos completo para visualizar como quedaria *
**/
?>
<?php include("header.php");?>
<section id="generacion-de-oferta">
	<div id="contenedor">
		<article>
			<h2>Generar oferta</h2>
			<?php include("slide.php");?>
			<form method="post" id="formulario" enctype="multipart/form-data">
				<input type="submit" name="" id="" class="submit" value="subir fotos">
				<!-- Subir imagen: <input type="file" name="file"> -->
			</form>

			 <form class="" action="../control/guarda_oferta.php" method="POST">
si usuario no completó el perfil no puede generar una oferta!!!
				 <input type="hidden" name="id_empresa" value="<?php echo $data['id']; ?>">
				 <input type="hidden" name="codigo" value="<?php echo $data['name']; ?>">

			<!--rubros-->
			<div class="rubros">
				<h3>Rubro de la oferta</h3>
				<h5>Podés elegir más de uno</h5>
				<?php include("rubros2.php");?>
			</div>

			<!--Puesto y cantidad de la oferta-->
			<div class="puestos">
				<h3>Tipo de puesto</h3>
				<!--Selección de puestos-->
				<input type="text" name="tipo_de_puesto" id="puesto" placeholder="Puesto a ocupar" required>
				<input type="checkbox" id="checkeador" name="pasante"> Es pasante
				<!--<p class="notas">Al tildar si es pasante debe reflejarse en el resultado como un :before content:'para pasante', ejmplo: Botones para pasante</p>-->
			</div>

				<!--Selección de puestos-->
				<div class="cantidad-puestos">
					<h3>Cantidad</h3>
					<input type="number" name="cantidad" id="cantidad" placeholder="XX" min="1">
				</div>

				<!--descripcion del puesto-->
				<div>
					<h3>Descripción de las tareas a realizar</h3>
					<textarea name="descripcion" id="" placeholder="Sea lo más claro posible para describir las tareas que deberá realizar la persona"></textarea>
				</div>

				<!--experiencia-->
				<div>
					<h3>Experiencia</h3>
					<span><input type="checkbox" name="experiencia" id="checkeador">Al tildar esta opción indica que la persona debe tener experiencia en el rubro y es excluyente a la hora de ver los perfiles con coincidencias</span>
				</div>

			<!--detalles de la persona-->
			<div class="formacion">
				<h3>Formación académica</h3>
				<select name="nivel_educativo" id="nivel-educativo">
						<option value="" disabled selected hidden>Nivel educativo</option>
						<option value="primario">Primario</option>
						<option value="secundario">Secundario</option>
						<option value="terciario">Terciario</option>
						<option value="universitario">Universitario</option>
						<option value="indistinto">Es indistinto</option>
					</select>
			</div>
			<!--Lugar de residencia-->
			<div class="residencia">
				<h3>Lugar de residencia</h3>
				<input type="text" name="residencia_pais" id="pais" placeholder="País">
				<input type="text" name="residencia_provincia" id="provincia" placeholder="Provincia">
				<input type="button" class="submit-otro" onclick="mostrarMapa(), initMap()" name="" value="Marcar zona en el mapa" /></p>
			</div>
		</article>
			<div id="CMapa" style="display: none;">
				<span style="color: #841F2F; font-size: 16px;"><b>¡Deberá arrastrar el marcador del mapa hasta la ubicación de su empresa!</b></span></p>
				<div id="contMapa"></div>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<input type="hidden" id="latitud" name="latitud">
				<input type="hidden" id="longitud" name="longitud">
			</div>
			<article>
			<!--debe ser nomade?-->
			<div class="datos-contacto">
				<span><input type="checkbox" name="nomade" id="checkeador"> Debe ser nómade</span>
				<p class="aclaracion">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat a ut deserunt harum officiis mollitia expedita animi, ipsa minus. Libero, voluptatibus. Omnis dolorem voluptatum quo similique modi sunt vero ut!</p>
			</div>

			<!--otras opciones-->
			<div class="edad">
				<h3>Edad</h3>
				<select name="edad" id="rango-edades">
					<option value="18 y 25">18-25</option>
					<option value="26 y 35">26-35</option>
					<option value="36 y 45">36-45</option>
					<option value="+45">+45</option>
					<option value="indistinto">Es indistinto</option>
				</select>
			</div>

			<div class="genero">
				<h3>Género</h3>
				<select name="genero" id="genero">
					<option value="indistinto">Es indistinto</option>
					<option value="masculino">Masculino</option>
					<option value="femenino">Femenino</option>
					<option value="otro">Otro</option>
				</select>
			</div>

			<div class="idioma">
				<h3>Idioma</h3>
				<input type="text" name="idioma" id="idioma" placeholder="Escribra los idiomas separandolos con ','">
			</div>

				<!--Carnet de conducir-->
				<div class="carnet">
					<h3>Carnet de conducir</h3>
					<input type="checkbox" name="carnet_conducir" id="checkeador" class="tiene-carnet"> Sí <br>
					<select name="carnet_categoria" id="categoria-carnet">
						<option value="" disabled selected hidden>Categoría</option>
						<option value="profesional">Profesional</option>
						<option value="comunB1">B1</option>
						<option value="comunB2">B2</option>
					</select>
				</div> <!--FIN Carnet de conducir-->

				<!--Remuneración pretendida-->
				<div class="remuneracion">
					<h3>Remuneración mensual</h3>
					<!-- <input type="range" id="rangoplata" name="" value="120" min="100" max="900" onchange="updateTextInput(this.value);"> -->
					<div class="pesos">$<input type="text" name="remuneracion" id="rango"></div>
					<!-- <script>
						function updateTextInput(val) {
				          document.getElementById('rango').value=val;
				        }
				    </script> -->
				</div>
				<br><br><br>

			<input type="submit" name="guardar_oferta" id="" class="submit" value="Generar oferta">
		</form>
		</article>
		<?php //include("sidebar.php");?>
	</div>
</section>

<script>
     $(function(){
        $("input[name='file']").on("change", function(){
            var formData = new FormData($("#formulario")[0]);
            var ruta = "ajax-imagen.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    $("#respuesta").html(datos);
                }
            });
        });
     });
		 /*FUNCIONES PARA MARCAR EL MAPA*/
	function mostrarMapa(){
		$('#CMapa').toggle('500');

		document.getElementById('contMapa').innerHTML="</article><div id='mapaO' style='width: 100%; height: 10%; position: absolute;'></div><article>";
	}
	var marker;
	var coords = {};
	 function initMap(){
					navigator.geolocation.getCurrentPosition(
			function(position){
				coords = {
				lng: position.coords.longitude,
				lat: position.coords.latitude
			};
				tot ={
							lati: document.getElementById('latitud').value,
							lngi: document.getElementById('longitud').value
			};
				if(tot.lati =="" && tot.lngi ==""){
					setMapa(coords);
			}else{
					setMapaD(tot);
				}
			},function(error){
				//console.log(error);
				switch(error.code) {
	 				 case error.PERMISSION_DENIED:
	 						 /* El usuario denegó el permiso para la Geolocalización.*/
							 //alert('No se puede acceder a la ubicación')
							setMapaSG();
	 						 break;
	 				 case error.POSITION_UNAVAILABLE:
	 						 /* La ubicación no está disponible.*/
							 setMapaSG();
	 						 break;
						 }
			})
		}

		function setMapaSG(){
			var mapSG = new google.maps.Map(document.getElementById('mapaO'),
			{
				zoom: 5,
				center: new google.maps.LatLng(-38.416097, -63.616671999999994),

			});
		marker = new google.maps.Marker({
			map: mapSG,
			draggable: true,
			animation: google.maps.Animation.DROP,
			position: new google.maps.LatLng(-38.416097, -63.616671999999994),
			});

			marker.addListener('click', toggleBounce);
			marker.addListener('dragend', function(event){
				document.getElementById('latitud').value = this.getPosition().lat();
				document.getElementById('longitud').value = this.getPosition().lng();
				});
			}

	function setMapa(coords){

		var map = new google.maps.Map(document.getElementById('mapaO'),
		{
			zoom: 13,
			center: new google.maps.LatLng(coords.lat,coords.lng),
		});
	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: new google.maps.LatLng(coords.lat, coords.lng),
		});

		marker.addListener('click', toggleBounce);
		marker.addListener('dragend', function(event){
			document.getElementById('latitud').value = this.getPosition().lat();
			document.getElementById('longitud').value = this.getPosition().lng();
			});
		}

	function setMapaD(){
		var mapD = new google.maps.Map(document.getElementById('mapaO'),
		{
			zoom: 13,
			center: new google.maps.LatLng(tot.lati, tot.lngi),

		});
	marker = new google.maps.Marker({
		map: mapD,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: new google.maps.LatLng(tot.lati, tot.lngi),
		});

		marker.addListener('click', toggleBounce);
		marker.addListener('dragend', function(event){
			document.getElementById('latitud').value = this.getPosition().lat();
			document.getElementById('longitud').value = this.getPosition().lng();
			});
		}

	function toggleBounce(){
		if(marker.getAnimation()!== null){
			marker.setAnimation(null)
			}else{
				marker.setAnimation(google.maps.Animation.BOUNCE);
				}
		}
</script>

<?php include("seguinos-redes.php");?>

<?php include("publicidades-ancho.php");?>

<?php include("footer.php");?>

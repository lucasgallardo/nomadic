<?php
session_start();
include_once('../control/conexion.php');
include_once('../control/functions.php');

if(tipo_de_usuario($conexion,$_SESSION['user'])==1){ //nomade
	$data=load_person($_SESSION["user"],$conexion);
}elseif (tipo_de_usuario($conexion,$_SESSION['user'])==2) { //empresa
	$data=load_company($_SESSION["user"],$conexion);
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Nomadic Resources</title>
	<!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->

	<link href="../img/icono.png" rel="shortcut icon" type="image/png" width="32px">

	<meta name="expires" content="text">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

	<!--para buscadores-->
	<meta name="author" content="Nomadic Resources" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="index, follow" />

	<!--si se ve bien em productos apple pongo lo siguiente-->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta http-equiv="expires" content="Thu Oct 14 16:06:35 CEST 2027">


	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->

	<!-- Siempre se coloca primero normalize.css -->
	<link rel="stylesheet" href="../css/normalize.css" type="text/css">
	<!--estos dos estilos siguientes son solo para el carrusel-->
	<link rel="stylesheet" type="text/css" href="../css/slick.css">
	<link rel="stylesheet" type="text/css" href="../css/slick-theme.css">
	<link rel="stylesheet" href="../css/estilos.css" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
<!-- script para recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- AngularJS -->
	<script src="../js/angular.min.js"></script>
	<!-- Fin AngularJS -->
	<!-- Llamada google maps -->
	<script type="text/javascript" src= "https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCwsZPCb9kiLL58NWCc_dfXsQNjnf8ukYM"></script>
	<!-- fin llamada api google maps -->

<body>
<div id="pantalla">
	<header>
		<h1><img src="../img/nomadic-resources.png" alt="Nomadic Resources"></h1>
	</header>

</div>
<?php include("nav.php");?>
<div id="pantalla">

	<?php
		if(tipo_de_usuario($conexion,$_SESSION['user'])==1) //nomade
			include_once("header-persona-login.php");
		elseif (tipo_de_usuario($conexion,$_SESSION['user'])==2) { //empresa
			include_once("header-empresa-login.php");
		}else {
			if (strpos($_SERVER['REQUEST_URI'],"index.php")==0) { //si no está logueado que redireccione al index
				header('Location:index.php');//solo si no está en el index
			}
		}
	?>

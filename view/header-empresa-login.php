<?php
if (isset($_SESSION['user'])) { //valido que el usuario esté logueado
?>
<style>
	nav.sticky div a.login-nav{
	display: none;
}
</style>

<div id="header-login" class="empresa-logeada">
	<div id="contenedor">
		<!--foto de perfil-->
		<div class="perfil-foto">
			<img src="<?php echo $data['image'] ?>" width="170px" alt="Foto Perfil">
		</div>
		<!--FIN foto de perfil-->

		<!--Datos personales-->
		<div class="datos-personales-login">
			<p class="nombre"><?php echo $data['name']; ?></p>
			<p class="razon-social"><?php echo $data['razon_social']; ?></p>
			<p class="numero-razon-social"><?php echo $data['cuil']; ?></p>
			<p class="pais"><?php echo $data['pais']; ?></p>
			<p class="provincia"><?php echo $data['provincia']; ?></p>
			<a href="page-registro-empresa.php"><input type="submit" name="" id="" class="submit-otro" value="ver perfil"></a>
			<a href="../control/exit.php"><input type="submit" name="" id="cerrar-sesion" class="submit-otro" value="cerrar sesión"></a>
		</div>
		<!--FIN Datos personales-->

		<!--Mis Postulaciones-->
		<a href="company_profile_o.php" id="ver-mis-postulaciones-boton">
			<div class="mis-postulaciones mis-postulantes">
				<h2><?php echo postulantesTotales($_SESSION["user"],$conexion); ?></h2>
				<h4>postulantes<br>en tus ofertas</h4>
				<button id="boton">VER MIS OFERTAS</button>
			</div>
		</a>
		<!--FIN Mis Postulaciones-->

		<!--Mis Ofertas-->
		<div class="mis-ofertas">
			<?php include("cifra-nomades.php");?>
		</div>
		<!--FIN Mis Ofertas-->
	</div>

	<a href="page-generar-oferta.php">
		<div class="generar-oferta">
			<div id="contenedor">
				<img src="img/ico-ofertas.png" alt="">
				<h4 class="submit">GENERAR NUEVA OFERTA LABORAL</h4>
			</div>
		</div>
	</a>
</div>
<?php
} else {
	//header('Location: index.php');
}
?>

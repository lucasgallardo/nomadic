<?php

/**
 * pagina de alguien logeado *
**/

?>
<?php include("header.php");?>
  <script src="https://www.paypalobjects.com/api/checkout.js"></script>
<style>
	nav.sticky div a.login-nav{
	display: none;
}
</style>

<?php include("header-persona-login.php");?>

<br><br><br><br>
<p class="notas">el header de una persona logeada siempre esta arriba y lo conecta a 3 lugares, modificar sus datos personales, ver donde se postuló y una cifra con el número de ofertas más similares a su perfil</p>

<p class="notas">si apreta en VER PERFIL abre la pagina "PAGE-REGISTRO-PERSONA.php" para modificar, agregar o restas informacion</p>

<p class="notas">si apreta en buscar ofertas aparece nuevamente los filtros de busqueda "FILTROS-BUSQUEDA-OFERTAS.php" seguido de los "RESULTADOS-OFERTAS"</p>

<p class="notas">Acá se ve como se visualiza "MIS POSTULACIONES" que es igual a la busqueda, solo que se divide en las "vencidas" y las "activas" y se suma un "retirar postulacion" este archivo es "MIS-POSTULACIONES.php"</p>

<?php include("mis-postulaciones.php");?>

<section>
	<div id="contenedor">
		<article>

		</article>
	</div>
</section>

<p class="notas">Ahora el contenido si me hubiese logeado como empresa:</p>
<?php include("header-empresa-login.php");?>

<p class="notas">el header de una empresa logeada siempre esta arriba y lo conecta a 3 lugares, modificar sus datos personales (ver perfil), ver las personas que se postularon a sus ofertas laborales y una cifra con la cantidad de curriculums cargados en el sitio</p>

<p class="notas">si apreta en VER PERFIL abre la pagina "PAGE-REGISTRO-EMPRESA.php" para modificar, agregar o restas informacion</p>

<p class="notas">si apreta en BUSCAR PERSONAL aparece nuevamente los filtros de busqueda "FILTROS-BUSQUEDA-PERSONAS.php" seguido de los "RESULTADOS-PERSONAS"</p>

<p class="notas">Acá se ve como se visualiza "MIS OFERTAS" que es igual a la busqueda, sólo que se suma una pestaa superior desplegable con la cantidad de personas postuladas y la posibilidad de descargar todos los cv de un solo click (si es que pagó anteriormente). Esto esta guardado en "MIS-OFERTAS.php"</p>

<?php include("mis-ofertas.php");?>

<p class="notas">EJEMPLO DE LAS FLECHAS para paginas</p>
<?php include("resultados-flechas.php");?>


<p class="notas">Suponiendo que no tienen más descargas de cv acceseibles, al querer bajar alguno aparece lo siguiente:</p>
<?php include("tienda-opciones.php");?>

<section>
	<div id="contenedor">
		<article>

		</article>
	</div>
</section>

<p class="notas">oferta completa</p>
<?php include("oferta-completa.php");?>


<?php include("seguinos-redes.php");?>

<?php include("publicidades-ancho.php");?>



<?php include("footer.php");?>

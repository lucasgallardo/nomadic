<?php
	session_start();
	$_SESSION["user"] = $_GET["id"];
	include("header.php");
	?>
<div ng-app="myApp" ng-controller = "contNomadicPers">
<!--<p class="notas">Esta página es la que debe aparecer si apretas en registrar, te llega el mail de confirmacion con link a ESTE sitio donde completas tu perfil (hasta donde quieras) de persona, en un archivo nombrado igual pero con "empresa" se encuentra el de empresa</p>-->
<section class="perfil-completo-persona">
	<div id="contenedor">
		<article>
			<!--<div class="perfil-foto">
				<div ng-repeat="foto in data">
					<img ng-src="{{foto.img}}" alt="Foto Perfil" id="fotosSP">
				</div>
			</div>-->
			<div class="perfil">
				<!--Datos personales-->
				<div ng-repeat="datosP in data" ng-cloak  class="datos-personales" id="perfil">
					</br>
					<input value="{{datosP.user_id}}" id="user_id" type="hidden">
          			<input value="{{datosP.user_name}}" type="text" id="nombre" placeholder="Nombre y Apellido" required readonly>
					<input value = "{{datosP.fecNac | date: 'dd/MM/yyyy'}}" type="text" name="" id=""  readonly>
					<input value ="{{datosP.edad}}" type="text" name="" id="edad" placeholder="Edad" readonly>
					<input value ="{{datosP.dni}}" type="text" name="" id="dni" placeholder="Número de documento" readonly>
					<input value ="{{datosP.genero}}" type="text" name="" id="" placeholder="Edad" readonly>
					<input value ="{{datosP.nacionalidad}}" type="text" name="" id="nacionalidad" placeholder="Nacionalidad" readonly>
					<input value = "{{datosP.cuil}}" type="text" name="" id="cuil-persona" placeholder="CUIL" readonly>
				</div>
				<!--FIN Datos personales-->

				<!--Residencia-->
				<div ng-repeat="residencia in data">
					<div class="residencia">
						<h3>Residencia</h3>
					<table width="100%">
						<tr>
							<td><span><b>Calle:</b></span></td>
						</tr>
						<tr>
							<td><input type="text" name="" id="calle" value="Calle:{{residencia.calle}}" readonly></td>
						</tr>
					</table>
					<table width="100%">
						<tr>
								<td><b>Altura:</b></td>
								<td><b>&nbsp;&nbsp;Piso:</b></td>
								<td><b>&nbsp;&nbsp;Dpto:</b></td>
						</tr>
						<tr>
							<td><input type="text" name="" id="altura" placeholder="Altura" value="{{residencia.altura}}" readonly></td>
							<td>&nbsp;&nbsp;<input type="text" name="" id="piso" placeholder="Piso" value="{{residencia.piso}}" readonly></br></td>
							<td>&nbsp;&nbsp;<input type="text" name="" id="apartamento" placeholder="Dpto" value="{{residencia.dpt}}" readonly></td>
						</tr>
					</table>
					<table width="100%">
						<tr>
							<td><span><b>Pais:</b></span></td>
						</tr>
						<tr>
							<td><input type="text" name="" id="pais" placeholder="País" value="{{residencia.pais}}" readonly></td>
						</tr>
					</table>
					<table width="100%">
						<tr>
							<td><span><b>Provincia:</b></span></td>
						</tr>
						<tr>
							<td><input type="text" name="" id="provincia" placeholder="Provincia" value="{{residencia.provEst}}" readonly></td>
						</tr>
					</table>
					<table width="100%">
							<tr>
								<td><b>Departamento:</b></td><td><b>Código Postal:</b></td>
							</tr>
							<tr>
								<td><input type="text" name="" id="departamento" placeholder="Departamento" value="{{residencia.departamento}}" readonly></td>
								<td><input type="text" name="" id="cp" placeholder="Código Postal" value="{{residencia.codPostal}}" readonly></td>
							</tr>
					</table>
					</div>
				</div>
				<!--FIN Residencia-->

				<!--Datos de contacto-->
				<div ng-repeat="dcontactos in data">
					<div class="datos-contacto">
						<h3>Datos de contacto</h3>
						<table width="100%">
							<tr><td><b>Mail:</b><td></tr>
							<tr><td><input type="email" name="" id="mail" placeholder="@email" value="{{dcontactos.user_email}}" readonly></td></tr>
							<tr><td><b>Mail alternativo: </b></td></tr>
							<tr><td><input type="email" name="" id="mailAlt" value="{{dcontactos.mailAlternat}}" readonly></td></tr>
							<tr><td><b>Teléfono: </b></td></tr>
							<tr><td><input type="tel" name="" id="telefono" value="{{dcontactos.telefono}}" readonly></td></tr>
							<tr><td><b>Teléfono alternativo: </b></td></tr>
							<tr><td><input type="tel" name="" id="telefonoAlt"  value="{{dcontactos.telAlternat}}" readonly></td></tr>
							<tr><td><span><input type="checkbox" name="checkeador" id="checkeador" ng-checked="{{dcontactos.nomade}}" value="true" disabled> Soy nómade</span></td></tr>
						<!--<p class="aclaracion">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat a ut deserunt harum officiis mollitia expedita animi, ipsa minus. Libero, voluptatibus. Omnis dolorem voluptatum quo similique modi sunt vero ut!</p>-->
						</table>
					</div>
				<div>
				<!--FIN Datos de contacto-->


				<!--Eleccion rubros-->
				<div class="rubros">
					<h3>Postulado en:</h3>
					<div class="rubros" ng-repeat="rubro in data">
								<input type="checkbox" name="bodegas" id="bodegas" ng-checked="{{rubro.bodega}}" disabled>
								<label for="bodegas"><span><img src="img/bodega-ico.png" alt=""><p>bodegas</p></span></label>
								<input type="checkbox" name="hoteles" id="hoteles" ng-checked="{{rubro.hoteles}}" disabled>
								<label for="hoteles"><span><img src="img/hoteleria-ico.png" alt=""><p>hoteles</p></span></label>

								<input type="checkbox" name="gastronomia" id="gastronomia" ng-checked="{{rubro.gastronomia}}" disabled>
								<label for="gastronomia"><span><img src="img/resto-ico.png" alt=""><p>gastronomia</p></span></label>

								<input type="checkbox" name="turismo" id="turismo" ng-checked="{{rubro.turismo}}" disabled>
								<label for="turismo"><span><img src="img/turismo-ico.png" alt=""><p>turismo</p></span></label>


								<input type="checkbox" name="spa" id="spa" ng-checked="{{rubro.spa}}" disabled>
								<label for="spa"><span><img src="img/spa-ico.png" alt=""><p>spa</p></span></label>

								<input type="checkbox" name="lineas-aereas" id="lineas-aereas" ng-checked="{{rubro.lineas_aereas}}" disabled>
								<label for="lineas-aereas"><span><img src="img/avion-ico.png" alt=""><p>líneas aéreas</p></span></label>

								<input type="checkbox" name="lineas-terrestres" id="lineas-terrestres" ng-checked="{{rubro.lineas_terrestres}}" disabled>
								<label for="lineas-terrestres"><span><img src="img/transporte-ico.png" alt=""><p>líneas terrestres</p></span></label>

								<input type="checkbox" name="alquiler de autos" id="alquiler de autos" ng-checked="{{rubro.alquiler_autos}}" disabled>
								<label for="alquiler de autos"><span><img src="img/auto-ico.png" alt=""><p>alquiler de autos</p></span></label>

								<input type="checkbox" name="wine-shop" id="wine-shop" ng-checked="{{rubro.wine_shop}}" disabled>
								<label for="wine-shop"><span><img src="img/wineshop-ico.png" alt=""><p>wine shop</p></span></label>

								<input type="checkbox" name="logistica" id="logistica" ng-checked="{{rubro.logistica}}" disabled>
								<label for="logistica"><span><img src="img/logistica-ico.png" alt=""><p>logística</p></span></label>
					</div>
					<br>
					<h3>Bajo el cargo/puesto de:</h3>
					<div ng-repeat="puesto in data">
						<div ng-if="{{puesto.descripcion_puesto}}''">
								<span style="color: #841F2F; font-weight: bold;">Sin datos.</span>
						</div>
						<span ng-if="puesto.descripcion_puesto">
							<input type="text" name="" id="puestos" value="{{puesto.descripcion_puesto}}" readonly>
						</span>
						<span ng-if="!puesto.descripcion_puesto">
							<span style="color: #841F2F; font-weight: bold;">Sin datos.</span>
						</span>
					</div>
				</div> <!--FIN Eleccion rubros-->

				<!--Antecedentes laborales-->
				<div>
					<div class="antecedentes">
						<h3>Antecedentes laborales:</h3>
						<div ng-if="dataD.length <= 0 ">
								<span style="color: #841F2F; font-weight: bold;">Sin datos.</span>
						</div>
						<div ng-repeat ="Dlaboral in dataD">
							<table width="100%">
								<tr><td><b>Empresa:</b></td></tr>
								<tr><td><input type="text" name="" id="empresa"  ng-value="Dlaboral.empresa" style="font-weight: bold;"></td></tr>
								<tr><td><b>Puesto:</b></td></tr>
								<tr><td><input type="text" name="" id="puesto-antecedente" ng-value="Dlaboral.puestoAntec"></td></tr>
							</table>
							<table width="100%">
								<tr><td><b>Inicio:</b></td><td><b>Fin:</b></td></tr>
								<tr><td><input type="text" name="" id="puesto" ng-value="Dlaboral.inicio| date: 'dd/MM/yyyy'" disabled readonly></td>
								<td><input type="text" name="" id="puesto" ng-value="Dlaboral.fin | date: 'dd/MM/yyyy'" disabled read></td></tr>
							</table>
							<hr style="background: #841F2F; height: 2px;"></hr>
						</div>
					</div>
				</div>
			</br>
				<!--FIN Antecedentes laborales-->


				<!--Formación académica-->
				<div class="formacion">
					<h3>Formación académica:</h3>
					<div ng-if="dataT.length <= 0 ">
							<span style="color: #841F2F; font-weight: bold;">Sin datos.</span>
					</div>
					<div ng-repeat="academico in dataT">
						<lable><b>Nivel:</b></label>
						<input type="text" name="" id="titulo" ng-value="academico.nivel | uppercase" readonly style="font-weight: bold;">
						<lable><b>Titulo:</b></label>
						<input type="text" name="" id="titulo" ng-value="academico.titulo" readonly>
					</div>
				</div>

				<!--FIN Formación académica-->

				<!--IDIOMAS-->
				<div class="idiomas" ng-repeat="idioma in data">
					<h3>Idiomas que domina:</h3>
					<span ng-if="idioma.idioma">
					<input type="text" name="idioma" id="puestos" value="{{idioma.idioma}}" readonly>
					</span>
					<span ng-if="!idioma.idioma">
						<span style="color: #841F2F; font-weight: bold;">Sin datos.</span>
					</span>
				</div><!--FIN IDIOMAS-->

				<!--Carnet de conducir-->
				<div class="carnet">
					<h3>Carnet de conducir:</h3>
					<span ng-if="dataC">
						<div ng-repeat="carnets in dataC">
							<table>
								<tr><td><b>Tipo:</b></td><td><b>Vence:</b></td></tr>
								<tr>
									<td><input type="text" name="" id="titulo" ng-value="carnets.tipo | uppercase" readonly style="font-weight: bold;"></td>
									<td><input type="text" name="" id="titulo" ng-value="carnets.vencimiento | date: 'dd/MM/yyyy'" disabled readonly style="font-weight: bold;"></td>
								</tr>
							</table>
						</div>
					</span>
					<span ng-if="!dataC">
							<span style="color: #841F2F; font-weight: bold;">Sin datos.</span>
					</span>
				</div>

				<!--FIN Carnet de conducir-->

				<!--Disponibilidad-->
				<div class="disponibilidad" ng-repeat="disponib in data">
					<h3>Disponibilidad:</h3>
					<p><input type="checkbox" name="fullTime" id="checkeador" ng-checked="{{disponib.fulltime}}" disabled> Full Time
					</p>

					<p><input type="checkbox" name="parTime" id="checkeador" ng-checked="{{disponib.partime}}" disabled> Part Time
					</p>

					<p><input type="checkbox" name="event" id="checkeador" ng-checked="{{disponib.eventual}}" disabled> Eventual
					</p>

					<p><input type="checkbox" name="temp" id="checkeador" ng-checked="{{disponib.temporal}}" disabled> Temporal
					</p>

					<p><input type="checkbox" name="fds" id="checkeador" ng-checked="{{disponib.finSemana}}" disabled> Fines de semana
					</p>

					<br>
				</div>










			</div>
		</article>
		<?php include("sidebar.php");?>
		</div>

</section>

</div>

<?php include("seguinos-redes.php");?>

<?php include("publicidades-ancho.php");?>



<?php include("footer.php");?>

<?php

include("header.php");
?>
<div>
<section class="presentacion">
	<div id="contenedor">
		<div class="alinear"></div>

		<article>
			<div class="grupo">
				<!--<h2>Oportunidades laborales</h2>-->
				<h2><span id="TitOport">Oportunidades laborales</span></h2>
				<div>
					<h4 class="nomade" id="msjTitPost">SI SOS POSTULANTE NOMADIC</h4>
					<p id="msjTitExNom">Si buscás trabajo en bodegas, enoturismo, gastronomía, hospedajes, hoteles o empresas de transporte de pasajeros, pronto vas a quedar conectado con las firmas y compañías del sector que necesitan cubrir sus puestos en Argentina y otras Capitales Mundiales del Vino. Solo hace falta que ingreses tu curriculum.</p>
				</div>
				<div>
					<h4 class="empresa" id="msjTitEmp">SI SOS EMPRESA</h4>
					<p id="msjTitExEmp">Si buscas trabajadores para tu firma o compañía y te cuesta encontrar el perfil adecuado, pronto vas a quedar conectado con profesionales de toda la Argentina y otras Capitales Mundiales del Vino. Solo hace falta que cargues los puestos vacantes y los requisitos que hacen falta para cubrirlos.</p>
				</div><br>
				<a href=""><button id="boton"><span id="btnCF">CONOCÉ COMO FUNCIONA</span></button></a>
			</div>
		</article>
	</div>
</section>

<section class="cifras">
	<div id="contenedor">
		<article>
			<?php include("cifra-ofertas.php");?>
			<div class="division"></div>
			<?php include("cifra-nomades.php");?>
		</article>
	</div>
</section>

<section class="buscar">
	<div id="contenedor">
		<article>
			<div class="titulo" id="buscar">
				<h2>Bucá tu proximo empleo</h2>
				<button id="boton" onClick="buscarNomade(), buscarOferta('sf', 0)">soy nómade</button>
				<button onclick = "buscarPersonas(), busqudaPersonas('sf', 0)" id="boton" class="boton-rosa">soy empresa</button>
			</div>
			<!--<p class="notas">estos son los filtros si es que seleccionas que sos nomade y buscas trabajo</p>-->
			<div style="display: none;" id="filtro-resultado-oferta">
				<?php include("filtros-busqueda-ofertas.php");?>

				<!--<p class="notas">Los resultados de los perfiles de las ofertas, es lo que el sitio debería mostrar si sos una persona buscando trabajo</p>-->
				<?php //include("resultados-ofertas.php");?>
				<div id="resultadoOferta">
				</div>
				<?php //include("resultados-flechas.php");?>
			</div>
			<div style="display: none;" id="filtro-resultado-personas">
				<!--<p class="notas">estos son los filtros si es que seleccionas que sos empresa y buscas personal</p>-->
				<?php include("filtros-busqueda-personas.php");?>

				<!--<p class="notas">Los resultados de los perfiles de la gente, es lo que el sitio debería mostrar si sos una empresa buscando personal</p>-->

				<?php //include("resultados-personas.php");?>
				<div id="resultadoPersonas">
				</div>
				<?php //include("resultados-flechas.php");?>
			</div>
				<!--<p class="notas">Botones para pasar a la siguiente página de los resultados</p>-->
		</article>
	</div>
</section>
</div>
<?php include("seguinos-redes.php");?>
<?php include("publicidades-ancho.php");?>
<?php include("log-in.php");?>
<?php include("footer.php");?>

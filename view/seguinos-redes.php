<?php

/**
 * separador "seguinos en la redes sociales" *
**/

?>

<section class="seguinos">
	<article>
		<div id="contenedor">
			<h2>Seguinos en las redes</h2>
			<div>
				<?php $redes=mysqli_query($conexion,"SELECT * FROM social") or die(mysqli_error($conexion));
				while ($red=mysqli_fetch_array($redes)) {
					if ($red['social_enable']==1) { ?>
						<a target="_blank" href="http://<?php echo $red['social_link']; ?>">
							<img src="../img/<?php echo $red['social_img']; ?>" alt="<?php echo $red['social_name']; ?>">
						</a>
					<?php }
				}
				?>

			</div>
		</div>
	</article>
</section>

<?php
/**footer**/
?>
<footer>
	<div id="contenedor">
		<h1><img src="../img/logo-blanco.png" alt=""></h1>
		<div class="linea-vertical" id="contacto"></div>
		<form action="">
			<select name="" id="">
				<option value="" disabled selected hidden>Elija su perfil</option>
				<option value="">Empresa</option>
				<option value="">Nomadic</option>
			</select>
			<input type="text" name="" placeholder="Nombre completo" required>
			<input type="email" name="" placeholder="Tu@email" required>
			<textarea name="" id="" placeholder="Mensaje" required></textarea>
			<input type="submit" name="" id="enviar">
		</form>
		<div class="footer-derecha">
			<?php include("nav.php");?>
			<br><br>
			<p>
				<strong>NomadicResources ©2017</strong>
				<br>
				Todos los derechos reservados
				<br>
				Mendoza - Argentina - 2017
				<br>
				<br>
				Al navegar este sitio manifiesta su conformidad con los términos de confidencialidad y el uso de cookies del mismo. Para más información, lea nuestra
				<a href="legales.html" target="_blank"><strong>POLÍTICA DE PRIVACIDAD</strong></a> - <a href="terminos.html" target="_blank"><strong>TÉRMINOS Y CONDICIONES</strong></a>
			</p>
		</div>
	</div>
</footer>
</div> <!--cierra pantalla-->

<!--smooth scroll-->
<script>
  $('a.scroll').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top-180
    }, 2000);
    return false;
});
</script>

<!--cambia el css al scrolear-->
<script>
var $document = $(document),
    $element = $('nav.sticky a.logo-chico-nav'),
    className = 'logo-chico-nav-aparece';

$document.scroll(function() {
  if ($document.scrollTop() >= 240) {
    // cantidad de pixeles a scrolear;
    // para hacer lo siguiente:
    $element.addClass(className);
  } else {
    $element.removeClass(className);
  }
});

</script>
<script src="../js/functions_bej.js" charset="utf-8"></script>
<script src="../js/functions.js" charset="utf-8"></script>
<script src="../js/functions_bejCompany.js" charset="utf-8"></script>
<script src="../js/functions_busq_lab.js" charset="utf-8"></script>
<script src="../js/functions_traslate.js" charset="utf-8"></script>
<script src="../js/jquery.gmaps.js" charset="utf-8"></script>

</body>
</html>

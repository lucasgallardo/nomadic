<?php

/**
 * RUBROS *
**/

?>

<!--Rubro de la oferta-->
			<div class="rubros" ng-repeat="rubro in data">
						<input type="checkbox" name="bodegas" id="bodegas" ng-checked="{{rubro.bodega}}">
						<label for="bodegas" ng-click="rubros('bodega', rubro.user_id)"><span><img src="img/bodega-ico.png" alt=""><p>bodegas</p></span></label>
						<input type="checkbox" name="hoteles" id="hoteles" ng-checked="{{rubro.hoteles}}">
						<label for="hoteles" ng-click="rubros('hoteles', rubro.user_id)"><span><img src="img/hoteleria-ico.png" alt=""><p>hoteles</p></span></label>
						
						<input type="checkbox" name="gastronomia" id="gastronomia" ng-checked="{{rubro.gastronomia}}">
						<label for="gastronomia" ng-click="rubros('gastronomia', rubro.user_id)"><span><img src="img/resto-ico.png" alt=""><p>gastronomia</p></span></label>
						
						<input type="checkbox" name="turismo" id="turismo" ng-checked="{{rubro.turismo}}">
						<label for="turismo" ng-click="rubros('turismo', rubro.user_id)"><span><img src="img/turismo-ico.png" alt=""><p>turismo</p></span></label>
						
						
						<input type="checkbox" name="spa" id="spa" ng-checked="{{rubro.spa}}">
						<label for="spa" ng-click="rubros('spa', rubro.user_id)"><span><img src="img/spa-ico.png" alt=""><p>spa</p></span></label>
						
						<input type="checkbox" name="lineas-aereas" id="lineas-aereas" ng-checked="{{rubro.lineas_aereas}}">
						<label for="lineas-aereas" ng-click="rubros('lineas_aereas', rubro.user_id)"><span><img src="img/avion-ico.png" alt=""><p>líneas aéreas</p></span></label>
						
						<input type="checkbox" name="lineas-terrestres" id="lineas-terrestres" ng-checked="{{rubro.lineas_terrestres}}">
						<label for="lineas-terrestres" ng-click="rubros('lineas_terrestres', rubro.user_id)"><span><img src="img/transporte-ico.png" alt=""><p>líneas terrestres</p></span></label>
						
						<input type="checkbox" name="alquiler de autos" id="alquiler de autos" ng-checked="{{rubro.alquiler_autos}}">
						<label for="alquiler de autos" ng-click="rubros('alquiler_autos', rubro.user_id)"><span><img src="img/auto-ico.png" alt=""><p>alquiler de autos</p></span></label>
						
						<input type="checkbox" name="wine-shop" id="wine-shop" ng-checked="{{rubro.wine_shop}}">
						<label for="wine-shop" ng-click="rubros('wine_shop', rubro.user_id)"><span><img src="img/wineshop-ico.png" alt=""><p>wine shop</p></span></label>

						<input type="checkbox" name="logistica" id="logistica" ng-checked="{{rubro.logistica}}">
						<label for="logistica" ng-click="rubros('logistica', rubro.user_id)"><span><img src="img/logistica-ico.png" alt=""><p>logística</p></span></label>
			</div>
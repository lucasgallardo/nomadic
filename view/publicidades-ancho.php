<?php

/**
 * Resultados de ofertas *
**/
include_once('../control/advertising.php');
include_once('../control/conexion.php');
?>

<section class="publicidades">
	<div id="contenedor">
		<article>
			<div class="publicidad-xl">
				<?php echo ad_xl($conexion,$user_type); ?>
			</div>
		</article>

		<article>
			<div class="publicidad-m">
				<?php echo ad_m($conexion,$user_type); ?>
			</div>
		</article>

		<article>
			<div class="publicidad-s">
				<?php echo ad_s($conexion,$user_type); ?>
			</div>
		</article>
	</div>
</section>

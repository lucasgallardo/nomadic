<?php include("header.php");?>
<div ng-app="myAppC" ng-controller = "contNomadicCompany">
<p class="notas">Esta página es la que debe aparecer si apretas en registrar, te llega el mail de confirmacion con link a ESTE sitio donde completas tu perfil (hasta donde quieras) de EMPRESA, en un archivo nombrado igual pero con "persona" se encuentra el de persona</p>

<section class="perfil-completo-persona perfil-completo-empresa">
	<div id="contenedor">
		<article>
			<h2>QUIERO SER PARTE DE NOMADIC</h2>
			<div class="perfil-foto">
				<div ng-repeat="foto in dataC">
					<img ng-src="{{foto.img}}" width="200px" alt="Foto Perfil" id="fotosS">
				</div>
				<a href="">
					<button id="boton" ng-click="mostrarSFoto()">CAMBIAR FOTO</button>
				</a></p>
				<div style="position: absolute; background: grey; z-index: 100; display:none; padding: 8px; border-radius:5px;" id="contF">
					<form id="subida">
				        <!--<label>Subir un archivo</label><br />-->
				        <input name="foto" type="file" id="foto" accept="image/png, .jpeg, .jpg, image/gif"/><br /><br />
				        <input type="hidden" id="accion" name="accion" value="fEmpresa"/></br>
				        <span ng-repeat="userF in dataC">
				        	<input type = "hidden" name="usuario" value="{{userF.user_empresa_id}}">
				    	</span>
				        <button id="boton" ng-click="subirArchivo()">Subir Imagen</button><br />
				        <!--<button id="boton" >Subir Imagen</button><br />-->
				    </form>
				</div>
				&nbsp;
					<div style="background:#CEF6CE; max-width: 400px; text-align: center; display: none; position: relative; margin: auto; padding: 5px;" id="msgGEmpF">
						¡Datos guardados correctamente!
					</div>
			</div>
			<div class="perfil">

				<!--Datos LEGALES-->
				<div class="datos-personales" ng-cloak ng-repeat="datosComp in dataC">
					<input type="text" name="" value="{{datosComp.user_empresa_title}}" id="nombre" placeholder="Nombre de la empresa" readonly>
				</div>
				<div style="background:; margin-top: -30px;">
				<div class="datos-personales">
					<span style="color: red;">{{msgValRazon}}</span>
					<select name="razon-social" id="razon-social">
						<option value="" disabled selected hidden>Tipo de Razón Social</option>
						<option value="CUIT" ng-checked="{{}}">CUIT</option>
						<option value="ORG">ORG</option>
						<option value="SA">SA</option>
					</select>
				</div>
				</div>
				<div style="background:; margin-top: -30px;">
				<div class="datos-personales" ng-cloak ng-repeat="datosComp in dataC">
					<span style="color: red;">{{msgCuit}}</span>
					<input type="text" name="" value="{{datosComp.user_empresa_cuit}}" id="numero-razon-social" placeholder="Número de Razón Social" readonly>

				<!--LUGAR-->
					<span style="color: red;">{{msgCalle}}</span>
					<input type="text" name="" value ="{{datosComp.calle}}" id="calle" placeholder="Calle">
					<span style="color: red;">{{msgAltura}}</span>
					<input type="text" name="" value = "{{datosComp.altura}}" id="altura" placeholder="Altura">
					<input type="text" name="" value ="{{datosComp.piso}}" id="piso" placeholder="Piso">
					<input type="text" name="" value = "{{datosComp.dpt}}" id="apartamento" placeholder="Dpto">
					<span style="color: red;">{{msgPais}}</span>
					<input type="text" name="" value = "{{datosComp.pais}}" id="pais" placeholder="País">
					<span style="color: red;">{{msgProv}}</span>
					<input type="text" name="" value = "{{datosComp.provEst}}" id="provincia" placeholder="Provincia">
					<span style="color: red;">{{msgDepart}}</span>
					<input type="text" name="" value = "{{datosComp.departamento}}" id="departamento" placeholder="Departamento">
					<span style="color: red;">{{msgPostal}}</span>
					<input type="text" name="" value = "{{datosComp.codPostal}}" id="cp" placeholder="Código Postal">
					<!--<br>
					<a href="" class="agregar">Agregar ubicación en el mapa</a>
					<br>-->

				<!--DATOS DE CONTACTO-->
					<span style="color: red;">{{msgTelEmp}}</span>
					<input type="tel" name="" value = "{{datosComp.telefonoEmp}}" id="telefono" placeholder="Número de teléfono" required>
					<!--<br>
					<a href="" class="agregar">Agregar otro teléfono</a>
					<br>-->
					<span style="color: red;">{{msgMailEmp}}</span>
					<input type="email" name="" value="{{datosComp.mailEmp}}" id="mail" placeholder="@email" required>
					<!--<br>
					<a href="" class="agregar">Agregar otro email</a>
					<br>-->
					<input type="hidden" id="latitudes" placeholder="latitud">
					<input type = "hidden" id="longitudes" placeholder="longitud">
					<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: relative; margin: auto;" id="msgGEmp">
						¡Datos guardados correctamente!
					</div><br>
					<input type="submit" name="" id="Modificar" class="submit" value="Modificar" ng-click="GuardarEmpresa(datosComp.user_empresa_id)">
				</div>
				</div>
				<!--FIN Datos personales-->


				<!--contacto de referencia-->
				<div class="datos-contacto-referencia" ng-cloak ng-repeat="referencia in dataC">
				<h3>Contacto de referencia</h3>
					<span style="color: red;">{{msgNombreCC}}</span><br>
					<input type="text" name="" value="{{referencia.nombre}}" id="nombreRef" placeholder="Nombre y Apellido" required>

					<span style="color: red;">{{msgDoCC}}</span><br>
					<input type="text" name="" value = "{{referencia.dni_contacto}}" id="dni" placeholder="Número de documento">

					<span style="color: red;">{{msgPuestoCC}}</span><br>
					<input type="text" name="" value = "{{referencia.puesto}}" id="puestos" placeholder="Agregar puesto (si es más de uno, dividi con ',')">

					<span style="color: red;">{{msgTelCC}}</span><br>
					<input type="tel" name="" value ="{{referencia.telefono}}" id="telefonoRef" placeholder="Número de teléfono" required>
					<input type="tel" name="" value = "{{referencia.tel_alternativo}}" id="telefonoRefAlt" placeholder="Agregar  teléfono alternativo">

					<span style="color: red;">{{msgMailCC}}</span><br>
					<input type="email" name="" value = "{{referencia.mail}}" id="mailRef" value="" placeholder="@email" required>
					<input type="email" name="" value = "{{referencia.mail_alternativo}}" id="mailRefAlt" placeholder="Agregar mail alternativo">
					<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: relative; margin: auto;" id="msgGR">
						¡Datos guardados correctamente!
					</div><br>
					<input type="submit" name="" id="ModificarRef" class="submit" value="Modificar" ng-click="carDatosCont(referencia.user_empresa_id)">
				</div> <!--FIN Datos de contacto-->



				<!--Eleccion rubros-->
				<div class="rubros" ng-repeat="rubro in dataC">
					<h3>Rubro/s de la empresa</h3>
					<h5>Podés elegir más de uno</h5>
						<input type="checkbox" name="bodegas" id="bodegas" ng-checked="{{rubro.bodega}}">
						<label for="bodegas" ng-click="rubros('bodega', rubro.user_empresa_id)"><span><img src="img/bodega-ico.png" alt=""><p>bodegas</p></span></label>
						<input type="checkbox" name="hoteles" id="hoteles" ng-checked="{{rubro.hoteles}}">
						<label for="hoteles" ng-click="rubros('hoteles', rubro.user_empresa_id)"><span><img src="img/hoteleria-ico.png" alt=""><p>hoteles</p></span></label>

						<input type="checkbox" name="gastronomia" id="gastronomia" ng-checked="{{rubro.gastronomia}}">
						<label for="gastronomia" ng-click="rubros('gastronomia', rubro.user_empresa_id)"><span><img src="img/resto-ico.png" alt=""><p>gastronomia</p></span></label>

						<input type="checkbox" name="turismo" id="turismo" ng-checked="{{rubro.turismo}}">
						<label for="turismo" ng-click="rubros('turismo', rubro.user_empresa_id)"><span><img src="img/turismo-ico.png" alt=""><p>turismo</p></span></label>


						<input type="checkbox" name="spa" id="spa" ng-checked="{{rubro.spa}}">
						<label for="spa" ng-click="rubros('spa', rubro.user_empresa_id)"><span><img src="img/spa-ico.png" alt=""><p>spa</p></span></label>

						<input type="checkbox" name="lineas-aereas" id="lineas-aereas" ng-checked="{{rubro.lineas_aereas}}">
						<label for="lineas-aereas" ng-click="rubros('lineas_aereas', rubro.user_empresa_id)"><span><img src="img/avion-ico.png" alt=""><p>líneas aéreas</p></span></label>

						<input type="checkbox" name="lineas-terrestres" id="lineas-terrestres" ng-checked="{{rubro.lineas_terrestres}}">
						<label for="lineas-terrestres" ng-click="rubros('lineas_terrestres', rubro.user_empresa_id)"><span><img src="img/transporte-ico.png" alt=""><p>líneas terrestres</p></span></label>

						<input type="checkbox" name="alquiler de autos" id="alquiler de autos" ng-checked="{{rubro.alquiler_autos}}">
						<label for="alquiler de autos" ng-click="rubros('alquiler_autos', rubro.user_empresa_id)"><span><img src="img/auto-ico.png" alt=""><p>alquiler de autos</p></span></label>

						<input type="checkbox" name="wine-shop" id="wine-shop" ng-checked="{{rubro.wine_shop}}">
						<label for="wine-shop" ng-click="rubros('wine_shop', rubro.user_empresa_id)"><span><img src="img/wineshop-ico.png" alt=""><p>wine shop</p></span></label>

						<input type="checkbox" name="logistica" id="logistica" ng-checked="{{rubro.logistica}}">
						<label for="logistica" ng-click="rubros('logistica', rubro.user_empresa_id)"><span><img src="img/logistica-ico.png" alt=""><p>logística</p></span></label>
					<!--FIN Eleccion rubros-->
				</div>
					</p>
				<div ng-repeat ="sitio in dataC">
					<h3>Sitio web de la empresa:</h3>
					<input type="text" name="" id="sitio-web" placeholder="http://" value="{{sitio.sitio_web}}">
					<div style="background:#CEF6CE; height: 25px; min-width: 400px; text-align: center; padding-top: 6px; display: none; position: relative; margin: auto;" id="msgGSitio">
						¡Datos guardados correctamente!
					</div><br>
					<input type="submit" name="" id="ModifSitio" class="submit" value="Modificar" ng-click="ModificarSitio(sitio.user_empresa_id)">
				</div>












			</div>
		</article>
		<?php include("sidebar.php");?>

	</div>
</section>
</div>

<?php include("seguinos-redes.php");?>
<?php include("publicidades-ancho.php");?>
<?php include("log-in.php");?>
<?php include("footer.php");?>

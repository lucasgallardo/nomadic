<?php
$HOST = "localhost";
$USER = "root";
$PASS = "1234";
$DB = "nomadic_db";

function conectar(){
	global $HOST, $USER, $PASS, $DB;
	$cnx = mysqli_connect($HOST, $USER, $PASS, $DB);
	if (mysqli_connect_errno()) {
		echo "Conexión fallida: ".mysqli_connect_error();
		exit();
	}

	return $cnx;
}

$conexion = conectar();

?>

<head>
<meta charset="UTF-8">
<title>Nomadic Resources. Work opportunities in the Capitals of Wine</title>

	<link href="img/icono.png" rel="shortcut icon" type="image/png" width="32px">

	<meta name="expires" content="text">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

	<!--para buscadores-->
	<meta name="author" content="Brick. Soluciones Gráficas" />
	<meta name="description" content="<?php echo $text['content'] ?>" />
	<meta name="keywords" content="Job Placement in Mendoza. Job Agency. World wine capitals.Job opportunities in Mendoza, job opportunities in Argentina, wine industry, hospitality industry, hospitality and tourism industry, hotel and hospitality industry, jobs abroad, trained professionals, hire qualified workers, world wine capitals, tourism and hospitality, work in wineries, wine tourism, gastronomy, accomodation, hotels, passenger transport companies." />
	<meta name="robots" content="index, follow" />

	<!--si se ve bien en productos apple pongo lo siguiente-->
	<meta name="apple-mobile-web-app-capable" content="yes" />


	<!-- CUIDADO: Respond.js no funciona si vemos la página de manera local -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Siempre se coloca primero normalize.css -->
<link rel="stylesheet" href="css/normalize.css" type="text/css">


<!--animaciones de cajas-->
<link rel="stylesheet" type="text/css" href="css/animacion.css">

<link rel="stylesheet" href="css/estilos.css" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>

</head>
<body>
	<div id="pantalla">
    <div class="subir"><a href="#pantalla" class="scroll"><img src="img/flecha-arriba.png" alt="Subir"></a></div>

		<header>
      <div id="logo">
        <div class="linea"></div>
        <h1>
          <img src="img/nomadicblanco.png" alt="Nomadic Resources">
          <br><br><br>
          <a href="#preguntas" class="scroll">
            <img src="img/flecha-abajo.png" alt="flecha abajo">
          </a>
        </h1>
      </div>
			<div class="lenguaje">
				<a href="index.php?lang=en" style="color: white;">English |</a>
				<a href="index.php?lang=es" style="color: white;">Español |</a>
			</div>
      <section id="efecto-fotos">
        <article>
          <div class="movimiento m1"></div>
          <div class="movimiento m2"></div>
          <div class="movimiento m3"></div>
          <div class="movimiento m4"></div>
          <div class="movimiento m5"></div>
          <!--div class="m5"></div-->
        </article>
      </section>

    </header>


    <section id="preguntas">
      <div id="contenedor">
        <h2>
          <img src="img/nomadic.png" alt="Nomadic Resources">
          <br>
          Job opportunities</h2>
      <article id="respuestas"></article>
      <div class="iconos">
       <a href="#respuestas" class="scroll">
        <div class="icono">
          <div class="bodegas"><img src="img/bodega-ico.png" alt="iconos"></div>
          <h5>WINERIES</h5>
        </div>
      </a>
       <a href="#respuestas" class="scroll">
        <div class="icono">
          <div class="hoteles"><img src="img/hoteleria-ico.png" alt="iconos"></div>
          <h5>HOTELS</h5>
        </div>
      </a>
       <a href="#respuestas" class="scroll">
        <div class="icono">
          <div class="gastro"><img src="img/gastronomia-ico.png" alt="iconos"></div>
          <h5>GASTRONOMY</h5>
        </div>
      </a>
       <a href="#respuestas" class="scroll">
        <div class="icono">
          <div class="turismo"><img src="img/turismo-ico.png" alt="iconos"></div>
          <h5>TOURISM</h5>
        </div>
      </a>
       <a href="#respuestas" class="scroll">
        <div class="icono">
          <div class="transporte"><img src="img/transporte-ico.png" alt="iconos"></div>
          <h5>TRANSPORT</h5>
        </div>
      </a>

      </div>


    </div><!--cierra contenedor-->

      <div class="respuestas">
        <div id="contenedor">

          <article class="nomada">
          <h4>As <strong>Nomadic</strong> candidate</h4>
          <ul>
          <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>
              <h3>Would you travel if you were offered a job abroad?</h3>
            </li>
          </div>
          </div>
          <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>
              <h3>Are you looking for a job in the hospitality or wine industry?</h3>
            </li>
          </div>
          </div>

          </ul>
        </article>

        <article class="empresa">
          <h4>As a company</h4>
          <ul>
          <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>
              <h3>Do you need professionals or trained workers for your company?</h3>
            </li>
          </div>
          </div>
          <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>
              <h3>Are you a CEO, Director or HR manager? Do you have the responsibility to hire qualified workers?</h3>
            </li>
          </div>
          </div>
          </ul>
        </article>



          <a href="#nosotros" class="scroll"><div class="yes"></div></a>

        </div>
      </div>

    </section>





    <!--///////////////////////////////////////////////////////-->
    <section id="nosotros">
      <div id="contenedor">
        <h2><img src="img/logo-hor.png" alt="Nomadic Resources"></h2>
        <h3>Very Soon!</h3>
        <h4><strong>If you are a Nomadic applicant</strong><br>
            If your are looking for a job in wineries, wine tourims, gastronomy, lodging, hotels or in passenger transport companies, you will soon be connected to the companies in the sector that need to fill their job vacancies in Argentina and other world wine capitals. You just need to upload your CV.
        </h4>
        <h4><strong>If you are a company </strong><br>
            If you are looking for workers for your company, and you are struggling in finding the right profile, you will soon be connected to professionals from Argentina and other world wine capitals. You just need to upload the job vacancies and the requirements to fill them up.
        </h4>

        <a href="#funcionamiento" class="scroll">
          <p>How does it work?</p>
          <img src="img/flecha-abajo.png" alt="flecha abajo">
        </a>
      </div>
    </section>

        <!--///////////////////////////////////////////////////////-->
<section id="foto-corte" class="foto-corte1"></section>
    <!--///////////////////////////////////////////////////////-->

    <!--///////////////////////////////////////////////////////-->

    <section id="funcionamiento">
      <h3>How does it work?</h3>

      <article class="puntouno">
        <div class="animation-element bounce-up cf">
          <div class="subject development">
            <div id="contenedor">
              <div class="img-puntos">
                <img src="img/punto1.png" alt="punto 1">
              </div>
              <div class="txt-puntos">
                <h2>1</h2>
                <p>If you are a Nomadic candidate, upload your CV. You will be automatically visible for hundreds of companies in Argentina and other world wine capitals, you will receive offers according to your profile, and you will be able to follow up your application.</p>
              </div>
            </div>
          </div>
        </div>
      </article>


      <article class="puntodos">
        <div class="animation-element bounce-up cf">
          <div class="subject development">
        <div id="contenedor">
          <div class="img-puntos">
            <img src="img/punto2.png" alt="punto 2">
          </div>
          <div class="txt-puntos">
            <h2>2</h2>
            <p>If your are a company upload the job vacancies from your company. Nomadic candidates will automatically know what kind of professional you are looking for, in Argentina or in other world wine capitals.</p>
          </div>
        </div>
        </div>
        </div>
      </article>

      <article class="puntotres">
        <div class="animation-element bounce-up cf">
          <div class="subject development">
        <div id="contenedor">
          <div class="img-puntos">
            <img src="img/punto3.png" alt="punto 3">
          </div>
          <div class="txt-puntos">
            <h2>3</h2>
            <p>Nomadic will automatically connect offers and demand. Each time a job vacancy is placed, Nomadic candidates, that fullfil the profile requirements, will receive an email alerting about the available position. At the same time, companies will receive the CVs of Nomadic applicants that match their search characteristics.</p>
          </div>
        </div>
        </div>
        </div>
      </article>

    </section>


    <!--///////////////////////////////////////////////////////-->

    <section id="tambien">
      <h3>You will also:</h3>
      <div id="contenedor">
        <article class="nomada">
          <h4>As <strong>Nomadic</strong> candidate</h4>
          <ul>
                     <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li> Update your personal information.</li>
               </div></div>
                    <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Do a follow up of your applications.</li>
          </div></div>
                   <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Get alerts for all the job vacancies that match your profile.</li>
               </div></div>
                        <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Plan trips according to job offers.</li>
               </div></div>
                        <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Contact companies interested in your services to set interview dates..</li>
               </div></div>
               <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Get to know and stablish professional relationships in your sector in Argentina and other World Wine Capitals.</li>
               </div></div>
               <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Boost your knowledge and improve your CV thanks to the possibility of experiencing new jobs in and outside the country.</li>
               </div></div>
          </ul>
        </article>

        <article class="empresa">
          <h4>As a company</h4>
          <ul>
                     <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Post all the job vacancies your company needs to fill.</li>
               </div></div>
                    <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Receive CVs of those professionals whose profile match your search.</li>
          </div></div>
                   <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Download an index of Nomadic applicants in a PDF file.</li>
               </div></div>
                        <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Set interviews with Nomadic applicants.</li>
               </div></div>
                        <div class="animation-element bounce-up cf">
          <div class="subject development">
            <li>Add value with trained staff, not only where the company is settled, but also with professionals that are looking for a job in Argentina and in other world wine capitals.</li>
               </div></div>
          </ul>
        </article>
      </div>
    </section>

    <!--///////////////////////////////////////////////////////-->
<section id="foto-corte" class="foto-corte2"></section>
    <!--///////////////////////////////////////////////////////-->
    <!--///////////////////////////////////////////////////////-->
    <!--///////////////////////////////////////////////////////-->
    <!--///////////////////////////////////////////////////////-->

    <section id="nosotros">
      <div id="contenedor">
        <h3>Become Nomadic!</h3>
      </div>
    <!--///////////////////////////////////////////////////////-->
      <section id="antefooter">
        <div id="contenedor">
          <h2><img src="img/logo-blanco.png" alt="Nomadic Resources"></h2>

             <div class="formula">
                  <form id="formulario2"  action="envia2.php" method="POST">

                     <select id="select" name="select">
                        <option selected value="0">Choose your profile</option>
                        <option id="nomadic" name="nomadic" value="nomadic">Nomadic candidate</option>
                        <option id="empresa" name="empresa" value="empresa">Company</option>
                     </select>


                    <input type="text" name="name" id="name" placeholder="Full name" required>
                    <input type="email" name="email" id="email" placeholder="your@email" required>
                    <textarea id="message" name="message" placeholder="Message" spellcheck="true" required></textarea required>
                    <input class="enviar" type="submit" value="SEND">
                    <div id="contactResponse2"></div>
                  </form>
            </div>
        </div>
      </section>
    <!--///////////////////////////////////////////////////////-->
    </section>
    <!--///////////////////////////////////////////////////////-->




<footer>
      <div id="contenedor">
        <p>NomadicResources ©2017 All rights reserved - Mendoza - Argentina 2017 </p>
        <p>By using this website you show approval with the terms of confidenciality and use of cookies. <br>for further information, read our
				<a href="terminos.html" target="_blank"><strong>Terms and conditions</strong></a> -
        <a href="legales.html" target="_blank"><strong>privacy policy.</strong></a>
        </p>
				<p></p>

      <a href="http://bricksg.com.ar" target="_blank"><p>design and development: <img src="img/bricksg.png" alt="BRICK Soluciones Gráficas Contenidos Editoriales"></p></a>
        </div>
</footer>



	</div><!--cierra pantalla-->


<!--smooth scroll-->
<script>
  $('a.scroll').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 1000);
    return false;
});
</script>


<!--smooth aparece flecha subir-->
<script type='text/javascript'>
//<![CDATA[
// Botón para Ir Arriba
jQuery.noConflict();
jQuery(document).ready(function() {
jQuery(".subir").hide();
jQuery(function () {
jQuery(window).scroll(function () {
if (jQuery(this).scrollTop() > 200) {
jQuery('.subir').fadeIn();
} else {
jQuery('.subir').fadeOut();
}
});
jQuery('.subir a').click(function () {
jQuery('body,html').animate({
scrollTop: 0
}, 800);
return false;
});
});

});
//]]>
</script>


<!--anima contenidos-->
<script>
  var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
      (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');
</script>



<script>
     $("#formulario2").submit(function(event)
     {
         /* evita el funcionamiento clasico del post */
         event.preventDefault();

         /* ubicar los elementos en la página */
         var $form = $( this ),
             $submit = $form.find( 'button[type="submit"]' ),
             name_value = $form.find( 'input[name="name"]' ).val(),
             email_value = $form.find( 'input[name="email"]' ).val(),
             message_value = $form.find( 'textarea[name="message"]' ).val(),
             select_value = $form.find( 'select[name="select"]' ).val(),
             url = $form.attr('action');

         /* Send the data using post */
         var posting = $.post( url, {
                           name: name_value,
                           email: email_value,
                           message: message_value,
                           select:select_value,
                           lang:'<?php echo $language; ?>'

                       });

         posting.done(function( data )
         {
             /* pone el echo en un div que está debajo del formulario, el estilo se pone en el php */
             $( "#contactResponse2" ).html(data);

             /* Change the button text. */
             $submit.text('Sent, thanks');

             /* Disable the button. */
             $submit.attr("disabled", true);
         });
    });
</script>
</body>

<head>
	<meta charset="UTF-8">
	<title>Nomadic Resources. Oportunidades laborales en las Capitales del Vino</title>

	<link href="img/icono.png" rel="shortcut icon" type="image/png" width="32px">

	<meta name="expires" content="text">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

	<!--para buscadores-->
	<meta name="author" content="Brick. Soluciones Gráficas" />
	<meta name="description" content="<?php echo $text['content'] ?>" />
	<meta name="keywords" content="Bolsa de trabajo Mendoza, Bolsa de trabajo Capitales mundiales del vino, oportunidades laborales en Mendoza, oportunidades laborales en Argentina, industria de la vitivinicultura, industria de la hospitalidad, industria de la hospitalidad y el turismo, hotelería e industria de la hospitalidad, empleo en el exterior, profesionales capacitados, contratar personal calificado, capitales mundiales del vino, turismo y hospitalidad, trabajo en bodegas, enoturismo, gastronomía, hospedajes, hoteles y empresas de transporte de pasajeros" />
	<meta name="robots" content="index, follow" />

	<!--si se ve bien en productos apple pongo lo siguiente-->
	<meta name="apple-mobile-web-app-capable" content="yes" />


	<!-- CUIDADO: Respond.js no funciona si vemos la página de manera local -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Siempre se coloca primero normalize.css -->
	<link rel="stylesheet" href="css/normalize.css" type="text/css">


	<!--animaciones de cajas-->
	<link rel="stylesheet" type="text/css" href="css/animacion.css">

	<link rel="stylesheet" href="css/estilos.css" type="text/css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>

</head>
<body>
	<div id="pantalla">
		<div class="subir"><a href="#pantalla" class="scroll"><img src="img/flecha-arriba.png" alt="Subir"></a></div>

		<header>
			<div id="logo">
				<div class="linea"></div>
				<h1>
					<img src="img/nomadicblanco.png" alt="Nomadic Resources">
					<br><br><br>
					<a href="#preguntas" class="scroll">
						<img src="img/flecha-abajo.png" alt="flecha abajo">
					</a>
				</h1>
			</div>
			<div class="lenguaje">
				<a href="index.php?lang=en" style="color: white;">English |</a>
				<a href="index.php?lang=es" style="color: white;">Español |</a>
			</div>
			<section id="efecto-fotos">
				<article>
					<div class="movimiento m1"></div>
					<div class="movimiento m2"></div>
					<div class="movimiento m3"></div>
					<div class="movimiento m4"></div>
					<div class="movimiento m5"></div>
					<!--div class="m5"></div-->
				</article>
			</section>

		</header>


		<section id="preguntas">
			<div id="contenedor">
				<h2>
					<img src="img/nomadic.png" alt="Nomadic Resources">
					<br>
					Oportunidades laborales</h2>
					<article id="respuestas"></article>
					<div class="iconos">
						<a href="#respuestas" class="scroll">
							<div class="icono">
								<div class="bodegas"><img src="img/bodega-ico.png" alt="iconos"></div>
								<h5>BODEGAS</h5>
							</div>
						</a>
						<a href="#respuestas" class="scroll">
							<div class="icono">
								<div class="hoteles"><img src="img/hoteleria-ico.png" alt="iconos"></div>
								<h5>HOTELES</h5>
							</div>
						</a>
						<a href="#respuestas" class="scroll">
							<div class="icono">
								<div class="gastro"><img src="img/gastronomia-ico.png" alt="iconos"></div>
								<h5>GASTRONOMÍA</h5>
							</div>
						</a>
						<a href="#respuestas" class="scroll">
							<div class="icono">
								<div class="turismo"><img src="img/turismo-ico.png" alt="iconos"></div>
								<h5>TURISMO</h5>
							</div>
						</a>
						<a href="#respuestas" class="scroll">
							<div class="icono">
								<div class="transporte"><img src="img/transporte-ico.png" alt="iconos"></div>
								<h5>TRANSPORTE</h5>
							</div>
						</a>

					</div>


				</div><!--cierra contenedor-->

				<div class="respuestas">
					<div id="contenedor">

						<article class="nomada">
							<h4>Como postulante <strong>Nomadic</strong></h4>
							<ul>
								<div class="animation-element bounce-up cf">
									<div class="subject development">
										<li>
											<h3>¿Viajarías si te ofrecen un empleo en el exterior?</h3>
										</li>
									</div>
								</div>
								<div class="animation-element bounce-up cf">
									<div class="subject development">
										<li>
											<h3>¿Buscás trabajo en la industria de la vitvinicultura o la industria de la hospitalidad?</h3>
										</li>
									</div>
								</div>

							</ul>
						</article>

						<article class="empresa">
							<h4>Como empresa</h4>
							<ul>
								<div class="animation-element bounce-up cf">
									<div class="subject development">
										<li>
											<h3>¿Necesitás profesionales o trabajadores capacitados para tu empresa?</h3>
										</li>
									</div>
								</div>
								<div class="animation-element bounce-up cf">
									<div class="subject development">
										<li>
											<h3>¿Sos CEO, director o gerente de RR.HH y tenés la responsabilidad de contratar personal calificado?</h3>
										</li>
									</div>
								</div>
							</ul>
						</article>



						<a href="#nosotros" class="scroll"><div class="yes"></div></a>

					</div>
				</div>

			</section>





			<!--///////////////////////////////////////////////////////-->
			<section id="nosotros">
				<div id="contenedor">
					<h2><img src="img/logo-hor.png" alt="Nomadic Resources"></h2>
					<h3>¡Muy pronto!</h3>
					<h4><strong>Si sos postulante Nomadic</strong><br>
						Si buscás trabajo en bodegas, enoturismo, gastronomía, hospedajes, hoteles o empresas de transporte de pasajeros, pronto vas a quedar conectado con las firmas y compañías del sector que necesitan cubrir sus puestos en Argentina y otras Capitales Mundiales del Vino. Solo hace falta que ingreses tu curriculum.
					</h4>
					<h4><strong>Si sos empresa </strong><br>
						Si buscas trabajadores para tu firma o compañía y te cuesta encontrar el perfil adecuado, pronto vas a quedar conectado con profesionales de toda la Argentina y otras Capitales Mundiales del Vino. Solo hace falta que cargues los puestos vacantes y los requisitos que hacen falta para cubrirlos.
					</h4>

					<a href="#funcionamiento" class="scroll">
						<p>¿Cómo funciona?</p>
						<img src="img/flecha-abajo.png" alt="flecha abajo">
					</a>
				</div>
			</section>

			<!--///////////////////////////////////////////////////////-->
			<section id="foto-corte" class="foto-corte1"></section>
			<!--///////////////////////////////////////////////////////-->

			<!--///////////////////////////////////////////////////////-->

			<section id="funcionamiento">
				<h3>¿Cómo funciona?</h3>

				<article class="puntouno">
					<div class="animation-element bounce-up cf">
						<div class="subject development">
							<div id="contenedor">
								<div class="img-puntos">
									<img src="img/punto1.png" alt="punto 1">
								</div>
								<div class="txt-puntos">
									<h2>1</h2>
									<p>Si sos postulante Nomadic carga tu curriculum. De manera automática quedarás visible ante cientos de empresas en Argentina y otras capitales mundiales del Vino, recibirás propuestas adecuadas a tu perfil y podrás seguir el estado de tu postulación.</p>
								</div>
							</div>
						</div>
					</div>
				</article>


				<article class="puntodos">
					<div class="animation-element bounce-up cf">
						<div class="subject development">
							<div id="contenedor">
								<div class="img-puntos">
									<img src="img/punto2.png" alt="punto 2">
								</div>
								<div class="txt-puntos">
									<h2>2</h2>
									<p>Si sos empresa carga los puestos vacantes de tu firma o compañía. De manera automática los postulantes Nomadic sabrán qué tipo de profesional estás necesitando, tanto en Argentina como en otras Capitales Mundiales del Vino.</p>
								</div>
							</div>
						</div>
					</div>
				</article>

				<article class="puntotres">
					<div class="animation-element bounce-up cf">
						<div class="subject development">
							<div id="contenedor">
								<div class="img-puntos">
									<img src="img/punto3.png" alt="punto 3">
								</div>
								<div class="txt-puntos">
									<h2>3</h2>
									<p>Nomadic conectará oferta y demanda en forma automática. Cada vez que se genere un puesto de trabajo, aquellos postulantes Nomadic que reúnan el perfil adecuado recibirán un correo electrónico que les avisará que hay un cargo disponible. A su vez, a las empresas les llegarán los currículum de postulantes Nomadic que coincidan con las características de su búsqueda. </p>
								</div>
							</div>
						</div>
					</div>
				</article>

			</section>


			<!--///////////////////////////////////////////////////////-->

			<section id="tambien">
				<h3>También vas a poder:</h3>
				<div id="contenedor">
					<article class="nomada">
						<h4>Como postulante <strong>Nomadic</strong></h4>
						<ul>
							<div class="animation-element bounce-up cf">
								<div class="subject development">
									<li> Actualizar tu información personal.</li>
								</div></div>
								<div class="animation-element bounce-up cf">
									<div class="subject development">
										<li>Realizar el seguimiento de tus postulaciones.</li>
									</div></div>
									<div class="animation-element bounce-up cf">
										<div class="subject development">
											<li>Recibir avisos de la totalidad de las ofertas laborales que coincidan con tu perfil.</li>
										</div></div>
										<div class="animation-element bounce-up cf">
											<div class="subject development">
												<li>Planificar viajes en base a propuestas laborales.</li>
											</div></div>
											<div class="animation-element bounce-up cf">
												<div class="subject development">
													<li>Contactar empresas interesadas en tus servicios para coordinar entrevistas.</li>
												</div></div>
												<div class="animation-element bounce-up cf">
													<div class="subject development">
														<li>Conocer y entablar relaciones profesionales de tu rubro en Argentina y en otras Capitales Mundiales del Vino.</li>
													</div></div>
													<div class="animation-element bounce-up cf">
														<div class="subject development">
															<li>Enriquecer tus conocimientos y mejorar tu curriculum gracias a la posibilidad de experiencias en nuevos trabajos dentro y fuera del país.</li>
														</div></div>
													</ul>
												</article>

												<article class="empresa">
													<h4>Como empresa</h4>
													<ul>
														<div class="animation-element bounce-up cf">
															<div class="subject development">
																<li>Publicar los puestos laborales que necesite cubrir su firma o compañía.</li>
															</div></div>
															<div class="animation-element bounce-up cf">
																<div class="subject development">
																	<li>Recibir los curriculum de aquellos profesionales cuyos perfiles coincidan con la búsqueda preestablecida.</li>
																</div></div>
																<div class="animation-element bounce-up cf">
																	<div class="subject development">
																		<li>Descargar fichero de postulantes Nomadic en formato PDF.</li>
																	</div></div>
																	<div class="animation-element bounce-up cf">
																		<div class="subject development">
																			<li>Coordinar entrevistas con postulantes Nomadic.</li>
																		</div></div>
																		<div class="animation-element bounce-up cf">
																			<div class="subject development">
																				<li>Sumar valor con personal capacitado, no solo de la zona donde funciona la empresa, sino también con profesionales que buscan empleo en Argentina y en otras Capitales Mundiales del Vino.</li>
																			</div></div>
																		</ul>
																	</article>
																</div>
															</section>

															<!--///////////////////////////////////////////////////////-->
															<section id="foto-corte" class="foto-corte2"></section>
															<!--///////////////////////////////////////////////////////-->
															<!--///////////////////////////////////////////////////////-->
															<!--///////////////////////////////////////////////////////-->
															<!--///////////////////////////////////////////////////////-->

															<section id="nosotros">
																<div id="contenedor">
																	<h3>¡Empezá a ser parte de Nomadic!</h3>
																</div>
																<!--///////////////////////////////////////////////////////-->
																<section id="antefooter">
																	<div id="contenedor">
																		<h2><img src="img/logo-blanco.png" alt="Nomadic Resources"></h2>

																		<div class="formula">
																			<form id="formulario2"  action="envia2.php" method="POST">

																				<select id="select" name="select">
																					<option selected value="0">Elija su perfil</option>
																					<option id="nomadic" name="nomadic" value="nomadic">Postulante Nomadic</option>
																					<option id="empresa" name="empresa" value="empresa">Empresa</option>
																				</select>


																				<input type="text" name="name" id="name" placeholder="Nombre completo" required>
																				<input type="email" name="email" id="email" placeholder="Tu@email" required>
																				<textarea id="message" name="message" placeholder="Mensaje" spellcheck="true" required></textarea required>
																					<input class="enviar" type="submit" value="ENVIAR">
																					<div id="contactResponse2"></div>
																				</form>
																			</div>
																		</div>
																	</section>
																	<!--///////////////////////////////////////////////////////-->
																</section>
																<!--///////////////////////////////////////////////////////-->




																<footer>
																	<div id="contenedor">
																		<p>NomadicResources ©2017 Todos los derechos reservados - Mendoza - Argentina - 2017 </p>
																		<p>Al navegar este sitio manifiesta su conformidad con los términos de confidencialidad y el uso de cookies del mismo. <br>Para más información, lea nuestros
																			<a href="terminos.html" target="_blank"><strong>Términos y Condiciones</strong></a> - <a href="legales.html" target="_blank"><strong>política de privacidad</strong></a>
																		</p>


																		<a href="http://bricksg.com.ar" target="_blank"><p>Diseño y desarrollo: <img src="img/bricksg.png" alt="BRICK Soluciones Gráficas Contenidos Editoriales"></p></a>
																	</div>
																</footer>



															</div><!--cierra pantalla-->


															<!--smooth scroll-->
															<script>
															$('a.scroll').click(function(){
																$('html, body').animate({
																	scrollTop: $( $.attr(this, 'href') ).offset().top
																}, 1000);
																return false;
															});
														</script>


														<!--smooth aparece flecha subir-->
														<script type='text/javascript'>
														//<![CDATA[
														// Botón para Ir Arriba
														jQuery.noConflict();
														jQuery(document).ready(function() {
															jQuery(".subir").hide();
															jQuery(function () {
																jQuery(window).scroll(function () {
																	if (jQuery(this).scrollTop() > 200) {
																		jQuery('.subir').fadeIn();
																	} else {
																		jQuery('.subir').fadeOut();
																	}
																});
																jQuery('.subir a').click(function () {
																	jQuery('body,html').animate({
																		scrollTop: 0
																	}, 800);
																	return false;
																});
															});

														});
														//]]>
													</script>


													<!--anima contenidos-->
													<script>
													var $animation_elements = $('.animation-element');
													var $window = $(window);

													function check_if_in_view() {
														var window_height = $window.height();
														var window_top_position = $window.scrollTop();
														var window_bottom_position = (window_top_position + window_height);

														$.each($animation_elements, function() {
															var $element = $(this);
															var element_height = $element.outerHeight();
															var element_top_position = $element.offset().top;
															var element_bottom_position = (element_top_position + element_height);

															//check to see if this current container is within viewport
															if ((element_bottom_position >= window_top_position) &&
															(element_top_position <= window_bottom_position)) {
																$element.addClass('in-view');
															} else {
																$element.removeClass('in-view');
															}
														});
													}

													$window.on('scroll resize', check_if_in_view);
													$window.trigger('scroll');
												</script>



												<script>
												$("#formulario2").submit(function(event)
												{
													/* evita el funcionamiento clasico del post */
													event.preventDefault();

													/* ubicar los elementos en la página */
													var $form = $( this ),
													$submit = $form.find( 'button[type="submit"]' ),
													name_value = $form.find( 'input[name="name"]' ).val(),
													email_value = $form.find( 'input[name="email"]' ).val(),
													message_value = $form.find( 'textarea[name="message"]' ).val(),
													select_value = $form.find( 'select[name="select"]' ).val(),
													url = $form.attr('action');

													/* Send the data using post */
													var posting = $.post( url, {
														name: name_value,
														email: email_value,
														message: message_value,
														select:select_value,
														lang:'<?php echo $language; ?>'
													});

													posting.done(function( data )
													{
														/* pone el echo en un div que está debajo del formulario, el estilo se pone en el php */
														$( "#contactResponse2" ).html(data);

														/* Change the button text. */
														$submit.text('Enviado, gracias');

														/* Disable the button. */
														$submit.attr("disabled", true);
													});
												});
											</script>
										</body>

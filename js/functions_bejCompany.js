var company = angular.module('myAppC', []);
company.controller('contNomadicCompany', function($scope,$http){
	$scope.buscarC = function(){
		$http.get("../control/functionsCompany.php")
		.success(function(dataC){
				$scope.dataC= dataC;
				var razon = dataC["0"].razonSocial;
				$("#razon-social > option[value="+razon+"]").prop("selected", true);
		})
	}
	$scope.buscarC();
	$scope.carDatosCont = function(idEmp){
		var nombre = $('#nombreRef').val();
		var documento = $('#dni').val();
		var puesto = $('#puestos').val();
		var tel = $('#telefonoRef').val();
		var telAlt = $('#telefonoRefAlt').val();
		var mail = $('#mailRef').val();
		var mailAlt = $('#mailRefAlt').val();
		var id_empresa = idEmp;
		$scope.msgNombreCC = "";
		$scope.msgDoCC = "";
		$scope.msgTelCC = "";
		$scope.msgMailCC = "";
		$scope.msgPuestoCC=""
		var valDCC = 5;
		var arrayContact = [nombre, documento, puesto, tel, mail];
		for (var i = 0; i < arrayContact.length; i++) {
			if(i == 0 && arrayContact[i]==''){
				$scope.msgNombreCC = "Debe completar este campo:";
				valDCC = valDCC-1;
			}else if(i == 1 && arrayContact[i]==''){
				$scope.msgDoCC = "Debe completar este campo:";
				valDCC = valDCC-1;
			} else if(i == 2 && arrayContact[i]==''){
				$scope.msgPuestoCC = "Debe completar este campo:";
				valDCC = valDCC-1;
			}else if(i == 3 && arrayContact[i]==''){
				$scope.msgTelCC = "Debe completar este campo:";
				valDCC = valDCC-1;
			} else if(i == 4 && arrayContact[i]==''){
				$scope.msgMailCC = "Debe completar este campo:";
				valDCC = valDCC-1;
			}
		};
		if (valDCC == 5) {
			$http.post("../control/functionsCompany.php", {'accion': 'GuardarDContactos', 'id_empresa': id_empresa, 'nombreC': nombre, 
															'documentoC': documento, 'puestoC': puesto, 'telC': tel, 'telAltC': telAlt, 
															'mailC': mail, 'mailAltC': mailAlt})
			.success(function(){
				$('#msgGR').fadeIn(1000);
				$('#msgGR').fadeOut(1500);
			})
		};
	};
	$scope.rubros=function(rb, user){
				var rubro = rb;
				var user_r = user;
				$http.post("../control/functionsCompany.php",{'accion':'guardarRubros', 'rubro': rubro, 'user_id': user_r})
				.then(function(){
				});
		}
	$scope.ModificarSitio=function(userSC){
		var userSC = userSC;
		var sitioName = $('#sitio-web').val();
			$http.post("../control/functionsCompany.php",{'accion':'guardarSitio', 'sitioName': sitioName, 'userSC': userSC})
			.success(function(){
				$('#msgGSitio').fadeIn(1000);
				$('#msgGSitio').fadeOut(1500);
			});
	}
	$scope.GuardarEmpresa = function(userGEmpresa){
		var id_empresa = userGEmpresa;
		var razonSocial = $('#razon-social').val();
		var cuit = $('#numero-razon-social').val();
		var calleEmp = $('#calle').val();
		var alturaEmp = $('#altura').val();
		var pisoEmp = $('#piso').val();
		var apartamento = $('#apartamento').val();
		var paisEmp = $('#pais').val();
		var provinciaEmp = $('#provincia').val();
		var departamentoEmp = $('#departamento').val();
		var cpEmp = $('#cp').val();
		var telEmp = $('#telefono').val();
		var mailEmp = $('#mail').val();
		var espc= ", ";
		$scope.msgValRazon = "";
		$scope.msgCuit = "";
		$scope.msgCalle = "";
		$scope.msgAltura = "";
		$scope.msgPais = "";
		$scope.msgProv = "";
		$scope.msgDepart = "";
		$scope.msgPostal = "";
		$scope.msgTelEmp = "";
		$scope.msgMailEmp = "";
		var valDE = 10;
		var ArrayEmpresa = [razonSocial, cuit, calleEmp, alturaEmp, paisEmp, provinciaEmp, departamentoEmp, cpEmp, telEmp, mailEmp];
		for (var i = 0; i < ArrayEmpresa.length; i++) {
			if(i==0 && !(ArrayEmpresa[i])){
				$scope.msgValRazon = "Debe seleccionar una opcion:";
				valDE = valDE -1;
			}else if(i==1 && ArrayEmpresa[i] == ''){
				$scope.msgCuit = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==2 && ArrayEmpresa[i] == ''){
				$scope.msgCalle = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==3 && ArrayEmpresa[i] == ''){
				$scope.msgAltura = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==4 && ArrayEmpresa[i] == ''){
				$scope.msgPais = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==5 && ArrayEmpresa[i] == ''){
				$scope.msgProv = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==6 && ArrayEmpresa[i] == ''){
				$scope.msgDepart = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==7 && ArrayEmpresa[i] == ''){
				$scope.msgPostal = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==8 && ArrayEmpresa[i] == ''){
				$scope.msgTelEmp = "Debe completar este campo:";
				valDE = valDE -1;
			}else if(i==9 && ArrayEmpresa[i] == ''){
				$scope.msgMailEmp = "Debe completar este campo:";
				valDE = valDE -1;
			}
		};
		if (valDE== 10) {
			var direccion_full = departamentoEmp + espc + provinciaEmp + espc + paisEmp;
					$scope.cargaMapa(direccion_full);
					/*Codigo del mapa*/
					var GoogleAPI = "https://maps.googleapis.com/maps/api/geocode/json?address=" + direccion_full + "&sensor=false";	
									$.getJSON(GoogleAPI, function(resultadoJSON){
									for(i=0; i < resultadoJSON.results.length; i++){
										respuesta = resultadoJSON.results[i];
										$("#latitudes").val(respuesta.geometry.location.lat);
										$("#longitudes").val(respuesta.geometry.location.lng);
										var latitud = $('#latitudes').val();
										var longitud= $('#longitudes').val();
									}
										$scope.tomarLatLong(latitud, longitud, id_empresa);
									});
					/*fin codigo del mapa*/
		};
	}
	$scope.cargaMapa=function(direccion){
								var geoCoder = new google.maps.Geocoder(direccion);			
								var request = {address:direccion};
								geoCoder.geocode(request, function(result, status){
									var latlng = new google.maps.LatLng(result[0].geometry.location.lat(), result[0].
										geometry.location.lng());
									var opciones ={
										zoom: 17,
										center: latlng,
										mapTypeId: google.maps.MapTypeId.ROADMAP
									};
									
								})
								};
	$scope.tomarLatLong=function(latitud, longitud, id_empresa){
								var latitud = latitud;
								var longitud = longitud;
								var razonSocial = $('#razon-social').val();
								var cuit = $('#numero-razon-social').val();
								var calleEmp = $('#calle').val();
								var alturaEmp = $('#altura').val();
								var pisoEmp = $('#piso').val();
								var apartamento = $('#apartamento').val();
								var paisEmp = $('#pais').val();
								var provinciaEmp = $('#provincia').val();
								var departamentoEmp = $('#departamento').val();
								var cpEmp = $('#cp').val();
								var telEmp = $('#telefono').val();
								var mailEmp = $('#mail').val();
								var id_empresa = id_empresa; 
								$scope.InsertarDDP(id_empresa, razonSocial, cuit, calleEmp, alturaEmp, pisoEmp, apartamento, paisEmp, provinciaEmp, departamentoEmp, cpEmp, latitud, longitud, telEmp, mailEmp);
						};
	$scope.InsertarDDP=function(id_empresa, razonSocial, cuit, calleEmp, alturaEmp, pisoEmp, apartamento, paisEmp, provinciaEmp, departamentoEmp, cpEmp, latitud, longitud, telEmp, mailEmp){
							$http.post("../control/functionsCompany.php",{'accion': 'guardarEmpresas', 'id_empresa': id_empresa, 'razonSoc': razonSocial,
																		 'cuit': cuit, 'calleEmp': calleEmp, 'alturaEmp': alturaEmp, 'pisoEmp': pisoEmp,
																		 'apartamento': apartamento, 'loc':departamentoEmp, 'prov': provinciaEmp, 
																		 'pais': paisEmp, 'codPos': cpEmp,'lat': latitud, 'long': longitud, 'telEmp': telEmp,
																		 'mailEmp': mailEmp})
											.then(function(guardar){
												$('#msgGEmp').fadeIn(1000);
												$('#msgGEmp').fadeOut(1500);
												/*$scope.msgD = guardar.data;*/
											});
		}
$scope.mostrarSFoto = function(){
		$('#contF').toggle();
	}


$scope.nombreArchivo = function(){
		var nombre = $('#imagen').val();
		var nombreB = nombre.split("\\");
		var nombreC = nombreB[nombreB.length -1];
		alert(nombreC);
	}
$scope.subirArchivo = function(){
var comprobar = $('#foto').val().length;
		
		if(comprobar>0){
			
			var formulario = $('#subida');
			
			var datos = formulario.serialize();
			
			var archivos = new FormData();	
			
			var url = '../control/subirF.php';
			
				for (var i = 0; i < (formulario.find('input[type=file]').length); i++) { 
				
               	 archivos.append((formulario.find('input[type="file"]:eq('+i+')').attr("name")),((formulario.find('input[type="file"]:eq('+i+')')[0]).files[0]));
				 
      		 	}
				
			$.ajax({
				
				url: url+'?'+datos,
				
				type: 'POST',
				
				contentType: false, 
				
            	data: archivos,
				
               	processData:false,
				
				beforeSend : function (){
					
					//$('#cargando').show(300);	
				
				},
				success: function(data){
					
					/*$('#cargando').hide(300);*/
					
					//$('#fotos').prop(data);
					$('#contF').css('display', 'none');
					$scope.buscarC();
					$('#subida')[0].reset();
					$('#msgGEmpF').fadeIn(1000);
					$('#msgGEmpF').fadeOut(1500);
					return false;
				}
				
			});
			
			return false;
			
		}else{
			
			alert('Selecciona una foto para subir e ingrese su descripcion');
			
			return false;
			
		}
}
});

function mostrarSFoto(){
		$('#contF').toggle();
	}


function nombreArchivo(){
		var nombre = $('#imagen').val();
		var nombreB = nombre.split("\\");
		var nombreC = nombreB[nombreB.length -1];
		alert(nombreC);
	}
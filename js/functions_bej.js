var app= angular.module('myApp', []);
app.controller('contNomadicPers', function($scope,$http){
		$scope.buscar=function(){
			$http.get("../control/functionsPeople.php")
			.success(function(data){
				$scope.data=data;
				$scope.usuario_global = data["0"].user_id;
				$scope.pepe = data["0"].genero;
				$scope.gene = data["0"].genero;
				$scope.genero(data["0"].genero);
				$scope.msgCargando ="";
				$('#perfil').html("");
				/*$scope.PruebaCD = data["0"].descripcion_puesto;*/
			});
		};

		$scope.modifDPP=function(){
			var nombre = $("#nombre").val();
			var nacimiento = $("#nacimiento").val();
			var edad = $("#edad").val();
			var dni = $("#dni").val();
			var generos = $("#generosa").val();
			var nacionalidad = $("#nacionalidad").val();
			var cuil = $("#cuil-persona").val();
			var user_id=$("#user_id").val();
			var accion = "guardar";
			/*Validar campos completados*/
			if (nombre == '') {
				$scope.valNombre= "Dato obligatorio";
			}else if(nacimiento == '' || nacimiento == "dd/mm/aaaa" ){
				$scope.valNaci= "Dato obligatorio";
			} else{
				$http.post("../control/functionsPeople.php", {"accion":"guardarDPP","user_id":user_id, "nombre":nombre, "nacimiento":nacimiento, "edad":edad, "dni":dni, "genero":generos, "nacionalidad":nacionalidad, "cuil":cuil})
				.then(function(){
					//$('#perfil').html("<div>Cargando...</div>");
					$('#msgG').fadeIn(1000);
					$scope.buscar();
					$('#msgG').fadeOut(1500);
					//$scope.msgCargando ="Enviando...";
				})
			};
		}

		$scope.guardarDDP=function(id_pers){
			var calle= $('#calle').val();
			var altura= $('#altura').val();
			var piso= $('#piso').val();
			var apartamento= $('#apartamento').val();
			var pais= $('#pais').val();
			var provincia= $('#provincia').val();
			var departamento= $('#departamento').val();
			var cp= $('#cp').val();
			var espc= ", ";
			var id_persona = id_pers;
			var direccion = [calle, altura, pais, provincia, departamento, cp];

			$scope.valCalle = "";
			$scope.valAltura = "";
			$scope.valPais = "";
			$scope.valProvincia = "";
			$scope.valDepartamento ="";
			$scope.valCodPos="";
			var val = 0;
			for (i= 0; i <= 5; i ++) {
				if(i == 0 && direccion[i]== ""){
					$scope.valCalle = "Campo obligatorio:";
					val = val -1;
				}else if(i == 1 && direccion[i]== "") {
						$scope.valAltura = "Campo obligatorio:";
						val = val -1;
				}else if (i == 2 && direccion[i]=="") {
						$scope.valPais = "Campo obligatorio:";
						val = val -1;
				}else if (i== 3 && direccion[i]=="") {
						$scope.valProvincia = "Campo obligatorio:";
						val = val -1;
				}else if (i== 4 && direccion[i]== "") {
						$scope.valDepartamento ="Campo obligatorio:";
						val = val - 1;
				}else if (i== 5 && direccion[i]==""){
						$scope.valCodPos="Campo obligatorio:";
						val = val-1;
				}
				val = val +1;
				};
				if(val == 6 ){
					var direccion_full = departamento + espc + provincia + espc + pais;
					$scope.cargaMapa(direccion_full);
					/*Codigo del mapa*/
					var GoogleAPI = "https://maps.googleapis.com/maps/api/geocode/json?address=" + direccion_full + "&sensor=false";	
									$.getJSON(GoogleAPI, function(resultadoJSON){
									for(i=0; i < resultadoJSON.results.length; i++){
										respuesta = resultadoJSON.results[i];
										$("#latitudes").val(respuesta.geometry.location.lat);
										$("#longitudes").val(respuesta.geometry.location.lng);
										var latitud = $('#latitudes').val();
										var longitud= $('#longitudes').val();
									}
										$scope.tomarLatLong(latitud, longitud, id_persona);
									});
					/*fin codigo del mapa*/
				}

		}


		$scope.buscar();
		$scope.cargaMapa=function(direccion){
								var geoCoder = new google.maps.Geocoder(direccion);			
								var request = {address:direccion};
								geoCoder.geocode(request, function(result, status){
									var latlng = new google.maps.LatLng(result[0].geometry.location.lat(), result[0].
										geometry.location.lng());
									var opciones ={
										zoom: 17,
										center: latlng,
										mapTypeId: google.maps.MapTypeId.ROADMAP
									};
									
								})
								};
		$scope.tomarLatLong=function(latitud, longitud, id_persona){
								var latitud = latitud;
								var longitud = longitud;
								var calle= $('#calle').val();
								var altura= $('#altura').val();
								var piso= $('#piso').val();
								var apartamento= $('#apartamento').val();
								var pais= $('#pais').val();
								var provincia= $('#provincia').val();
								var departamento= $('#departamento').val();
								var cp= $('#cp').val();
								var id_persona = id_persona; 
								$scope.InsertarDDP(id_persona, calle, altura, piso, apartamento, pais, provincia, departamento, cp, latitud, longitud);
						};

		$scope.InsertarDDP=function(id_persona, calle, altura, piso, apartamento, pais, provincia, departamento, cp, latitud, longitud){
							$http.post("../control/functionsPeople.php",{'accion': 'guardarDDP', 'id_person': id_persona, 'calle': calle, 'altura': altura, 'piso': piso,
																		 'apartamento': apartamento, 'loc':departamento, 
																		 'prov': provincia, 'pais': pais, 'codPos': cp,'lat': latitud, 'long': longitud})
											.then(function(guardar){
												$('#msgGDom').fadeIn(1000);
												$('#msgGDom').fadeOut(1500);
												/*$scope.msgD = guardar.data;*/
											});
		}
		$scope.InsertDCP=function(id_personaE){
			var nomade = $('input:checkbox[name=checkeador]:checked').val();
			if(!nomade){
				nomade = "false";
			}
			var mail = $('#mail').val();
			var mailAlt = $('#mailAlt').val();
			var telefono = $('#telefono').val();
			var telefonoAlt = $('#telefonoAlt').val();
			var id_persona = id_personaE;
			//var espCC= ", ";
			//alert(nomade + espCC + mail + espCC + mailAlt + espCC + telefono + espCC + telefonoAlt + espCC + id_persona);
			var contacto = [mail, telefono];
			var alternativo =[mailAlt, telefonoAlt];
			$scope.valMail = "";
			$scope.valMailAlt = "";
			$scope.valTel = "";
			$scope.valTelAlt = "";
			var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
			var caractN = new RegExp(/^([0-9])/);
			var valD = 0;
			var valDA = 2;
			for (i= 0; i <= 2; i ++) {
				if(i == 0 && contacto[i]== ""){
					$scope.valMail = "Campo obligatorio:";
					valD = valD -1;
				}else if(i == 0 && caract.test(contacto[i])== false){
					$scope.valMail = "Use formato: ejemplo@mail.com";
					valD = valD -1;
				}else if (i == 1 && contacto[i]=="") {
						$scope.valTel = "Campo obligatorio:";
						valD = valD -1;
				}else if (i== 1 && caractN.test(contacto[i])== false) {
						$scope.valTel = "Ingrese solo números";
						valD = valD -1;

				}
				valD ++;
				};
				for (var i = 0; i < alternativo.length; i++) {
					/*if(i == 0 && alternativo[i]!= ""){
						$scope.valMail = "Campo obligatorio:";
						valD = valD -1;
					}else*/ if(i == 0 && caract.test(alternativo[i])== false && alternativo[i]!=''){
						$scope.valMailAlt = "Use formato: ejemplo@mail.com";
						valDA = valDA -1;
					}else /*if (i == 1 && alternativo[i]=="") {
						$scope.valTel = "Campo obligatorio:";
						valD = valD -1;
					}else*/ if (i== 1 && caractN.test(alternativo[i])== false && alternativo[i]!='') {
						$scope.valTelAlt = "Ingrese solo números";
						valDA = valDA -1;

				}
				};
				if(valD == 3 && valDA == 2){
					$http.post("../control/functionsPeople.php", {'accion':'guardarDCP', 'mail':mail, 'mailAlt': mailAlt, 
								'tel': telefono, 'telAlt': telefonoAlt, 'nomade': nomade, 'user_id': id_persona})
					.then(function(){
						$('#msgGCon').fadeIn(1000);
						$('#msgGCon').fadeOut(1500);
					})
				}

		}

		$scope.rubros=function(rb, user){
				var rubro = rb;
				var user_r = user;
				$http.post("../control/functionsPeople.php",{'accion':'guardarRubros', 'rubro': rubro, 'user_id': user_r})
				.then(function(){
				})
		}

		$scope.cargos=function(user){
			var puesto = $('#puestos').val();
			var user_C = user;
			$http.post("../control/functionsPeople.php", {'accion':'guardarCargos', 'user_id': user_C, 'cargo' : puesto})
			.then(function(){
					$('#msgGCar').fadeIn(1000);
					$('#msgGCar').fadeOut(1500);
					//$scope.PruebaCD = "dato";
			})
		}

		$scope.antLab=function(){
			var empresa = $('#empresa').val();
			var sector = $('#sector').val();
			var puestoAnte = $('#puesto-antecedente').val();
			var finicio = $('#inicio').val();
			var ffin = $('#fin').val();
			var descripcion = $('#descripcion').val();
			var user_AL= $scope.usuario_global;
			var id_laboral = $('#id_laboral').val();
			var datosLaborales = [empresa, sector, puestoAnte, finicio, ffin];
			$scope.valEmpresal = "";
			$scope.valSector = "";
			$scope.valPuesto = "";
			$scope.valFinicio = "";
			var valL= 5; 
			for(var i = 0; i<=4; i++){
				if(i == 0 && datosLaborales[i] ==''){
					$scope.valEmpresal ="Campo obligatorio:";
					valL = valL -1;
				}else if(i == 1 && datosLaborales[i] =='') {
					$scope.valSector = "Campo obligatorio:";
					valL = valL -1;
				}else if(i == 2 && datosLaborales[i]==''){
					$scope.valPuesto = "Campo obligatorio:";
					valL = valL -1;
				}else if(i == 3 && datosLaborales[i]==''){
					$scope.valFinicio = "Debe completar fecha inicio y fin:";
					valL = valL -1;
				}else if(i == 4 && datosLaborales[i]==''){
					$scope.valFinicio = "Debe completar fecha inicio y fin:";
					valL = valL -1;	
				}
			}
			if(valL == 5){
							$http.post("../control/functionsPeople.php", {'accion':'guardarAntLab', 'user_id' : user_AL, 'empresa':empresa, 
														  'sector':sector, 'puestoAnte': puestoAnte, 'finicio': finicio, 
														  'ffin': ffin, 'descripcion': descripcion, 'id_laboral': id_laboral})
						.then(function(){
							$('#descripcion').val("");
							$('#empresa').val("");
							$('#id_laboral').val("");
							$('#sector').val("");
							$('#puesto-antecedente').val("");
							$('#inicio').val("");
							$('#fin').val("");
							$scope.buscarDLN();
			})
			}
		}


		/*NUEVOOOO*/		

		$scope.buscarDL=function(id_dato){
			var idd = id_dato;
			$http.get("../control/functionsPeopleWork.php")
			.success(function(dataD){
				$('#empresa').val(dataD[idd].empresa);
				$('#id_laboral').val(dataD[idd].id_laboral);
				$('#sector').val(dataD[idd].sector);
				$('#puesto-antecedente').val(dataD[idd].puestoAntec);
				$('#inicio').val(dataD[idd].inicio);
				$('#fin').val(dataD[idd].fin);
				$('#descripcion').val(dataD[idd].descripcion);
			});
		};

		$scope.buscarDLN=function(){
			$http.get("../control/functionsPeopleWork.php")
			.success(function(dataD){
				$scope.dataD=dataD;
				 var count = Object.keys($scope.dataD).length;
				 if(count > 0){
				 	$('#contLab').css('display','block');
				 }else{
				 	$('#contLab').css('display','none');
				 }
				/*alert(count);*/
				/*$scope.empresa = dataD["0"].empresa;*/
			});
		};
		$scope.buscarDLN();
		/*FIN NUEVOOO*/
		$scope.eliminarDL=function(id_labo){
			var id_labo= id_labo;
			$http.post("../control/functionsPeople.php", {'accion': 'eliminarAntLab', 'id_laboral': id_labo})
			.then(function(){
				$scope.buscarDLN();
			});
		};

		/*FUNCION DATOS ACADEMICOS*/

		$scope.CarDatosAcad = function(){
			var nivelAcademico = $('#nivel-educativo').val();
			var titulo = $('#titulo').val();
			var aInicio = $('#Ainicio').val();
			var aFin = $('#Afin').val();
			var user_AA= $scope.usuario_global;
			var id_academico = $('#id_academico').val();
			var datosAcademicos = [nivelAcademico, titulo, aInicio, aFin];
			$scope.valNivelE = "";
			$scope.valTitulo = "";
			$scope.valFechaI = "";
			var valA= 3; 
			for(var i = 0; i<=3; i++){
				if(i == 0 && datosAcademicos[i] =='' || i == 0 && datosAcademicos[i] == null){
					$scope.valNivelE ="Seleccionar un valor:";
					valA = valA -1;
				}else if(i == 1 && datosAcademicos[i] =='') {
					$scope.valTitulo = "Campo obligatorio:";
					valA = valA -1;
				}else if(i == 2 && datosAcademicos[i]==''){
					$scope.valFechaI = "Debe completar fecha inicio y fin:";
					valA = valA -1;
				}else if(i == 3 && datosAcademicos[i]==''){
					$scope.valFechaI = "Debe completar fecha inicio y fin:";
					valA = valA -1;
				}
			}
			if(valA == 3){
				$http.post("../control/functionsPeople.php", {'accion': 'GuardAcademico','id_academico': id_academico, 'id_personaA' : user_AA, 'nivelAcademico': nivelAcademico, 'titulo': titulo, 'aInicio': aInicio, 'aFin': aFin})
				.then(function(){
				$('#nivel-educativo').val("");
				$('#titulo').val("");
				$('#Ainicio').val("");
				$('#Afin').val("");
				$('#id_academico').val("");
				$scope.buscarAcad();
			});
			}
		};
		$scope.buscarAcad = function(){
			$http.get("../control/functionsPeopleStudy.php")
			.success(function(dataT){
				$scope.dataT=dataT;
				 var count = Object.keys($scope.dataT).length;
				 if(count > 0){
				 	$('#contStudy').css('display','block');
				 }else{
				 	$('#contStudy').css('display','none');
				 }
			});
		}
		$scope.ModifAcad=function(id_acad){
			var id_aca = id_acad;
			$http.get("../control/functionsPeopleStudy.php")
			.success(function(dataT){
				$('#id_academico').val(dataT[id_aca].id_academico);
				$('#titulo').val(dataT[id_aca].titulo);
				$('#Ainicio').val(dataT[id_aca].a_inicio);
				$('#Afin').val(dataT[id_aca].a_fin);
				$("#nivel-educativo > option[value="+ dataT[id_aca].nivel +"]").prop("selected", true);
			});
		}
		$scope.eliminarDA=function(id_acad){
			var id_aca = id_acad;
			var user_AA= $scope.usuario_global;
			$http.post("../control/functionsPeople.php", {'accion': 'eliminarAcademico', 'user_id': user_AA, 'id_aca': id_aca})
			.success(function(){
				$scope.buscarAcad();
			})
		}
		$scope.buscarAcad();
		
		/*FIN DATOS ACADEMICOS*/

		/*CARNET DE CONDUCIR*/
		$scope.BuscarCarnets= function(){
			$http.get("../control/functionPeopleCarnets.php")
			.success(function(dataC){
				$scope.dataC=dataC;
				 var count = Object.keys($scope.dataC).length;
				 if(count > 0){
				 	$('#contCarnets').css('display','block');
				 }else{
				 	$('#contCarnets').css('display','none');
				 }
			})
		}
		$scope.CargarCC= function(){
			var carnet = $('input[name=checCC]:checked').val();
			if (carnet) {
				carnet = 'si';
			}else {
				carnet = '';
			};
			var id_carnets = $('#id_carnets').val();
			var user_CC= $scope.usuario_global;
			var catCarnet = $('#categoria-carnet').val();
			var vencimiento = $('#vencimiento').val();
			var DatosCC = [carnet, catCarnet, vencimiento];
			var valCC = 2;
			$scope.valCC = "";
			$scope.valCCTt = "";
			$scope.valCCV = "";
			for (var i = 0; i < DatosCC.length; i++) {
				if(i == 0 && DatosCC[i] == ''){
					$scope.valCC = 'Debe seleccionar este item';
					valCC = valCC - 1;
				}else if(i == 1 && DatosCC[i] == ''){
					$scope.valCCTt = 'Debe seleccionar categoría';
					valCC = valCC - 1;
				}else if(i == 2 && DatosCC[i] == ''){
					$scope.valCCV = 'Debe seleccionar este item';
					valCC = valCC - 1;
				}
			}
			if(valCC == 2){
				$http.post("../control/functionsPeople.php", {'accion': 'GuardarCarnet', 'categoria':catCarnet, 'vtoCarnet': vencimiento, 'id_personaC': user_CC, 'idCarnets': id_carnets})
				.success(function(){
					$scope.BuscarCarnets();
					$('#id_carnets').val("");
					$("#categoria-carnet > option[value='']").prop("selected", true);
					$('#vencimiento').val("");
					$('input[name=checCC]').prop('checked', '');
				});
			};
			};

		$scope.ModificarCarnets= function(id_carnet){
			var id_carnet = id_carnet;
			$http.get("../control/functionPeopleCarnets.php")
			.success(function(dataC){
				$('#id_carnets').val(dataC[id_carnet].idcarnets);
				carnet = $('input[name=checCC]').prop('checked', 'true');
				$("#categoria-carnet > option[value="+ dataC[id_carnet].tipo +"]").prop("selected", true);
				$('#vencimiento').val(dataC[id_carnet].vencimiento);

			})
		}
		$scope.EliminarCarnets=function(id_carnetE){
			var id_carnetE = id_carnetE;
			var user_CC= $scope.usuario_global;
			$http.post("../control/functionsPeople.php",{'accion': 'EliminarCarnet', 'id_carnets': id_carnetE, 'id_personaE': user_CC})
			.success(function(){
			$scope.BuscarCarnets();
			})

		}
		$scope.BuscarCarnets();
		/*FIN CARNET DE CONDUCIR*/
		
		/*DISPONIBILIDAD*/
		$scope.ModificarDispo=function(){
			var fullTime= $('input[name=fullTime]:checked').val();
			var parTime= $('input[name=parTime]:checked').val();
			var eventual= $('input[name=event]:checked').val();
			var temporal= $('input[name=temp]:checked').val();
			var finSemana = $('input[name=fds]:checked').val();
			var user_dis= $scope.usuario_global;
			$scope.valDisp = "";
			if(fullTime){
				fullTime = "true";
			}else{
				fullTime= "false";
			}
			if(parTime){
				parTime = "true";
			}else{
				parTime= "false";
			}
			if(eventual){
				eventual = "true";
			}else{
				eventual= "false";
			}
			if(temporal){
				temporal = "true";
			}else{
				temporal= "false";
			}
			if(finSemana){
				finSemana = "true";
			}else{
				finSemana= "false";
			}
			if( fullTime == 'false' && parTime == 'false' && eventual == 'false' && temporal == 'false' && finSemana == 'false'){
					$scope.valDisp= "Debe seleccionar algún item:";
				}else{
					$http.post("../control/functionsPeople.php",{'accion': 'GuardarDiponible','user_dis': user_dis, 'fullTime': fullTime, 'parTime': parTime, 'eventual': eventual, 'temporal': temporal, 'finSemana': finSemana})
					.success(function(){
						$('#msgDispo').fadeIn(1000);
						$('#msgDispo').fadeOut(1500);
					});
				}
		}
		/*FIN DISPONIBILIDAD*/

		/*CARGA IDIOMA*/
		$scope.CargIdioma=function(){
			var user_idioma= $scope.usuario_global;
			var idiomas = $('input[name=idioma]').val();
			if (idiomas == '') {
				$scope.MsgIdm="Debe completar este campo:";
			}else{
				$http.post("../control/functionsPeople.php", {'accion': 'GuardarIdioma', 'idiomas': idiomas, 'user_idioma': user_idioma}).
				success(function(){
					$('#msgIdioma').fadeIn(1000);
					$('#msgIdioma').fadeOut(1500);	
				})
			}
		}
		/*FIN CARGA IDIOMA*/

		/*CARGA FOTOS PERSONA*/
		$scope.mostrarSFotoP = function(){
		$('#contFP').toggle();
			}
		$scope.subirArchivoP = function(){
		var comprobarP = $('#fotoP').val().length;
				
				if(comprobarP>0){
					
					var formularioP = $('#subidaP');
					
					var datosP = formularioP.serialize();
					
					var archivosP = new FormData();	
					
					var urlP = '../control/subirF.php';
					
						for (var i = 0; i < (formularioP.find('input[type=file]').length); i++) { 
						
		               	 archivosP.append((formularioP.find('input[type="file"]:eq('+i+')').attr("name")),((formularioP.find('input[type="file"]:eq('+i+')')[0]).files[0]));
						 
		      		 	}
						
					$.ajax({
						
						url: urlP+'?'+datosP,
						
						type: 'POST',
						
						contentType: false, 
						
		            	data: archivosP,
						
		               	processData:false,
						
						beforeSend : function (){
							
							//$('#cargando').show(300);	
						
						},
						success: function(data){
							
							/*$('#cargando').hide(300);*/
							
							//$('#fotos').prop(data);
							$('#contFP').css('display', 'none');
							$scope.buscar();
							$('#subidaP')[0].reset();
							$('#msgGEmpFP').fadeIn(1000);
							$('#msgGEmpFP').fadeOut(1500);
							return false;
						}
						
					});
					
					return false;
					
				}else{
					
					alert('Selecciona una foto para subir e ingrese su descripcion');
					
					return false;
					
				}
		};
		/*FIN CARGA FOTOS PERSONA*/
		$scope.genero=function(generos){
			if(generos == 'Masculino'){
				$scope.selectU = 'Masculino';
				$scope.selectD = 'Femenino';
				$scope.selctT = 'Otro';
				$scope.selctC = 'Género';
			}else if (generos == 'Femenino'){
				$scope.selectU = 'Femenino';
				$scope.selectD = 'Masculino';
				$scope.selctT = 'Otro'
				$scope.selctC = 'Género';
			}else if (generos == 'Otro'){
				$scope.selectU = 'Otro';
				$scope.selectD = 'Masculino';
				$scope.selctT = 'Femenino';
				$scope.selctC= 'Género';
			}
			else{
				$scope.selectU = 'Género';
				$scope.selectD = 'Masculino';
				$scope.selctT= 'Femenino';
				$scope.selctC= 'Otro';
			}
		}

	})
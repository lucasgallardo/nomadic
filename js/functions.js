$(document).ready(function() {
  $('#ingresar').click(function(){
    var user = $('#usuario').val();
    var pass = $('#pass').val();
console.log(pass);
    if($.trim(user).length > 0 && $.trim(pass).length > 0){
      $.ajax({
        url:"../control/authenticator.php",
        method:"POST",
        data:{user:user, pass:pass},
        cache:"false",
        beforeSend:function() {
          $('#ingresar').val("Ingresando...");
          $("#result").html("");
        },
        success:function(data) {
          $('#ingresar').val("Ingresar");
          console.log(data);
          switch (data) {
            case "1":
              $(location).attr('href','person_profile_p.php');
              break;
            case "2":
              $(location).attr('href','company_profile_o.php');
              break;
            case "verificar":
              $("#result").html("<div class='error'><strong>¡Error!</strong> Debe validar el correo.</div>");
              break;
            default:
              $("#result").html("<div class='error'><strong>¡Error!</strong> las credenciales son incorrectas.</div>");
          }
        }
      });
    }else{
      $("#result").html("<div class='error'>Debes completar todos los campos primero.</div>");
    };
  });
});

$(document).ready(function(){
  $('#registrar').click(function(){
    var empresa = $('#nombre-empresa').val();
    var cuit = $('#cuit').val();
    var name = $('#nombre').val();
    var lastname = $('#apellido').val();
    var email = $('#email').val();
    var pass = $('#password').val();
    var rpass = $('#confirmar-password').val();
    var captchaResponse = $('#g-recaptcha-response').val();

    if ($('#condicionesterminos').is(':checked'))
    {
      if ($.trim(name).length > 0 && $.trim(lastname).length > 0 && $.trim(email).length > 0 && $.trim(pass).length > 0 && $.trim(rpass).length > 0){
        $.ajax({
          url:'../control/register.php',
          method:"POST",
          data:{name:name, lastname:lastname, email:email, pass:pass, rpass:rpass, empresa:empresa, cuit:cuit, captchaResponse: $("#g-recaptcha-response").val()},
          cache:false,
          beforeSend:function(){
            $('#registrar').val("Comprobando información...");
          },
          success:function(data){
            $('#registrar').val("Registrar");
            if(data){
              $("#resultRegistro").html(data);
            };
          }
        });
      }else{
        $("#resultRegistro").html("<div class='error'>Debes completar todos los campos primero.</div>");
      };
    }else {
      $("#resultRegistro").html("<div class='error'>Debe aceptar términos y condiciones");
    }

  });
});


$(document).ready(function(){
  $('#correo_recupera').click(function(){
    var mail = $('#mailrecupera').val();

      if ($.trim(mail).length > 0){
        $.ajax({
          url:'../control/recupera_clave.php',
          method:"POST",
          data:{mail:mail},
          cache:false,
          beforeSend:function(){
            $('#correo_recupera').val("Enviando...");
          },
          success:function(data){
            $('#correo_recupera').val("Enviar mail");
            if(data){
              $("#resultRecupera").html(data);
            };
          }
        });
      }else{
        $("#resultRecupera").html("<div class='error'>Ingresar un mail válido.</div>");
      };
  });
});

$(document).ready(function(){
  $('#guarda_clave').click(function(){
    var clave = $('#inputPassword').val();
    var clave2 = $('#inputPassword2').val();
    var id = $('#idCliente').val();
    var type = $('#usuario_tipo').val();
    var email = $('#usuario_email').val();

    if ($.trim(clave).length > 0 && $.trim(clave2).length > 0){
        $.ajax({
          url:'../control/guarda_clave.php',
          method:"POST",
          data:{clave:clave, clave2:clave2, id:id, type:type, email:email},
          cache:false,
          beforeSend:function(){
            $('#guarda_clave').val("Actualizando...");
          },
          success:function(data){
            $('#guarda_clave').val("Guardar");
            if(data){
              $("#resultado").html(data);
            };
          }
        });
      }else{
        $("#resultado").html("<div class='error'>Ingresar una clave.</div>");
      };
  });
});

// function SINO(cual) {
//    var elElemento=document.getElementById(cual);
//    if(elElemento.style.display == 'block') {
//       elElemento.style.display = 'none';
//    } else {
//       elElemento.style.display = 'block';
//    }
// }


// $(document).ready(function(){
//  $("#cambia").click(function(){
//  $("#texto").toggle(400);
//  });
// });

$(document).ready(function(){
  $('#postular').click(function(){
    var id_oferta = $('#id_oferta').val();
    var postulante = $('#postulante').val();


    // if ($('#condicionesterminos').is(':checked')) //usar para validar login
    // {
      if ($.trim(id_oferta).length > 0 && $.trim(postulante).length > 0){
        $.ajax({
          url:'../control/postular.php',
          method:"POST",
          data:{id_oferta:id_oferta, postulante:postulante},
          cache:false,
          beforeSend:function(){
            $('#postular').val("Comprobando información...");
          },
          success:function(data){
            $('#postular').val("Postularme");
            if(data){
              $("#resultPost").html(data);
            };
          }
        });
      }else{
        $("#resultPost").html("<div class='error'>Error.</div>");
      };
    // }else {
    //   $("#resultRegistro").html("<div class='error'>Debe aceptar términos y condiciones");
    // }

  });
});
